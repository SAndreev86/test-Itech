<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<?$APPLICATION->IncludeComponent("bitrix:news.detail",".default",[

        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => IBLOCK_ALL_BOOKS,
        "ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
        "PROPERTY_CODE" => Array("DESCRIPTION"),
    ]
);?>









<? $APPLICATION->IncludeComponent("bitrix:news.list", "author.books", Array(

        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => IBLOCK_ALL_BOOKS,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => "",
        "PROPERTY_CODE" => Array("DESCRIPTION"),
    )
); ?>

<? $APPLICATION->IncludeComponent("bitrix:news.list", "last.books", Array(

        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => IBLOCK_ALL_BOOKS,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => "",
        "PROPERTY_CODE" => Array("DESCRIPTION"),
    )
); ?>

<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list",
	"review.list",
	array(
		"SEF_MODE" => "N",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => IBLOCK_REVIEWS,
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"EDIT_URL" => "",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ELEMENT_ASSOC_PROPERTY" => "2",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"SEF_FOLDER" => "/",
		"COMPONENT_TEMPLATE" => "review.list"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form",
	"review.form",
	array(
		"SEF_MODE" => "N",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => IBLOCK_REVIEWS,
		"PROPERTY_CODES" => array(
			0 => "14",
			1 => "15",
			2 => "16",
			3 => "NAME",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "14",
			1 => "15",
			2 => "16",
			3 => "NAME",
		),
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS_NEW" => "N",
		"STATUS" => "ANY",
		"LIST_URL" => "",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ELEMENT_ASSOC_PROPERTY" => "",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"SEF_FOLDER" => "/",
		"COMPONENT_TEMPLATE" => "review.form"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>