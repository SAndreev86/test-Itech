<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arFilterNew = [
    "ACTIVE" =>"Y",
    [
        "LOGIC" => "AND",
    ],
];

foreach ($arParams["PROPERTY_CODE"] as $code) {

    if(!empty($_REQUEST[$code]) && $code == "NAME") {

        $arFilterNew[0]["NAME"] = "%".$_REQUEST[$code]."%";

    }
    if(!empty($_REQUEST[$code]) && $code == "AFTER_PRICE_BOOK") {

        $arFilterNew[0]['>=PROPERTY_PRICE'] = $_REQUEST[$code];

    }
    if(!empty($_REQUEST[$code]) && $code == "BEFORE_PRICE_BOOK") {

        $arFilterNew[0]["<=PROPERTY_PRICE"] = $_REQUEST[$code];

    }

    if (!empty($_REQUEST[$code]) && $code != "NAME" && $code != "AFTER_PRICE_BOOK" && $code != "BEFORE_PRICE_BOOK") {

        $arFilterNew[0]["PROPERTY_".$code] = "%".$_REQUEST[$code]."%";

    }

    $arResult["ITEMS"][] =  $code;
}


if(!empty($_REQUEST["SEARCH"])) {
    $arFilterNew = [
        "ACTIVE" =>"Y",
        [
            "LOGIC" => "OR",
            "NAME" => "%".$_REQUEST["SEARCH"]."%",
            "PROPERTY_AUTHOR" => "%".$_REQUEST["SEARCH"]."%",
        ],
    ];
}



$GLOBALS[$arParams["FILTER_NAME"]] = $arFilterNew;


$this->IncludeComponentTemplate();
?>