<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<form class="search-header-form" action="/">
    <input name="SEARCH" class="search-header-text" type="text" data-validate="text" placeholder="<?=GetMessage("IBLOCK_SEARCH")?>">
    <input class="search-header-btn js-search-header-btn" type="submit" value="">
</form>
