<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>

<tr>
    <form name="filter" action="/" method="get">

        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <th>
                <div class="form-input">

                    <label>
                        <? if ($arItem == "AFTER_PRICE_BOOK") : ?>
                            <input type="text" name="<?= $arItem ?>"
                                   value="<?= $_REQUEST[$arItem] ?>" data-validate=" text">
                            <span class="form-item-label">От</span>
                        <? endif ?>
                        <? if ($arItem == "BEFORE_PRICE_BOOK") : ?>
                            <input type="text" name="<?= $arItem ?>"
                                   value="<?= $_REQUEST[$arItem] ?>" data-validate=" text">
                            <span class="form-item-label">До</span>
                        <? endif ?>

                        <? if ($arItem != "AFTER_PRICE_BOOK" && $arItem != "BEFORE_PRICE_BOOK") : ?>
                            <input type="text" name="<?= $arItem ?>"
                                   value="<?= $_REQUEST[$arItem] ?>" data-validate=" text">
                            <span class="form-item-label"><?= GetMessage("ENTER") ?></span>
                        <? endif ?>

                    </label>


                </div>

            </th>
        <? endforeach; ?>
        <th>
            <input class="default-btn green" type="submit" value="<?= GetMessage("FILTER") ?>"/>
        </th>
    </form>
</tr>

