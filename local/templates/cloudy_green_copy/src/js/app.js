var _pageShare;
var moduleApp = {
    'init': function () {
        moduleApp.pollifil();
        moduleApp.sliderSwiper();
        moduleApp.productSlider();
        moduleApp.formValidation();
        moduleApp.contacts();
        moduleApp.tabs();
        moduleApp.alternativeAccordion();
        moduleApp.accordion();
        moduleApp.customSelect();
        moduleApp.elementForms();
        moduleApp.mobileMenu();
        moduleApp.callMe();
        moduleApp.sendMessage();
        // moduleApp.sendEmail();
        moduleApp.parallaxTeam();
        moduleApp.resizeWindow();
        moduleApp.validSearch();
        moduleApp.brands();
        moduleApp.pageNews();
        moduleApp.subscribe();
        moduleApp.searchHeader();
        moduleApp.socialShare();
        moduleApp.loupeCursore();
        moduleApp.catalogPage();
        moduleApp.counterProduct();
        moduleApp.detailPage();
        moduleApp.shoppingCart();
        moduleApp.lkHistory();
        moduleApp.lkProfile();
        moduleApp.addFile();
        moduleApp.lkPersonal();
        moduleApp.newOrder();
        moduleApp.regionalOrder();
        moduleApp.startupMessage();
        moduleApp.basket();
        moduleApp.adaptiveTable();
    },
    'pollifil': function(){
        if (!("classList" in window.document.body)) {
            Object.defineProperty(Element.prototype, 'classList', {
                get: function() {
                    var self = this, bValue = self.className.split(" ")
                    bValue.add = function (){
                        var b;
                        for(var i in arguments){
                            b = true;
                            for (var j = 0; j<bValue.length;j++)
                                if (bValue[j] == arguments[i]){
                                    b = false;
                                    break
                                }
                            if(b)
                                self.className += (self.className?" ":"")+arguments[i]
                        }
                    };
                    bValue.remove = function(){
                        self.className = "";
                        for(var i in arguments)
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != arguments[i])
                                    self.className += (self.className?" " :"")+bValue[j]
                    };
                    bValue.toggle = function(x){
                        var b;
                        if(x){
                            self.className = "";
                            b = false;
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != x){
                                    self.className += (self.className?" " :"")+bValue[j];
                                    b = false;
                                } else b = true;
                            if(!b)
                                self.className += (self.className?" ":"")+x
                        } else throw new TypeError("Failed to execute 'toggle': 1 argument required");
                        return !b;
                    };

                    return bValue;
                },
                enumerable: false
            });
        };

        (function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                    || window[vendors[x]+'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
        }());
    },
    'popupOpen': function (content, style, beforeFunction, afterFunction, beforeClose, $subject) {
        $subject = $subject || $;
        content = content || '';
        style = style || 'fb-default-style';
        beforeFunction = beforeFunction || '';
        afterFunction = afterFunction || false;
        beforeClose = beforeClose || false;
        var configFancy = {
            content: content,
            wrapCSS: style,
            padding: 0,
            margin: 10,
            fitToView: false,
            openEffect: 'drop',
            closeEffect: 'drop',
            scrolling: 'auto',
            maxWidth: 1100,
            // maxHeight: 800,
            autoHeight: true,
            'beforeShow': function () {
                if (beforeFunction) {
                    beforeFunction();
                }
                hasPlaceholderSupport = function () {
                    var input = document.createElement('input');
                    return ('placeholder' in input);
                }
            },
            'afterShow': function () {
                $('.fancybox-wrap').addClass('fancybox-wrap-open');
                if (afterFunction) {
                    afterFunction();
                }
            },
            'beforeClose': function () {
                var $thisWrapper = $('.fancybox-wrap');
                if ($thisWrapper.hasClass('fancybox-wrap-close')) {
                    return true;
                } else {
                    if (beforeClose) {
                        beforeClose();
                    }
                    $thisWrapper.addClass('fancybox-wrap-close');
                    setTimeout(function () {
                        $.fancybox.close();
                    }, 300);
                    return false;
                }
            }
        };

        $subject.fancybox(configFancy);
    },
    'formValidation': function ($submitBtn, submitFunction) {
        submitFunction = submitFunction || false;
        $submitBtn = $submitBtn || $('.js-form-submit');
        var $submitForm = $submitBtn.closest('form');
        $submitForm.addClass('is-form-validation');
        var errorValidate = 'Поле обязательно для заполнения';
        var errorValidate2 = 'Поле заполнено не корректно';

        if(!device.mobile()){
            if(!device.tablet() || !device.android()){
                $submitForm.find('[data-mask="phone"]').mask("+7 (999) 999 99 99", {placeholder: "-"});
            }
            else{
                $submitForm.find('[data-mask="phone"]').attr('type','number');
            }
        }
        else{
            $submitForm.find('[data-mask="phone"]').attr('type','number');
        }

        $submitBtn.click(function (e) {
            e.preventDefault();

            var $this = $(this);
            var $thisForm = $this.closest('form');
            if ($this.hasClass('disabled')) {
                return false;
            }
            var $forms = $thisForm.find('[data-validate]');
            var result = formChecking($forms, true);
            if (result) {
                if (submitFunction) {
                    $this.addClass('disabled');
                    submitFunction($thisForm);
                }
                else{
                    setTimeout(function() {
                        $thisForm.submit();
                    }, 50);
                }
            }
            else {
                $forms.on('keyup keypress change', function () {
                    var $current = $(this);
                    setTimeout(function () {
                        formChecking($current, true);
                    }, 100);
                });
            }

            return false;
        });

        $(document).on('keydown', function (e) {
            return true;
            if (e.keyCode == 13) {
                var $thisFocus = $(document.activeElement);
                if ($thisFocus.is('textarea')) {
                    return true;
                }
                if ($thisFocus.closest('.form-select').length) {
                    return true;
                }
                if ($thisFocus.closest('.is-form-validation').length) {
                    $submitBtn.trigger('click');
                }
                return false;
            }
        });


        function formChecking($inp, onFocus) {

            onFocus = onFocus || false;
            var noError = true;

            $inp.each(function (ind, elm) {

                var $this = $(elm);
                var mask = $this.data('validate');
                var value = $this.val();
                var placeHolder = $this.attr('placeholder');

                if($this.is(':visible')){
                    if (mask == 'text') {
                        var regex = /^\s/;
                        if ((value.length < 1 || regex.test(value)) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'phone') {
                        if ((value.length < 1) || (value.indexOf('-') > -1)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'file') {
                        if (value.length < 2) {
                            noError = false;
                            $this.closest('.form-file').addClass('show-error');
                            if (onFocus) {
                                $this.focus();
                                onFocus = false;
                            }
                        } else {
                            $this.closest('.form-file').removeClass('show-error');
                        }
                    }

                    if (mask == 'textarea') {
                        var regex = /^\s/;
                        if ((value.length < 3 || regex.test(value)) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-textarea').addClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-textarea').removeClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'email') {
                        if(value != ''){
                            var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                            if (!regex.test(value) || (value == placeHolder)) {
                                noError = false;
                                $this.closest('.form-input').addClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideDown(200);
                                if (onFocus) {
                                    if(!device.ios()){
                                        $this.focus();
                                        onFocus = false;
                                    }
                                }
                            } else {
                                $this.closest('.form-input').removeClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideUp(200);
                            }
                        }
                    }

                    if(mask == "email-required"){
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'filter-email') {
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (regex.test(value) || (value == '')) {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        } else {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                    }

                    if (mask == 'select') {
                        if (!value) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-select').addClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-select').removeClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'checkbox') {
                        if (!$this.is(':checked')) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-checkbox').addClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        } else {
                            $this.closest('.form-checkbox').removeClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'captcha') {
                        var response = grecaptcha.getResponse();
                        if (response.length == 0){
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.addClass('show-error');
                        } else {
                            $this.removeClass('show-error');
                        }
                    }

                    if (mask == 'serial-number'){
                        var regex = /[0-9]{9,}/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'pass') {
                        if (value.length < 6) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'two-pass'){
                        var pass = $('.fancybox-inner .password').val();
                        if(value == '' || value != pass){
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                        else{
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'date'){
                        if (value.length < 1) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }
                }
            });

            setTimeout(function(){
                $.fancybox.update();
            },300);

            return noError;
        }
    },
    'sliderSwiper': function () {
        if($('.js-main-slider').length> 0){

            var configMain = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    pagination: '.swiper-pagination',
                    onSlideChangeStart: function(){
                    }
                },
                $mainSlider = $('.js-main-slider');

            if($('.js-main-slider .swiper-slide').length > 1){
                var $mainSwiper = $mainSlider.swiper(configMain);
            }
        }

        if($('.js-about-slider').length > 0){
            var slideActive = 0;
            var slidePosition = 0;
            var scroll = 0;

            var configAbout = {
                    slidesPerView: 4,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    simulateTouch: false,
                    onSlideNextStart: function(swiper){
                        var activeIndex = swiper.activeIndex;
                        if(activeIndex > slideActive){
                            $(swiper.slides[activeIndex]).find('.js-about-date').trigger('click')
                        }
                        slidePosition++;
                    },
                    onSlidePrevStart: function(swiper){
                        var activeIndex = swiper.activeIndex,
                            currentLimit = activeIndex + 3;

                        if(slideActive > currentLimit ){
                            $(swiper.slides[currentLimit]).find('.js-about-date').trigger('click')
                        }
                        slidePosition--;
                    },
                },
                $aboutSlider = $('.js-about-slider');

            var $aboutSwiper = $aboutSlider.swiper(configAbout);

            $('.js-about-date').on('click', function(){
                if(!$(this).hasClass('active')){
                    var $dates = $('.js-about-date'),
                        $this = $(this),
                        $slide = $this.closest('.swiper-slide');
                    var $content = $this.find('.content').clone();

                    $dates.removeClass('active');
                    $this.addClass('active');
                    slideActive = $slide.index();
                    $('.js-about-content .content').removeClass('active').addClass('deleted');
                    $content.appendTo('.js-about-content');
                    setTimeout(function(){
                        $('.js-about-content .deleted').remove();
                        $('.js-about-content .content').addClass('active');
                    },400);
                    //$this.find('.content').clone().appendTo('.js-about-content');
                }
            });
        }

        if($('.js-is-slider').length > 0){
            var configIsSlider = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    autoHeight: true,
                    pagination: '.swiper-pagination',
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                },
                $isSlider = $('.js-is-slider');

            var $isSwiper = $isSlider.swiper(configIsSlider);            
        }

        if($('.js-detail-slider').length > 0){
            var configDetail = {
                    slidesPerView: 2,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 20,
                    nextButton: '.detail-slider-button-next',
                    prevButton: '.detail-slider-button-prev',
                },
                $detailSlider = $('.js-detail-slider');

            var $detailSwiper = $detailSlider.swiper(configDetail);
        }

        if($('.js-prodaction-slider').length > 0){
            var configProdaction = {
                slidesPerView: 'auto',
                spaceBetween: 20,
                nextButton: '.product-swiper-next',
                prevButton: '.product-swiper-prev',
                },
                $prodactionSlider = $('.js-prodaction-slider');

            var productSwiper = $prodactionSlider.swiper(configProdaction);
        }

        if($('.js-office-slider').length > 0){
            var configOffice = {
                slidesPerView: 'auto',
                spaceBetween: 20,
                nextButton: '.office-swiper-next',
                prevButton: '.office-swiper-prev',
                },
                $officeSlider = $('.js-office-slider');

            var officeSwiper = $officeSlider.swiper(configOffice);
        }

        if($('.js-slider-news-profile').length > 0){
            var configNewsProfile = {
                    slidesPerView: 3,
                    centeredSlides: false,
                    paginationClickable: false,
                    spaceBetween: 0,
                    autoHeight: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    breakpoints: {
                        945:{
                            slidesPerView: 4,
                            spaceBetween: 0,
                        },
                        935:{
                            slidesPerView: 2,
                        },
                        640:{
                            slidesPerView: 1,
                            spaceBetween: 0,
                        }
                    }
                },
                $configNewsProfileSlider = $('.js-slider-news-profile');

            var $configNewsProfileSwiper = $configNewsProfileSlider.swiper(configNewsProfile);
        }
    },
    'productSlider': function(){
        if($('.js-product-slider').length > 0){
            var configProduct = {
                    slidesPerView: 4,
                    centeredSlides: false,
                    paginationClickable: false,
                    spaceBetween: 30,
                    updateTranslate: false,
                    breakpoints: {
                        945:{
                            slidesPerView: 4,
                            spaceBetween: 30,
                        },
                        935:{
                            slidesPerView: 2,
                        },
                        640:{
                            slidesPerView: 1,
                            spaceBetween: 20,
                        }
                    }
                },
                $productSlider = $('.js-product-slider:visible');

            $productSlider.each(function(ind,elt){
                var $this = $(this);
                configProduct.nextButton = $this.closest('.product-slider-wrapper').find('.swiper-button-next')[0];
                configProduct.prevButton = $this.closest('.product-slider-wrapper').find('.swiper-button-prev')[0];

                if($productSlider.find('.swiper-slide').length < 4) {
                    $this.find('.swiper-slide').addClass('swiper-slide-active');
                    $this.find('.swiper-button-prev').css('display', 'none');
                    $this.find('.swiper-button-next').css('display', 'none');
                }
                else{
                    var $productSwiper = new Swiper($this, configProduct);
                }
            });
        }
    },
    'productSlider2': function($wrapper){
        var configProduct = {
                slidesPerView: 4,
                centeredSlides: false,
                paginationClickable: false,
                spaceBetween: 30,
                updateTranslate: false,
                breakpoints: {
                    945:{
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                    935:{
                        slidesPerView: 2,
                    },
                    640:{
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            },
            $productSlider = $wrapper.find('.js-product-slider'),
            $productSwiper;

        configProduct.nextButton = $wrapper.find('.swiper-button-next');
        configProduct.prevButton = $wrapper.find('.swiper-button-prev');

        $productSwiper = new Swiper($productSlider, configProduct);
    },
    'contacts': function() {
        if($('.page-contacts').length){

            var $points = $('.js-map-item.active'),
                lat = $points.attr('data-lat'),
                long = $points.attr('data-long'),
                myMap,
                myGeoObject;

            ymaps.ready(function() {
                initMap(lat, long);
            });
        }

        function initMap(lat, long){
            myMap = new ymaps.Map('map', {
                    center: [lat, long],
                    zoom: 14,
                    controls: ['zoomControl']
            });

            constructMapPoints(lat, long);
        };

        function constructMapPoints(lat, long){
            if (myGeoObject) {
                myMap.geoObjects.remove(myGeoObject);
                myGeoObject = null;
            }

            myMap.setCenter([lat, long], 14);

            myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [lat, long]
                },
            }, {
                iconImageHref: '/dist/img/pin.png',
                iconLayout:  'default#image',
                iconImageSize: [58, 66],
            });

            myMap.geoObjects.add(myGeoObject);
        };

        $('.js-map-item').on('click', function(e){
            e.preventDefault();

            var $this = $(this);

            if(!$this.hasClass('active')){
                $('.js-map-item').removeClass('active');
                $this.addClass('active');

                lat = $this.attr('data-lat');
                long = $this.attr('data-long');

                constructMapPoints(lat, long);
            }
        });

        $('.js-connect-btn').on('click', function(){
            var template = $('.js-connect-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){
                setTimeout(function(){
                    var selectricElement = $('.fancybox-inner  .js-select').selectric();
                    selectricElement


                },300);

                if(device.ipad()) {
                    $('.fancybox-inner .js-select').on('mouseup', function () {
                        $(document).scrollTop(0);
                    });
                }

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){

                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);

                });
            });
        });

        $('.js-service-btn').on('click', function(){
            var template = $('.js-service-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        $('.js-training-btn').on('click', function(){
            var template = $('.js-training-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'alternativeAccordion': function() {
        $('.js-alternative-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.alternative-accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300,
                imageUrl = '',
                logoUrl = '';

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

                imageUrl = $this.attr('data-img');
                logoUrl = $this.attr('data-logo');

                $('.js-image-about').addClass('disabled');

                setTimeout(function(){
                    $('.js-border-img').attr('xlink:href', imageUrl);
                    $('.js-img-slide').attr('src', imageUrl);
                    $('.js-logo-slide').attr('src', logoUrl);

                    setTimeout(function(){
                        $('.js-image-about').removeClass('disabled');
                    },100);

                },300);
            }
        });
    },
    'tabs': function() {
        $(document).on('click', '.js-tabs-controls', function(e){
            e.preventDefault();

            var $item = $(this).closest('.tabs-controls-item'),
                $parent = $(this).closest('.js-tabs-wrapper'),
                $contentItem = $parent.find('.js-tabs-item'),
                itemNumber = $item.index();

            $contentItem.eq(itemNumber)
                .add($item)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });

        //табы для деталки со слайдерами
        $(document).on('click', '.js-tabs-controls-custom-slider', function(e){
            e.preventDefault();

            var $item = $(this).closest('.tabs-controls-item'),
                $parent = $(this).closest('.js-tabs-wrapper'),
                $contentItem = $parent.find('.js-tabs-item'),
                itemNumber = $item.index();

            if(!$(this).hasClass('active-slide')){

                setTimeout(function(){
                    moduleApp.productSlider2($contentItem.eq(itemNumber));
                }, 200);
                $(this).addClass('active-slide');
            }

            $contentItem.eq(itemNumber)
                .add($item)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });

        if($(document).width() < 930){
            if(!$('.tabs-controls').hasClass('mobile')){
                $('.tabs-controls').each(function(ind,elem){
                    var $this = $(elem),
                        length = 0;

                    $this.css('width', '1000px');
                    $this.find('.tabs-controls-item').each(function(index,element){
                        length = length + $(element).outerWidth(true);
                    });

                    $this.addClass('mobile');
                    $this.css('width', length+'px');
                });
            }
        }
    },
    'accordion': function() {
        $('.js-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300;

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

            } else {
                content.stop(true, true).slideUp(duration);
                item.removeClass('active');
            }

        });
    },
    'customSelect': function(){
        if($('.js-select').is(':visible')){
            $('.js-select').selectric();
        }
    },
    'elementForms': function(){
        $(document).on('change', 'input', function(){
            if($(this).val().length > 0){
                $(this).closest('.form-input').addClass('filled');
                $(this).closest('.delivery-wrapper').addClass('filled');
            }
            else{
                $(this).closest('.form-input').removeClass('filled');
                $(this).closest('.delivery-wrapper').removeClass('filled');
            }
        });

        $(document).on('change', 'textarea', function(){
            if($(this).val().length > 0){
                $(this).parents('.form-textarea').addClass('filled');
            }
            else{
                $(this).parents('.form-textarea').removeClass('filled');
            }
        });
    },
    'mobileMenu': function(){
        $('.js-m-btn-menu').on('click', function(){
            $(this).toggleClass('active');
            $('.mobile-menu-wrapper').toggleClass('active');

            $('.site-body').toggleClass('open-menu');
        });

        $('.js-mm-trigger').on('click', function(){
            var $triggerElement = $('.js-mm-trigger'),
                $this = $(this),
                $parentTrigger = $this.parents('.mm-item'),
                $parentsTrigger = $('.js-mm-trigger').parents('.mm-item'),
                listItem = $parentTrigger.find('.mm-inner-item'),
                listsItem = $parentsTrigger.find('.mm-inner-item'),
                duration = 300;

            if($this.hasClass('active')){
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                listsItem.stop(true, true).slideUp(duration);
            }
            else{
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                $(this).addClass('active');
                $parentTrigger.addClass('active');
                listsItem.stop(true, true).slideUp(duration);
                listItem.stop(true, true).slideDown(duration);
            }

        });

        $('.js-inner-menu-link').on('click', function(e){

            var $this = $(this);
            if(!$this.closest('.mobile-inner-menu-item').hasClass('open')){
                e.preventDefault();
                console.log('open');
                $('.mobile-inner-menu-item').removeClass('open');
                $this.closest('.mobile-inner-menu-item').addClass('open');
            }
            else {
                console.log('close');
                $this.closest('.mobile-inner-menu-item').removeClass('open');
            }
        });

        $('.js-close-third-menu').on('click', function(){
            $('.mobile-inner-menu-item').removeClass('open');
        });

        if($(document).width() < 940){
            moduleApp.innerMobileMenu();
            if($('.mobile-inner-menu-item.active').length > 0){
                $('.mobile-wrapper-inner-menu').animate({'scrollLeft': $('.mobile-wrapper-inner-menu').scrollLeft() + $('.mobile-inner-menu-item.active').offset().left - 30}, 1500);
            }
        }
    },
    'innerMobileMenu': function(){
        var countWidthMenu = 0;

        $('.mobile-inner-menu-item').each(function(ind,elem){
            var $elem = $(elem);
            countWidthMenu = countWidthMenu + $elem.outerWidth(true);
        });

        $('.mobile-inner-menu').css('width', countWidthMenu+'px');
    },
    'callMe': function(){
        $('.js-call-me').on('click', function(){
            var template = $('.js-call-me-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //$.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendMessage': function(){
        $('.js-send-message').on('click', function(){
            var template = $('.js-send-message-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){
                setTimeout(function(){
                    $('.fancybox-inner .js-select').selectric();
                },300);
                if(device.ipad()) {
                    $('.fancybox-inner .js-select').on('change', function () {
                        $(document).scrollTop(0);
                    });
                }

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //setTimeout(function(){
                    //    $.fancybox.update();
                    //}, 600);

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendEmail': function(){
        $('.js-form-email').on('click', function(){
            var $this = $(this),
                $formContainer = $this.closest('form');

            moduleApp.popupOpen($formContainer, false, function(){
                var value = $formContainer.serializeArray(),
                    urlAjax = $formContainer.attr('action');

                moduleApp.ajaxSendForm(value,urlAjax);
            });
        });
    },
    'ajaxSendForm': function(value,urlAjax){
        $.ajax({
            url: urlAjax,
            data: value,
            type: 'POST',
            dataType: 'json',
            success: function (result) {
                console.log(result.popup_id);
                //console.log($('#'+result.popup_id));
                var resultMessage = $('#'+result.popup_id).html();
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            },
            error: function (error) {
                var resultMessage = '<div class="result-message"><h3>Ошибка</h3><p>'+error+'</p></div>';
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            }
        });
    },
    'parallaxTeam': function(){
        if($('.animate-layer-element').length){
            var scrolled = $(window).scrollTop();
            var countSrolled = (0-(scrolled*.15));
            $('.animate-layer-element img').each(function(ind,elem){
                //var transformZ = 'translateZ(0.000' + ind + 'px)';
                $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
            });

            $(window).bind('scroll',function(){
                moduleApp.parallaxScroll();
            });
        }

        // $('.team-production-sleder').addClass('active');

        // $('.animation-slide-element').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        //     if(visiblePartY == 'both' || visiblePartY == 'bottom') {
        //         $('.animation-slide-element').addClass('active');
        //     }
        // });

        $('.swiper-container').on('inview', function (event, isInView, visiblePartX, visiblePartY) {

            if(isInView){
                $(this).find('.animation-slide-element').addClass('active');
            }
        });
    },
    'parallaxScroll': function(){
        var scrolled = $(window).scrollTop();
        var countSrolled = (0-(scrolled*.15));
        $('.animate-layer-element img').each(function(ind,elem){
            $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
        });
    },
    'resizeWindow': function(){
        $(window).resize(function(){
            var widthWindow = $(document).width();

            if(widthWindow < 930){
                if(!$('.tabs-controls').hasClass('mobile')){
                    $('.tabs-controls').each(function(ind,elem){
                        var $this = $(elem);
                        var length = 0;
                        $this.find('.tabs-controls-item').each(function(index,element){
                            length = length + $(element).outerWidth(true);
                        });

                        $this.addClass('mobile');
                        $this.css('width', length+'px');
                    });
                }

                moduleApp.innerMobileMenu();

                if(!$('.adaptive-table').parent('.container-scroll-table')){
                    $('.adaptive-table').wrap('<div class="wrapper-scroll-table"><div class="container-scroll-table"></div> </div>')
                }
            }
            if(widthWindow > 930){
                if($('.tabs-controls').hasClass('mobile')) {
                    $('.tabs-controls').each(function (ind, elem) {
                        var $this = $(elem);
                        $this.css('width', '100%');
                        $this.removeClass('mobile');
                    });
                }
            }

        });
    },
    'validSearch': function(){
        $('.js-search-btn').on('click', function(e){
            var $this = $(this);
            var $input = $this.siblings('.js-search-input');
            if($input.val().length < 3){
                e.preventDefault(e);
            }
        })
    },
    'brands': function(){
        $('.js-brand-item').on('click', function(){
            if($(this).hasClass('active')){
                $('.js-brand-item').removeClass('active');
            }
            else{
                $('.js-brand-item').removeClass('active');
                $(this).addClass('active');
            }
        });
    },
    'pageNews': function(){

        var stopFlagNews = true,
            filterValue = '',
            lang = $('.site_lang').html(),
            ie9 = false

        if($('html').hasClass('ie9')){
            ie9 = true;
        }

        if($('.ajax-wrapper-news').length > 0){

            if($('.actual-news-wrapper').length == 0){
                var $activeCard = $('.item-news-card.active'),
                    id = $activeCard.find('.js-name-news').attr('data-id'),
                    link = $activeCard.find('.js-name-news').attr('data-link'),
                    nextNews = $activeCard.find('.js-name-news').attr('data-next'),
                    prevNews = $activeCard.find('.js-name-news').attr('data-prev');

                sendNews(id,link,nextNews,prevNews,filterValue);
            }
        }

        $('.mCustomScrollbar-list-news').mCustomScrollbar({
            axis:"y",
            theme:"dark",
            callbacks:{
                onTotalScroll: function(){
                    if($('.js-info-news-list').length > 0){

                        stopFlagNews = false;

                        var page = $('.js-info-news-list').attr('data-page');
                        filterValue = $('.js-search-news-text').val();

                        $('.js-info-news-list').remove();

                        $.ajax({
                            url: '/ajax/news-list.php',
                            data: {
                                PAGEN_1: page,
                                filter: filterValue
                            },
                            type: 'POST',
                            dataType: 'html',
                            success: function (result) {
                                addListNews(result);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }
                }
            }
        });

        $('.js-search-news-btn').on('click', function(e){
            e.preventDefault();

            //if($('.js-search-news-text').val().length > 3){
                var value = $('.js-search-news-text').val(),
                    lang = $('.site_lang').html();
                $.ajax({
                    url: '/ajax/filter-news.php',
                    data: {
                        value: value,
                        site_lang: lang
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function (result) {
                        addNewNews(result.actualNews);
                        changeListNews(result.listNews);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            //}
        });

        $(document).on('click', '.js-name-news', function(e){
            if(!ie9){
                e.preventDefault();

                $('.item-news-card').removeClass('active');
                $(this).closest('.item-news-card').addClass('active');
                var id = $(this).attr('data-id'),
                    link = $(this).attr('data-link'),
                    nextNews = $(this).attr('data-next'),
                    prevNews = $(this).attr('data-prev');

                filterValue = $('.js-search-news-text').val();

                sendNews(id, link, nextNews, prevNews, filterValue);
            }
        });

        $(document).on('click', '.js-next-news', function(e){
            e.preventDefault();
            var $listNews = $('.list-news'),
                indexActiveElement = $listNews.find('.active').index() + 1;
                
            $listNews.find('.active').removeClass('active');
            if(indexActiveElement > $listNews.find('.item-news-card').length - 1){
                var page = $('.js-info-news-list').attr('data-page');
                filterValue = $('.js-search-news-text').val();

                $('.js-info-news-list').remove();

                $.ajax({
                    url: '/ajax/news-list.php',
                    data: {
                        PAGEN_1: page,
                        filter: filterValue
                    },
                    type: 'POST',
                    dataType: 'html',
                    success: function (result) {
                        addListNews(result);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            setTimeout(function(){
                $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');
            }, 400);


            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');

            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);

            setTimeout(function(){
                $('html,body').animate({'scrollTop': $('.ajax-wrapper-news').offset().top}, 500);
            },700);

        });

        $(document).on('click', '.js-prev-news', function(e){
            e.preventDefault();

            var $listNews = $('.list-news');
            var indexActiveElement = $listNews.find('.active').index() - 1;
            $listNews.find('.active').removeClass('active');
            $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');

            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');
            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);
        });

        $(document).on('click', '.tag-list span', function(e){
            e.preventDefault();

            var value = $(this).html();
                lang = $('.site_lang').text();

            $.ajax({
                url: '/ajax/filter-news.php',
                data: {
                    value: value,
                    site_lang: lang
                },
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    addNewNews(result.actualNews);
                    changeListNews(result.listNews);
                    $('.js-search-news-text').val(value);
                    $('.wrapper-search-news').find('.form-input-search').addClass('filled');
                },
                error: function (error) {
                    console.log(error);
                }
            });

        });

        function sendNews(id, link, nextNews, prevNews, filterValue){
            var lang = $('.site_lang').html();
            $.ajax({
                url: '/ajax/news.php',
                 data: {
                     id: id,
                     link: link,
                     nextNews: nextNews,
                     prevNews: prevNews,
                     filter: filterValue,
                     site_lang: lang,
                 },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    _pageShare = {
                        'link': result._pageShare.link,
                        'title': result._pageShare.title,
                        'description': result._pageShare.description,
                        'twitter_description': result._pageShare.twitter_description,
                        'image': result._pageShare.image
                    };
                    addNewNews(result.actualNews, link);
                    changeMetaTeg();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        };

        function addNewNews(resultNews, linkNews){
            $('.actual-news-wrapper').addClass('deleted');
            $('.ajax-wrapper-news').append(resultNews);
            if(linkNews != ''){
                changeUrlNews(linkNews);
            }
            setTimeout(function() {
                $('.actual-news-wrapper.deleted').remove();
                $('.actual-news-wrapper.desabled').removeClass('desabled');
                updateScrollbar();
            }, 500);
        };

        function addListNews(newsListAdd){
            $('.list-news').append(newsListAdd);
            stopFlagNews = true;
        }

        function changeListNews(resultListNews){
            $('.list-news').addClass('deleted');
            $('.wrapper-list-news .mCSB_container').append(resultListNews);
            setTimeout(function() {
                $('.list-news.deleted').remove();
                $('.list-news.desabled').removeClass('desabled');
                updateScrollbar();
            }, 700);
        };

        function updateScrollbar(){
            setTimeout(function() {
                $('.mCustomScrollbar ').mCustomScrollbar("update");
                $('.mCustomScrollbar-list-news').mCustomScrollbar("update");
            }, 300);
        };

        function changeMetaTeg(){
            $('#og_url').attr('content', _pageShare.link);
            $('#og_title').attr('content', _pageShare.title);
            $('#og_description').attr('content', _pageShare.description);
            $('#og_image').attr('content', _pageShare.image);
        };

        function changeUrlNews(linkNews){
            if($('.page-news').length > 0 && !$('html').hasClass('ie9')){
                history.pushState('', '', linkNews);
            }
        }
    },
    'startupMessage':function(){
        var title = '';
        if (appConfig.startupMessage.title && appConfig.startupMessage.message) {
            var template = '<div class="fb-popup-default">';
            template += '<div class="fbp-title">'+appConfig.startupMessage.title+'</div>';
            template += '<div class="fbp-message">'+appConfig.startupMessage.message+'</div>';
            template += '</div>';

            $.fancybox({
                content: template,
                wrapCSS: 'message-wrapper-popUp',
                padding: [0],
                fitToView: false,
                openEffect: 'elastic',
                closeEffect: 'elastic',
                maxWidth: 500,
                maxHeight: 380,
            });
        }
    },
    'subscribe': function(){
        moduleApp.formValidation($('.js-form-subscribe'), function(e){
            console.log(e);
            var $this = $(e),
                url = $this.attr('action'),
                value = $this.serializeArray();
            console.log(value);
            $this.find('.delivery-input').val('');
            $('.filled').removeClass('filled');
            moduleApp.ajaxSendForm(value,url);
            $this.find('.js-form-subscribe').removeClass('disabled');
        });

        $('.delivery-input').focusout(function(){
            $(this).closest('.form-input').removeClass('show-error');
        });
    },
    'searchHeader': function(){
        $('.js-search-header-btn').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $thisForm = $this.closest('form');

            if(!$this.hasClass('active')){
                $this.addClass('active');
                $this.closest('form').addClass('activated');
            }
            else{

                if($thisForm.find('.search-header-text').val().length > 3){
                    $thisForm.submit();
                }
                else{
                    $thisForm.find('.search-header-text').val('');
                    $this.removeClass('active');
                    $this.closest('form').removeClass('activated');
                }
            }
        });
    },
    'socialShare': function(){
        $(document).on('click', '[data-service]', function(e){
            e.preventDefault();

            var $this = $(this),
                shareService = $this.attr('data-service'),
                windowLink = '';

            windowLink += 'http://share.yandex.ru/go.xml?service=' + shareService;
            windowLink += '&title=' + _pageShare.title;

            if (shareService=='twitter') {
                windowLink += ' ' + _pageShare.twitter_description;
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
            }
            else if (shareService=='livejournal'){
                windowLink = 'http://www.livejournal.com/update.bml?';
                windowLink += 'subject=' + _pageShare.title;
                windowLink += '&event=' + _pageShare.description + ' <a href="' + _pageShare.link + '">' + _pageShare.link + '</a>';
            } else {
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
                windowLink += '&description=' + _pageShare.description;
                windowLink += '&image=' + _pageShare.image;
            }

            window.open(windowLink,'','toolbar=0,status=0,width=625,height=435');

        });
    },
    'loupeCursore': function(){
        (function ($) {
            $.fn.loupe = function (arg) {
                var options = $.extend({
                    loupe: 'loupe',
                    width: 200,
                    height: 150
                }, arg || {});

                return this.length ? this.each(function () {
                    var $this = $(this), $big, $loupe,
                        $small = $this.is('img') ? $this : $this.find('img:first'),
                        move, hide = function () { $loupe.hide(); },
                        time;

                    if ($this.data('loupe') != null) {
                        return $this.data('loupe', arg);
                    }

                    move = function (e) {
                        var os = $small.offset(),
                            sW = $small.outerWidth(),
                            sH = $small.outerHeight(),
                            oW = options.width / 2,
                            oH = options.height / 2;

                        if (!$this.data('loupe') ||
                            e.pageX > sW + os.left + 10 || e.pageX < os.left - 10 ||
                            e.pageY > sH + os.top + 10 || e.pageY < os.top - 10) {
                            return hide();
                        }

                        time = time ? clearTimeout(time) : 0;

                        $loupe.show().css({
                            left: e.pageX - oW,
                            top: e.pageY - oH
                        });
                        $big.css({
                            left: -(((e.pageX - os.left) / sW) * $big.width() - oW)|0,
                            top: -(((e.pageY - os.top) / sH) * $big.height() - oH)|0
                        });
                    };

                    $loupe = $('<div />')
                        .addClass(options.loupe)
                        .css({
                            width: options.width,
                            height: options.height,
                            position: 'absolute',
                            overflow: 'hidden'
                        })
                        .append($big = $('<img />').attr('src', $this.attr($this.is('img') ? 'src' : 'href')).css('position', 'absolute'))
                        .mousemove(move)
                        .hide()
                        .appendTo('body');

                    $this.data('loupe', true)
                        .mouseenter(move)
                        .mouseout(function () {
                            time = setTimeout(hide, 10);
                        });
                }) : this;
            };
        }(jQuery));
    },
    'catalogPage': function(){
        var loupeConfig = {
            width: 260,
            height: 260,
            loupe:'quick-view-loupe'
        },
            ajax_request = "";

        if($('.js-checkbox-filter:checked').length > 0){
            createElementsFilter();
            serializeArray();
        }

        $(document).on('click', '.js-quick-view', function(e){
            e.preventDefault();
            var template = $(this).find('.js-quick-view-block').html();

            var configFancy = {
                content: template,
                wrapCSS: 'fb-card-window',
                padding: 0,
                margin: 10,
                fitToView: false,
                openEffect: 'drop',
                closeEffect: 'drop',
                scrolling: 'auto',
                maxWidth: 1100,
                autoWidth: true,
                autoHeight: true,
                'beforeShow': function () {
                    $('.fancybox-inner .js-loupe-container img').loupe(loupeConfig);
                },
            }

            $.fancybox(configFancy);
        });

        $('.js-filter-element').on('change', function(){
            serializeArray();
        });

        $('.js-sort-element').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $thisInput = $this.find('input');

            if($this.hasClass('up') || $this.hasClass('down')){

                if($this.hasClass('down')){
                    $this.removeClass('down').addClass('up');
                    $thisInput.attr('value','asc');
                }
                else{
                    $this.removeClass('up').addClass('down');
                    $thisInput.attr('value','desc');
                }
            }
            else{
                $('.js-sort-element').each(function(ind,elt){
                    $(elt).find('input').attr('value','off');
                    $(elt).removeClass('up down')
                });
                $this.addClass('up');
                $thisInput.attr('value','asc');
            }
            setTimeout(function(){
                serializeArray();
            },100);
        });

        $(document).on('click', '.js-sf-delete', function(){
            var $this = $(this);
            deleteElementFilter($this);
        });

        $('.js-checkbox-filter').on('change', function(){
            if($(this).is(':checked')){
                createElementFilter($(this).attr('data-name'),$(this).attr('id'));
            }
            else{
                var idElement = $(this).attr('id'),
                    $element = $('.catalog-sf-item').find('.js-sf-delete').filter(function(){
                        return $(this).attr('data-id') == idElement;
                    });
                deleteElementFilter($element);
            }
        });

        $('.js-sort-clear-filter').on('click', function(){
            $('input[type=checkbox]').prop("checked", false);
            $('.catalog-sub-filter-wrapper').addClass('disabled');
            setTimeout(function(){
                $('.catalog-sub-filter-wrapper').html('');
                $('.catalog-sub-filter-wrapper').removeClass('disabled');
                serializeArray();
            },300);
        });

        $('.js-catalog-filter-trigger').on('click', function(){
            var $triggerElement = $(this),
                $parentTrigger = $triggerElement.parents('.catalog-fl'),
                listItem = $parentTrigger.find('.catalog-fl-inner'),
                duration = 300;

            if($triggerElement.hasClass('active')){
                $triggerElement.removeClass('active');
                listItem.stop(true, true).slideUp(duration);
            }
            else{
                $triggerElement.addClass('active');
                listItem.stop(true, true).slideDown(duration);
            }
        });

        //открытие и закрытие мобильного фильтра
        $(document).on('touchstart', function(e){
            $(".filter-global-list").removeClass("active");
        });
        //открытие и закрытие мобильного фильтра
        $(".filter-global-list").on('touchstart', function(e){
            e.stopPropagation();
        });
        //открытие и закрытие мобильного фильтра
        $('.js-mm-trigger-filter').on('touchstart', function(e){
            e.preventDefault();
            e.stopPropagation();
            $('.filter-global-list').toggleClass('active');
            // alert('filter4');
        });

        //пагинация
        $(document).on('click', '.js-ajax-product-pagination', function(e){
            e.preventDefault();
            var  value = $('.js-catalog-global-wrapper').serializeArray(),
                pagin = $(this).attr('data-pagen_1');

            sendAjax(value,pagin);
        });

        function serializeArray(){
            value = $('.js-catalog-global-wrapper').serializeArray();
            //console.log(value);
            sendAjax(value);
        };

        function sendAjax(value,pagin){
            if (ajax_request) {
                ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/catalog-global-filter.php',
                data: {
                    q: value,
                    PAGEN_1: pagin
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    bodyAddClass();
                    scrollTop();
                    deletedPagination();
                    deletedListElement();
                    setTimeout(function(){
                        addNewListElement(result.poducts);
                    },300);
                    setTimeout(function(){
                        addNewPagination(result.paginator);
                    },800);
                },
                error: function (error) {
                }
            });
        };

        function createElementFilter(name,id){
            $('.catalog-sub-filter-wrapper').append('<div class="catalog-sf-item"><span class="catalog-sf-title">'+name+'</span><div class="catalog-sf-delete js-sf-delete" data-id="'+id+'"></div></div>')
        };

        function createElementsFilter(){
            $('.js-checkbox-filter:checked').each(function(ind,elt){
                var name = $(elt).attr('data-name'),
                    id = $(elt).attr('id');
                $('.catalog-sub-filter-wrapper').append('<div class="catalog-sf-item"><span class="catalog-sf-title">'+name+'</span><div class="catalog-sf-delete js-sf-delete" data-id="'+id+'"></div></div>')
            });
        };

        function deleteElementFilter($element){
            var $wrapper = $element.closest('.catalog-sf-item'),
                name = $element.attr('data-id');

            $wrapper.addClass('deleted');
            $('.catalog-filter-list-wrapper').find('#'+name).removeAttr("checked").prop("checked", false);
            setTimeout(function(){
                $wrapper.remove()
            },300);
            serializeArray();
        };

        function addNewListElement(ListElement){
            $('.catalog-content-wrapper').append(ListElement);
            setTimeout(function(){
                $('.catalog-cards-list.append').removeClass('append');
            },300);
        };

        function deletedListElement(){
            $('.catalog-content-wrapper .catalog-cards-list').addClass('deleted');
            setTimeout(function(){
                $('.catalog-cards-list.deleted').remove();
            },300);
        };

        function scrollTop(){
            $('html,body').animate({'scrollTop': $('.catalog-main-block-wrapper').offset().top - 200}, 400);
        };

        function addNewPagination(newPagination){
            $('.catalog-main-block-wrapper').append(newPagination);
            setTimeout(function() {
                $('.pagination-wrapper.append').removeClass('append');
            },300);
        };

        function deletedPagination(){
            $('.catalog-main-block-wrapper .pagination-wrapper').addClass('deleted');
            setTimeout(function(){
                $('.pagination-wrapper.deleted').remove();
            },300);
        };

        function bodyAddClass(){
            $('body').addClass('overflow-catalog');
            setTimeout(function(){
                $('body').removeClass('overflow-catalog');
            },400);
        };

        if(device.ipad()){
            $('html').addClass('ipad-tablet');
        }
        else{
            $('html').removeClass('ipad-tablet');
        }
    },
    'counterProduct': function(){
        $(document).on('click', '.js-card-counter-plus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-card-counter-value');


            if(parseInt($input.val()) < $this.attr('data-max')){
                $input.val(parseInt($input.val()) + 1);
            }

            if(parseInt($input.val()) == $this.attr('data-max')){
                $this.addClass('disabled');
            }

            if(parseInt($input.val()) > 1){
                $this.siblings('.js-card-counter-minus').removeClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('click', '.js-card-counter-minus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-card-counter-value');

            if(!$this.hasClass('disabled')){
                $input.val(parseInt($input.val()) - 1);

                if(parseInt($input.val()) < $this.siblings('.js-card-counter-plus').attr('data-max')){
                    $this.siblings('.js-card-counter-plus').removeClass('disabled');
                }
            }

            if(parseInt($input.val()) == 1){
                $this.addClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('change', '.js-card-counter-value', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(parseInt($this.val()) > parseInt($this.siblings('.js-card-counter-plus').attr('data-max'))){
                $this.val($this.siblings('.js-card-counter-plus').attr('data-max'));
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($this.val()));
            }
        });
    },
    'detailPage': function(){
        var loupeConfig = {
            width: 260,
            height: 260,
            loupe:'quick-view-loupe-detail'
        };

        //лупа на деталке
        if(!device.mobile()){
            $('.js-loop-detail img').loupe(loupeConfig);
        }

        //переключалка изображений на деталке
        $('.js-detail-main-slide').on('click', function(){
            var $this = $(this),
                urlMediumImage = $this.attr('data-mediumImg');
                //urlBigImage = $this.attr('data-bigImg');
            if(!$this.hasClass('active')){
                //$('.big-detail-image').attr('data-bigImg', urlBigImage);
                $('.big-detail-image img').addClass('deleted');
                $('.js-detail-main-slide').removeClass('active');
                $this.addClass('active')
                setTimeout(function(){
                    $('.big-detail-image .deleted').remove();
                    $('.big-detail-image').append('<img src="'+urlMediumImage+'" class="append">');
                    setTimeout(function(){
                        $('.big-detail-image .append').removeClass('append');
                        if(!device.mobile()) {
                            $('.js-loop-detail img').loupe(loupeConfig);
                        }
                    },300);
                },300);

                if($('.quick-view-loupe').length > 0){
                    $('.quick-view-loupe').remove();
                }
            }
        });

        //$('.js-fancy-image').on('click', function(){
        //   console.log('click');
        //
        //    var template = '<div class="loop-image-detail"><img src="'+$(this).attr("data-bigImg")+'"></div>';
        //    moduleApp.popupOpen(template, 'fb-noscroll-window');
        //});

        //выставление рейтинга в форме
        $(document).on('click', '.fancybox-inner .form-rating-item', function(){
            var $this = $(this),
                $stars = $this.siblings(),
                count = $stars.length + 1,
                index = $this.index();

            $stars.removeClass('active');
            $this.addClass('active');
            for(var i = 0; i < count; i++){
                if(index <= i ){
                    $($stars[i]).addClass('active');
                }
            }

            var activeCount = $('.fancybox-inner .js-rating .active').length;
            $('.fancybox-inner .js-rating-count').val(activeCount)
        });

        //оставить отзыв
        $('.js-reviews').on('click', function(){
            var template = $('.js-review-form').html();
            moduleApp.popupOpen(template,'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        //полезные ли комментарии
        $('.js-review-help').on('click', function(){
            if($(this).attr('data-checked') != 'checked'){
                var $this = $(this),
                    idProduct = $this.attr('data-id'),
                    typeReview = $this.attr('data-type'),
                    countReview = parseInt($this.attr('data-count'));

                countReview++;
                $this.attr('data-count',countReview);
                $this.find('span').html('('+countReview+')');
                $this.attr('data-checked','checked');
                $this.siblings('.js-review-help').attr('data-checked','checked');

                $.ajax({
                    url:'/ajax/comments/comment-like.php',
                    data:{
                        id: idProduct,
                        type: typeReview
                    },
                    type: 'POST',
                    dataType:'JSON',
                    success: function(result){
                        console.log(result);
                    }
                });
            }
        });

        //ajax пагинация
        $(document).on('click', '.js-ajax-comment-pagination', function(e){
            e.preventDefault();

            var $this = $(this),
                idProduct = $this.attr('data-product_id'),
                pagenPage = $this.attr('data-pagen_1');

            $.ajax({
                url: '/ajax/comments/comments-pagination.php',
                data:{
                    product_id:idProduct,
                    PAGEN_1: pagenPage
                },
                type: 'POST',
                dataType:'JSON',
                success:function(result){

                    console.log(result);
                    $('.reviews-list').addClass('deleted');
                    $('.reviews-list-wrapper').append(result.comments);
                    setTimeout(function(){
                        $('.reviews-list.deleted').remove();
                        $('.reviews-list.active').removeClass('active');
                        $('.reviews-right-column .pagination-wrapper').addClass('deleted');
                    },300);
                    setTimeout(function(){
                        $('html,body').animate({'scrollTop': $('.reviews-list-wrapper').offset().top - ($(window).outerHeight()/3)}, 500);
                        $('.reviews-right-column .pagination-wrapper').remove();
                        $('.reviews-right-column ').append(result.paginator);
                        setTimeout(function(){
                            $('.reviews-right-column .pagination-wrapper').removeClass('append');
                        },200);
                    },700);
                }
            });
        });

        //добавление в избранное
        $('.js-liked').on('click', function(){

            $(this).toggleClass('active');
            var idProduct = $(this).attr('data-product_id');

            $.ajax({
                url:'/ajax/store/add_to_favorites.php',
                data:{
                    id: idProduct
                },
                type: 'POST',
                dataType:'JSON',
                success:function(result){
                    console.log(result);
                }
            });
        });

        //социальное меню для адаптивки
        $('.js-social-btn').on('click', function(e){
            e.preventDefault();
            $(this).closest('.social-menu').toggleClass('open');
        });
    },
    'shoppingCart': function(){
        $(document).on('click', '.js-add-cart-product', function(){
            var $this = $(this);
                //idProduct = $this.attr('data-id'),
                //countProduct = $this.siblings('.js-card-counter-value');
            addProductCart($this);
            clearCounterProduct($this);
        });

        //добавление в корзину
        function addProductCart($product){
            var idProduct = $product.attr('data-id'),
                $cardBuy = $product.closest('.js-card-buy'),
                countProduct = $cardBuy.find('.js-card-counter-value').val(),
                stateProduct = 'add';
            sendAjaxCart(idProduct, countProduct, stateProduct, $product);
        };

        //удаление из корзины
        function removeProductCart($product){
            var idProduct = $product.attr('data-id'),
                countProduct = 0,
                stateProduct = 'remove';
            sendAjaxCart(idProduct, countProduct, stateProduct);
        };

        //изменение малой корзины на странице
        function changeSmollChoppingCart(template){
            console.log(template);
        };

        //отправка аякса в корзину
        function sendAjaxCart(idProduct, countProduct, stateProduct, $product){
            console.log(idProduct);
            console.log(countProduct);
            console.log(stateProduct);
            $.ajax({
                url:'/ajax/store/add_to_cart.php',
                data:{
                    PRODUCT_ID: idProduct,
                    QUANTITY: countProduct,
                    state: stateProduct
                },
                type: 'POST',
                dataType:'json',
                success: function(result){
                    console.log(result);
                    animationBasket($product);
                }
            });
        };

        //сбрасываем значение счетчика и цену товара
        function clearCounterProduct($product){
            var $cardBuy = $product.closest('.js-card-buy'),
                price = $cardBuy.find('.js-price-count').attr('data-price');

            $cardBuy.find('.js-price-count span').html(price);
            $cardBuy.find('.js-card-counter-value').val('1');
        };

        function animationBasket($product){
            console.log('animation');
            var imageUrl = $product.attr('data-img');
            var leftPos = 0,
                topPos = 0,
                leftBasketPos = $('.shop-basket').offset().left,
                topBasketPos = $('.shop-basket').offset().top;
            if($('.page-detail-tea').length > 0){
                topPos = $('.detail-slider-wrapper').offset().top;
                leftPos = $('.detail-slider-wrapper').offset().left;
            }

            if($('.page-catalog-tea').length > 0){
                topPos = $product.offset().top - 10;
                leftPos = $product.offset().left + ($product.width()/2);
            }

            $('body').append('<div class="animation-product" style="left: '+leftPos+'px; top: '+topPos+'px;" ><img src="'+imageUrl+'" alt=""></div> ');

            setTimeout(function(){
                $('.animation-product').addClass('animation');
                $('.animation-product').css({left:leftBasketPos, top:topBasketPos});
            },100);
            setTimeout(function(){
                // $('.animation-product').remove();
            },450);
        };
    },
    'lkHistory': function(){
        $(document).on('click', '.js-info-order', function(){
            var template = $(this).siblings('.info-ordewr-container').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window');
        });

        $('.js-calculation').on('click', function(){

            var user = $(this).attr('data-user'),
                urlAjax = $(this).attr('data-url');

            $.ajax({
                url: urlAjax,
                data: {user:user},
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    moduleApp.popupOpen($('#'+result.popup_id));
                },
                error: function (error) {
                    console.log('error' + error);
                }
            });
        });

        $(document).on('change', '.js-add-print-row', function(){            

            var tempalte = '',
                $printWrapper = $('.print-table-wrapper');

            $('.js-add-print-row:checked').closest('tr').each(function(ind,elt){
                console.log($(elt));
                tempalte = tempalte + '<tr>' +$(elt).html() + '</tr>';
            });

            $printWrapper.find('tbody').empty();
            $printWrapper.find('tbody').append(tempalte);

            if($('.js-add-print-row:checked').length < 1){
                $printWrapper.find('tbody').append($('.history-table.ht tbody').html());
            }

            $printWrapper.find('tbody tr').each(function(ind,elt){
                $(elt).find('td:last').empty();
            });


        });

        $(document).on('click', '.js-period-print', function(){
            window.print();
        });
    },
    'lkProfile': function(){
        $('.js-write-manager').on('click', function(){
            var template = $('.js-manager-container').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        $('.js-period-filter').on('change', function(){
            console.log('change');
            var $thisForm = $(this),
                value = $thisForm.serializeArray(),
                urlAjax = $thisForm.attr('action');
            // console.log(value);
            // console.log(urlAjax);
            ajaxSendFilter(value,urlAjax);
        });

        $.datetimepicker.setLocale('ru');

        $('.datetimepicker').each(function(ind,elt){
            
            $(elt).datetimepicker({ 
                lang:'ru',
                timepicker:false,
                format:'d.m.Y',
                onClose: function(ct,$i){
                    console.log($i);
                }
            });
        });

        $('.js-reorder-btn').on('click', function(){
           var $this = $(this),
               dataOrder = $this.attr('data-order');
           console.log(dataOrder);

            $.ajax({
                url: '/ajax/store/reorder.php',
                data: {order:dataOrder},
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        function ajaxSendFilter(value,urlAjax){
            $.ajax({
                url: urlAjax,
                data: value,
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    console.log(result.table);
                    $('.js-wrapper-ajax-filter tbody').empty();
                    $('.js-wrapper-ajax-filter tbody').append(result.table);
                },
                error: function (error) {
                    console.log('error' + error);
                }
            });
        };

        // function errorOrder(){}

        moduleApp.formValidation($('.js-book-form'), function($form){
            // console.log($form);
            $form.submit();
        });
    },
    'lkPersonal': function(){
        $('.js-save-change').on('click', function(){
            $('.personal-global-form').submit();
        });

        $('.js-add-person').on('click', function(){
            console.log('add');
            var template = $('.js-new-manager-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function($form){
                    $.fancybox.update();
                    
                    $form.submit();
                });
            });
        });

        $('.js-new-pass').on('click', function(){
            var template = $('.js-form-new-pass').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function($form){
                    $.fancybox.update();
                    
                    $form.submit();
                });
            });
        });

        $('.js-add-logo').on('change', function(){
            $('.personal-logotip').addClass('disabled');
        })
    },
    'handleFileSelect': function(evt){

        var $this = $(this);
        var $wrapperFileInput = $this.closest('.js-file-wrapper');
        $wrapperFileInput.find('.file-name').remove();
        var name = '<span class="file-name">'+$(this).val().split('\\').pop().split('/').pop()+'</span>';
        // var deletes = '<div class="delete-file js-delete-file"></div>';
        var size = '';
        // console.log($wrapperFileInput);

        // if (window.File) {
        //     var files = evt.target.files;
        //     var f = files[0];
        //     var reader = new FileReader();

        //     reader.readAsDataURL(f);
        //     reader.onload = (function (theFile) {
        //         size = '<div class="file-size">'+'('+Math.floor(theFile.size/1024) +'кб)'+'</div>';
        //     })(f);
        // }

        // $listFile.find('label').removeClass('active');
        // if($this.hasClass('one-file')){
        //     $listFile.append('<label class="active"><input type="file" class="js-add-file one-file" disabled name="File[]" data-validate="file"></label>');
        // }
        // else{
        //     if($listFile.find('label').length < 3){
        //         $listFile.append('<label class="active"><input type="file" class="js-add-file" name="File[]"></label>');
        //     }
        //     else{
        //         $listFile.append('<label class="active"><input type="file" class="js-add-file" disabled name="File[]"></label>');
        //     }
        // }

        $wrapperFileInput.append(name);
    },
    'addFile':function(){
        $(document).on('change', '.js-add-file', moduleApp.handleFileSelect);

        // $(document).on('click', '.js-delete-file', function(){
        //     var $this = $(this);
        //     var $formFile = $this.closest('.form-file');
        //     var $fileItem = $this.parent('.file-item');
        //     var index = $fileItem.index();
        //     var $listLabel = $formFile.find('.js-list-file');

        //     console.log($listLabel.find('.active'));
        //     $listLabel.find('.active').find('input').removeAttr('disabled');
        //     $listLabel.find('label').eq(index).remove();
        //     $fileItem.remove();
        // });
    },
    'newOrder': function(){
        $('.js-filter-order').on('change', function(){
            // sendAjax($(this).val());
            $(this).closest('form').submit();
        });

        // аякс для филтра
        // function sendAjax(value,pagin){
        //     if (ajax_request) {
        //         ajax_request.abort();
        //     }
        //     ajax_request = $.ajax({
        //         url: '/ajax/lk/catalog-lk-filter.php',
        //         data: {
        //             q: value
        //         },
        //         type: 'POST',
        //         dataType: 'JSON',
        //         success: function (result) {
        //             addElementTable(result);
        //         },
        //         error: function (error) {
        //         }
        //     });
        // };

        // смена элементов таблицы
        // function addElementTable(result){
        //     $('.table-new-order tbody').empty();
        //     $('.table-new-order tbody').append(result);
        // };

        // валидация изменение значение кол-ва товара
        $(document).on('change', '.js-count-new-order', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            var id = $this.attr('data-id');
            var price = $this.closest('tr').find('.js-price').text().replace(/,/, '.');
            var $newPrice = String((price * $this.val()).toFixed(2)).replace(/\./, ',');

            $this.closest('tr').find('.js-all-price').html($newPrice);
        });

        // добавлене к заказу
        $(document).on('click', '.js-add-to-order', function(){
            var value = $(this).attr('data-id'),
                count = $(this).closest('tr').find('.js-count-new-order').val();
            $.ajax({
                url: '/ajax/store/add_to_cart.php',
                data: {
                    PRODUCT_ID: value,
                    QUANTITY: count
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                },
                error: function (error) {
                }
            });
        });
    },
    'regionalOrder': function(){
        ajax_request = "";

        $(document).on('change', '.js-count-order', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            var id = $this.attr('data-id'),
                name = $this.attr('data-name'),
                value = $this.val(),
                basketAction = 'recalculate',
                idProduct = $this.attr('data-product_id');

            sendAjaxOrder(id,value,name, basketAction, idProduct, $this);
        });

        $('.js-deleted-in-order').on('click', function(){

            var $this = $(this),
                id = $this.attr('data-id'),
                basketAction = 'delete',
                value = 0,
                name = id;

            $this.closest('tr').remove();

            sendAjaxOrder(id,value,name, basketAction);
        });

        function sendAjaxOrder(id,value,name,basketAction, idProduct, $element){
            if (ajax_request) {
                // ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/recalculation_cart.php',
                data: {
                    id: id,
                    value: value,
                    name:name,
                    basketAction: basketAction,
                    product_id:idProduct
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    console.log(result);
                    changeCartAfterAjax(result);

                    if(basketAction == 'recalculate'){
                        changePriceElement($element);
                    }

                },
                error: function (error) {
                }
            });
        };

        function changePriceElement($element){
            var $this = $element,
                $thisRow = $this.closest('tr'),
                count = $thisRow.find('.js-count-order').val(),
                price = Number($thisRow.find('.js-price-sale').text());

            $thisRow.find('.js-all-price').html((price * count).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
            console.log($thisRow);
        };

        function changeCartAfterAjax(result){
            $('.js-total-price').html((result.ALL_PRICE_WITHOUT_DISCOUNT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-total-sale').html((result.DISCOUNT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-total-price-sale').html((result.TOTAL).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-marketing').html((result.MARKETING_SUPPORT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
        };
    },
    'startupMessage':function(){
        var title = '';
        if (appConfig.startupMessage.title && appConfig.startupMessage.message) {
            var template = '<div class="fb-popup-message">';
            template += '<h3 class="fb-title">'+appConfig.startupMessage.title+'</h3>';
            template += '<p class="fb-message">'+appConfig.startupMessage.message+'</p>';
            template += '</div>';

            $.fancybox({
                content: template,
                wrapCSS: 'fb-noscroll-window',
                padding: 0,
                margin: 10,
                fitToView: false,
                openEffect: 'drop',
                closeEffect: 'drop',
                scrolling: 'auto',
                maxWidth: 1100,
                autoHeight: true,
            });
        }
    },
    'basket': function(){
        $(document).on('click', '.js-basket-counter-plus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-count-basket'),
                id = $input.attr('data-id'),
                name = $input.attr('data-id'),
                basketAction = 'recalculate',
                idProduct = $input.attr('data-product_id'),
                value = $input.val() + 1;

            sendAjaxOrder(id,value,name,basketAction, idProduct, $this);

            if(parseInt($input.val()) < $this.attr('data-max')){
                $input.val(parseInt($input.val()) + 1);
            }

            if(parseInt($input.val()) == $this.attr('data-max')){
                $this.addClass('disabled');
            }

            if(parseInt($input.val()) > 1){
                $this.siblings('.js-basket-counter-minus').removeClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('click', '.js-basket-counter-minus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-count-basket'),
                id = $input.attr('data-id'),
                name = $input.attr('data-id'),
                basketAction = 'recalculate',
                idProduct = $input.attr('data-product_id'),
                value = $input.val() - 1;

            sendAjaxOrder(id,value,name,basketAction, idProduct, $this);

            if(!$this.hasClass('disabled')){
                $input.val(parseInt($input.val()) - 1);

                if(parseInt($input.val()) < $this.siblings('.js-basket-counter-plus').attr('data-max')){
                    $this.siblings('.js-basket-counter-plus').removeClass('disabled');
                }
            }

            if(parseInt($input.val()) == 1){
                $this.addClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('change', '.js-count-basket', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            if(parseInt($this.val()) > parseInt($this.siblings('.js-basket-counter-plus').attr('data-max'))){
                $this.val($this.siblings('.js-basket-counter-plus').attr('data-max'));
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($this.val()));
            }
        });

        $(document).on('click', '.js-deleted-in-basket', function(){

            var $this = $(this),
                id = $this.attr('data-id'),
                basketAction = 'delete',
                value = 0,
                name = id;

            $this.closest('tr').remove();

            sendAjaxOrder(id,value,name, basketAction);
        });

        $('.js-add-basket-product').on('click', function(){
            console.log('add to cart');
        });

        ajax_request = "";

        function sendAjaxOrder(id,value,name,basketAction, idProduct, $element){
            if (ajax_request) {
                ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/add_to_cart.php',
                data: {
                    id: id,
                    value: value,
                    name:name,
                    basketAction: basketAction,
                    product_id:idProduct
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    console.log(result);
                    console.log($element);
                },
                error: function (error) {
                }
            });
        };
    },
    'adaptiveTable': function(){
        var widthWindow = $(document).width();

        if(widthWindow < 930){
            $('.adaptive-table').wrap('<div class="wrapper-scroll-table"><div class="container-scroll-table"></div> </div>');
        }
    }
};

$(document).ready(function () {
    moduleApp.init();
});