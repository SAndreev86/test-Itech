var _pageShare;
var moduleApp = {
    'init': function () {
        moduleApp.pollifil();
        moduleApp.sliderSwiper();
        moduleApp.formValidation();
        moduleApp.contacts();
        moduleApp.tabs();
        moduleApp.alternativeAccordion();
        moduleApp.accordion();
        moduleApp.customSelect();
        moduleApp.elementForms();
        moduleApp.mobileMenu();
        moduleApp.callMe();
        moduleApp.sendMessage();
        // moduleApp.sendEmail();
        moduleApp.parallaxTeam();
        moduleApp.resizeWindow();
        moduleApp.validSearch();
        moduleApp.brands();
        moduleApp.pageNews();
        moduleApp.subscribe();
        moduleApp.searchHeader();
        moduleApp.socialShare();
    },
    'pollifil': function(){
        if (!("classList" in window.document.body)) {
            Object.defineProperty(Element.prototype, 'classList', {
                get: function() {
                    var self = this, bValue = self.className.split(" ")
                    bValue.add = function (){
                        var b;
                        for(var i in arguments){
                            b = true;
                            for (var j = 0; j<bValue.length;j++)
                                if (bValue[j] == arguments[i]){
                                    b = false;
                                    break
                                }
                            if(b)
                                self.className += (self.className?" ":"")+arguments[i]
                        }
                    };
                    bValue.remove = function(){
                        self.className = "";
                        for(var i in arguments)
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != arguments[i])
                                    self.className += (self.className?" " :"")+bValue[j]
                    };
                    bValue.toggle = function(x){
                        var b;
                        if(x){
                            self.className = "";
                            b = false;
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != x){
                                    self.className += (self.className?" " :"")+bValue[j];
                                    b = false;
                                } else b = true;
                            if(!b)
                                self.className += (self.className?" ":"")+x
                        } else throw new TypeError("Failed to execute 'toggle': 1 argument required");
                        return !b;
                    };

                    return bValue;
                },
                enumerable: false
            });
        };

        (function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                    || window[vendors[x]+'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
        }());
    },
    'popupOpen': function (content, style, beforeFunction, afterFunction, beforeClose, $subject) {
        $subject = $subject || $;
        content = content || '';
        style = style || 'fb-default-style';
        beforeFunction = beforeFunction || '';
        afterFunction = afterFunction || false;
        beforeClose = beforeClose || false;
        $subject.fancybox({
            content: content,
            wrapCSS: style,
            padding: 0,
            margin: 10,
            fitToView: false,
            openEffect: 'drop',
            closeEffect: 'drop',
            scrolling: 'auto',
            maxWidth: 1100,
            //maxWidth:'100%',
            maxHeight: 800,
            autoHeight: true,
            'beforeShow': function () {
                if (beforeFunction) {
                    beforeFunction();
                }
                hasPlaceholderSupport = function () {
                    var input = document.createElement('input');
                    return ('placeholder' in input);
                }
            },
            'afterShow': function () {
                $('.fancybox-wrap').addClass('fancybox-wrap-open');
                if (afterFunction) {
                    afterFunction();
                }
            },
            'beforeClose': function () {
                var $thisWrapper = $('.fancybox-wrap');
                if ($thisWrapper.hasClass('fancybox-wrap-close')) {
                    return true;
                } else {
                    if (beforeClose) {
                        beforeClose();
                    }
                    $thisWrapper.addClass('fancybox-wrap-close');
                    setTimeout(function () {
                        $.fancybox.close();
                    }, 300);
                    return false;
                }
            }
        });
    },
    'formValidation': function ($submitBtn, submitFunction) {
        submitFunction = submitFunction || false;
        $submitBtn = $submitBtn || $('.js-form-submit');
        var $submitForm = $submitBtn.closest('form');
        $submitForm.addClass('is-form-validation');
        var errorValidate = 'Поле обязательно для заполнения';
        var errorValidate2 = 'Поле заполнено не корректно';

        if(!device.mobile()){
            if(!device.tablet() || !device.android()){
                $submitForm.find('[data-mask="phone"]').mask("+7 (999) 999 99 99", {placeholder: "-"});
            }
            else{
                $submitForm.find('[data-mask="phone"]').attr('type','number');
            }
        }
        else{
            $submitForm.find('[data-mask="phone"]').attr('type','number');
        }

        $submitBtn.click(function (e) {
            e.preventDefault();

            var $this = $(this);
            var $thisForm = $this.closest('form');
            if ($this.hasClass('disabled')) {
                return false;
            }
            var $forms = $thisForm.find('[data-validate]');
            var result = formChecking($forms, true);
            if (result) {
                if (submitFunction) {
                    $this.addClass('disabled');
                    submitFunction($thisForm);
                }
                else{
                    setTimeout(function() {
                        $thisForm.submit();
                    }, 50);
                }
            }
            else {
                $forms.on('keyup keypress change', function () {
                    var $current = $(this);
                    setTimeout(function () {
                        formChecking($current, true);
                    }, 100);
                });
            }

            return false;
        });

        $(document).on('keydown', function (e) {
            return true;
            if (e.keyCode == 13) {
                var $thisFocus = $(document.activeElement);
                if ($thisFocus.is('textarea')) {
                    return true;
                }
                if ($thisFocus.closest('.form-select').length) {
                    return true;
                }
                if ($thisFocus.closest('.is-form-validation').length) {
                    $submitBtn.trigger('click');
                }
                return false;
            }
        });


        function formChecking($inp, onFocus) {

            onFocus = onFocus || false;
            var noError = true;

            $inp.each(function (ind, elm) {

                var $this = $(elm);
                var mask = $this.data('validate');
                var value = $this.val();
                var placeHolder = $this.attr('placeholder');

                if($this.is(':visible')){
                    if (mask == 'text') {
                        if ((value.length < 1) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'phone') {
                        if ((value.length < 1) || (value.indexOf('-') > -1)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'file') {
                        if (value.length < 2) {
                            noError = false;
                            $this.closest('.form-file').addClass('show-error');
                            if (onFocus) {
                                $this.focus();
                                onFocus = false;
                            }
                        } else {
                            $this.closest('.form-file').removeClass('show-error');
                        }
                    }

                    if (mask == 'textarea') {
                        if ((value.length < 3) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-textarea').addClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-textarea').removeClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'email') {
                        if(value != ''){
                            var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                            if (!regex.test(value) || (value == placeHolder)) {
                                noError = false;
                                $this.closest('.form-input').addClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideDown(200);
                                if (onFocus) {
                                    if(!device.ios()){
                                        $this.focus();
                                        onFocus = false;
                                    }
                                }
                            } else {
                                $this.closest('.form-input').removeClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideUp(200);
                            }
                        }
                    }

                    if(mask == "email-required"){
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'filter-email') {
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (regex.test(value) || (value == '')) {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        } else {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                    }

                    if (mask == 'select') {
                        if (!value) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-select').addClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-select').removeClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'checkbox') {
                        if (!$this.is(':checked')) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-checkbox').addClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        } else {
                            $this.closest('.form-checkbox').removeClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'captcha') {
                        var response = grecaptcha.getResponse();
                        if (response.length == 0){
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.addClass('show-error');
                        } else {
                            $this.removeClass('show-error');
                        }
                    }

                    if (mask == 'serial-number'){
                        var regex = /[0-9]{9,}/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'pass') {
                        if (value.length < 6) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'two-pass'){
                        var pass = $('.password').val();
                        if(value == '' || value != pass){
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                        else{
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'date'){
                        if (value.length < 1) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }
                }
            });

            setTimeout(function(){
                $.fancybox.update();
            },300);

            return noError;
        }
    },
    'sliderSwiper': function () {
        if($('.js-main-slider').length){

            var configMain = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    pagination: '.swiper-pagination',
                    onSlideChangeStart: function(){
                    }
                },
                $mainSlider = $('.js-main-slider');

            if($('.js-main-slider .swiper-slide').length > 1){
                var $mainSwiper = $mainSlider.swiper(configMain);
            }
        }

        if($('.js-about-slider').length){
            var slideActive = 0;
            var slidePosition = 0;
            var scroll = 0;

            var configAbout = {
                    slidesPerView: 4,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    simulateTouch: false,
                    onSlideNextStart: function(swiper){
                        var activeIndex = swiper.activeIndex;
                        if(activeIndex > slideActive){
                            $(swiper.slides[activeIndex]).find('.js-about-date').trigger('click')
                        }
                        slidePosition++;
                    },
                    onSlidePrevStart: function(swiper){
                        var activeIndex = swiper.activeIndex,
                            currentLimit = activeIndex + 3;

                        if(slideActive > currentLimit ){
                            $(swiper.slides[currentLimit]).find('.js-about-date').trigger('click')
                        }
                        slidePosition--;
                    },
                },
                $aboutSlider = $('.js-about-slider');

            var $aboutSwiper = $aboutSlider.swiper(configAbout);

            $('.js-about-date').on('click', function(){
                if(!$(this).hasClass('active')){
                    var $dates = $('.js-about-date'),
                        $this = $(this),
                        $slide = $this.closest('.swiper-slide');
                    var $content = $this.find('.content').clone();

                    $dates.removeClass('active');
                    $this.addClass('active');
                    slideActive = $slide.index();
                    $('.js-about-content .content').removeClass('active').addClass('deleted');
                    $content.appendTo('.js-about-content');
                    setTimeout(function(){
                        $('.js-about-content .deleted').remove();
                        $('.js-about-content .content').addClass('active');
                    },400);
                    //$this.find('.content').clone().appendTo('.js-about-content');
                }
            });
        }

        if($('.js-is-slider').length){
            var configIsSlider = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    autoHeight: true,
                    pagination: '.swiper-pagination',
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                },
                $isSlider = $('.js-is-slider');

            var $isSwiper = $isSlider.swiper(configIsSlider);
            
        }

    },
    'contacts': function() {
        if($('.page-contacts').length){

            var $points = $('.js-map-item.active'),
                lat = $points.attr('data-lat'),
                long = $points.attr('data-long'),
                myMap,
                myGeoObject;

            ymaps.ready(function() {
                initMap(lat, long);
            });
        }

        function initMap(lat, long){
            myMap = new ymaps.Map('map', {
                    center: [lat, long],
                    zoom: 14,
                    controls: ['zoomControl']
            });

            constructMapPoints(lat, long);
        };

        function constructMapPoints(lat, long){
            if (myGeoObject) {
                myMap.geoObjects.remove(myGeoObject);
                myGeoObject = null;
            }

            myMap.setCenter([lat, long], 14);

            myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [lat, long]
                },
            }, {
                iconImageHref: '/dist/img/pin.png',
                iconLayout:  'default#image',
                iconImageSize: [58, 66],
            });

            myMap.geoObjects.add(myGeoObject);
        };

        $('.js-map-item').on('click', function(e){
            e.preventDefault();

            var $this = $(this);

            if(!$this.hasClass('active')){
                $('.js-map-item').removeClass('active');
                $this.addClass('active');

                lat = $this.attr('data-lat');
                long = $this.attr('data-long');

                constructMapPoints(lat, long);
            }
        });

        $('.js-connect-btn').on('click', function(){
            var template = $('.js-connect-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){

                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);

                });
            });
        });

        $('.js-service-btn').on('click', function(){
            var template = $('.js-service-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        $('.js-training-btn').on('click', function(){
            var template = $('.js-training-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'alternativeAccordion': function() {
        $('.js-alternative-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.alternative-accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300,
                imageUrl = '',
                logoUrl = '';

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

                imageUrl = $this.attr('data-img');
                logoUrl = $this.attr('data-logo');

                $('.js-image-about').addClass('disabled');

                setTimeout(function(){
                    $('.js-border-img').attr('xlink:href', imageUrl);
                    $('.js-img-slide').attr('src', imageUrl);
                    $('.js-logo-slide').attr('src', logoUrl);

                    setTimeout(function(){
                        $('.js-image-about').removeClass('disabled');
                    },100);

                },300);
            }
        });
    },
    'tabs': function() {
        $('.js-tabs-controls').on('click', function(e){
            e.preventDefault();

            var $item = $(this).closest('.tabs-controls-item'),
                $parent = $(this).closest('.js-tabs-wrapper'),
                $contentItem = $parent.find('.js-tabs-item'),
                itemNumber = $item.index();

            $contentItem.eq(itemNumber)
                .add($item)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });

        if($(document).width() < 930){
            if(!$('.tabs-controls').hasClass('mobile')){
                $('.tabs-controls').each(function(ind,elem){
                    var $this = $(elem),
                        length = 0;

                    $this.css('width', '1000px');
                    $this.find('.tabs-controls-item').each(function(index,element){
                        console.log($(element).outerWidth(true));

                        length = length + $(element).outerWidth(true);
                        console.log(length);
                    });

                    $this.addClass('mobile');
                    $this.css('width', length+'px');
                });
            }
        }
    },
    'accordion': function() {
        $('.js-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300;

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

            } else {
                content.stop(true, true).slideUp(duration);
                item.removeClass('active');
            }

        });
    },
    'customSelect': function(){
        $('.js-select').selectric();
    },
    'elementForms': function(){
        $(document).on('change', 'input', function(){
            if($(this).val().length > 0){
                $(this).closest('.form-input').addClass('filled');
                $(this).closest('.delivery-wrapper').addClass('filled');
            }
            else{
                $(this).closest('.form-input').removeClass('filled');
                $(this).closest('.delivery-wrapper').removeClass('filled');
            }
        });

        $(document).on('change', 'textarea', function(){
            if($(this).val().length > 0){
                $(this).parents('.form-textarea').addClass('filled');
            }
            else{
                $(this).parents('.form-textarea').removeClass('filled');
            }
        });
    },
    'mobileMenu': function(){
        $('.js-m-btn-menu').on('click', function(){
            $(this).toggleClass('active');
            $('.mobile-menu-wrapper').toggleClass('active');
        });

        $('.js-mm-trigger').on('click', function(){
            var $triggerElement = $('.js-mm-trigger');
            var $this = $(this);
            var $parentTrigger = $this.parents('.mm-item');
            var $parentsTrigger = $('.js-mm-trigger').parents('.mm-item');
            var listItem = $parentTrigger.find('.mm-inner-item');
            var listsItem = $parentsTrigger.find('.mm-inner-item');
            var duration = 300;

            if($this.hasClass('active')){
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                listsItem.stop(true, true).slideUp(duration);
            }
            else{
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                $(this).addClass('active');
                $parentTrigger.addClass('active');
                listsItem.stop(true, true).slideUp(duration);
                listItem.stop(true, true).slideDown(duration);
            }

        });
    },
    'callMe': function(){
        $('.js-call-me').on('click', function(){
            var template = $('.js-call-me-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //$.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendMessage': function(){
        $('.js-send-message').on('click', function(){
            var template = $('.js-send-message-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //setTimeout(function(){
                    //    $.fancybox.update();
                    //}, 600);

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendEmail': function(){
        $('.js-form-email').on('click', function(){
            var $this = $(this),
                $formContainer = $this.closest('form');

            moduleApp.popupOpen($formContainer, false, function(){
                var value = $formContainer.serializeArray(),
                    urlAjax = $formContainer.attr('action');

                moduleApp.ajaxSendForm(value,urlAjax);
            });
        });
    },
    'ajaxSendForm': function(value,urlAjax){
        $.ajax({
            url: urlAjax,
            data: value,
            type: 'POST',
            dataType: 'json',
            success: function (result) {
                console.log(result);
                var resultMessage = '<div class="result-message"><div class="title">'+result.title+'</div><p>'+result.message+'</p></div>';
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            },
            error: function (error) {
                var resultMessage = '<div class="result-message"><h3>Ошибка</h3><p>'+error+'</p></div>';
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            }
        });
    },
    'parallaxTeam': function(){
        if($('.animate-layer-element').length){
            var scrolled = $(window).scrollTop();
            var countSrolled = (0-(scrolled*.15));
            $('.animate-layer-element img').each(function(ind,elem){
                //var transformZ = 'translateZ(0.000' + ind + 'px)';
                $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
            });

            $(window).bind('scroll',function(){
                moduleApp.parallaxScroll();
            });
        }
    },
    'parallaxScroll': function(){
        var scrolled = $(window).scrollTop();
        var countSrolled = (0-(scrolled*.15));
        $('.animate-layer-element img').each(function(ind,elem){
            $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
        });
    },
    'resizeWindow': function(){
        $(window).resize(function(){
            var widthWindow = $(document).width();

            if(widthWindow < 930){
                if(!$('.tabs-controls').hasClass('mobile')){
                    $('.tabs-controls').each(function(ind,elem){
                        var $this = $(elem);
                        var length = 0;
                        $this.find('.tabs-controls-item').each(function(index,element){
                            console.log($(element).outerWidth(true));
                            length = length + $(element).outerWidth(true);
                        });

                        $this.addClass('mobile');
                        $this.css('width', length+'px');
                    });
                }
            }
            if(widthWindow > 930){
                if(!$('.tabs-controls').hasClass('mobile')) {
                    $('.tabs-controls').each(function (ind, elem) {
                        var $this = $(elem);
                        $this.css('width', '100%');
                        $this.removeClass('mobile');
                    });
                }
            }

        });
    },
    'validSearch': function(){
        $('.js-search-btn').on('click', function(e){
            var $this = $(this);
            var $input = $this.siblings('.js-search-input');
            if($input.val().length < 3){
                e.preventDefault(e);
            }
        })
    },
    'brands': function(){
        $('.js-brand-item').on('click', function(){
            if($(this).hasClass('active')){
                $('.js-brand-item').removeClass('active');
            }
            else{
                $('.js-brand-item').removeClass('active');
                $(this).addClass('active');
            }
        });
    },
    'pageNews': function(){

        var stopFlagNews = true,
            filterValue = '',
            lang = $('.site_lang').html();

        if($('.ajax-wrapper-news').length > 0){

            if($('.actual-news-wrapper').length == 0){
                var $activeCard = $('.item-news-card.active'),
                    id = $activeCard.find('.js-name-news').attr('data-id'),
                    link = $activeCard.find('.js-name-news').attr('data-link'),
                    nextNews = $activeCard.find('.js-name-news').attr('data-next'),
                    prevNews = $activeCard.find('.js-name-news').attr('data-prev');

                sendNews(id,link,nextNews,prevNews,filterValue);
            }
        }

        $('.mCustomScrollbar-list-news').mCustomScrollbar({
            axis:"y",
            theme:"dark",
            callbacks:{
                onTotalScroll: function(){
                    if($('.js-info-news-list').length){

                        stopFlagNews = false;

                        var page = $('.js-info-news-list').attr('data-page');
                        filterValue = $('.js-search-news-text').val();

                        $('.js-info-news-list').remove();

                        $.ajax({
                            url: '/ajax/news-list.php',
                            data: {
                                PAGEN_1: page,
                                filter: filterValue
                            },
                            type: 'POST',
                            dataType: 'html',
                            success: function (result) {
                                addListNews(result);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }
                }
            }
        });

        $('.js-search-news-btn').on('click', function(e){
            e.preventDefault();

            //if($('.js-search-news-text').val().length > 3){
                var value = $('.js-search-news-text').val(),
                    lang = $('.site_lang').html();
                $.ajax({
                    url: '/ajax/filter-news.php',
                    data: {
                        value: value,
                        site_lang: lang
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function (result) {
                        console.log(result);
                        addNewNews(result.actualNews);
                        changeListNews(result.listNews);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            //}
        });

        $(document).on('click', '.js-name-news', function(e){
            e.preventDefault();

            $('.item-news-card').removeClass('active');
            $(this).closest('.item-news-card').addClass('active');
            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');

            filterValue = $('.js-search-news-text').val();

            sendNews(id, link, nextNews, prevNews, filterValue);
            
        });

        $(document).on('click', '.js-next-news', function(e){
            e.preventDefault();
            var $listNews = $('.list-news'),
                indexActiveElement = $listNews.find('.active').index() + 1;
            $listNews.find('.active').removeClass('active');
            $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');

            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');

            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);
        });

        $(document).on('click', '.js-prev-news', function(e){
            e.preventDefault();

            var $listNews = $('.list-news');
            var indexActiveElement = $listNews.find('.active').index() - 1;
            $listNews.find('.active').removeClass('active');
            $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');

            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');
            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);
        });

        function sendNews(id, link, nextNews, prevNews, filterValue){
            var lang = $('.site_lang').html();
            $.ajax({
                url: '/ajax/news.php',
                 data: {
                     id: id,
                     link: link,
                     nextNews: nextNews,
                     prevNews: prevNews,
                     filter: filterValue,
                     site_lang: lang,
                 },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    console.log(result._pageShare);
                    _pageShare = {
                        'link': result._pageShare.link,
                        'title': result._pageShare.title,
                        'description': result._pageShare.description,
                        'twitter_description': result._pageShare.twitter_description,
                        'image': result._pageShare.image
                    };
                    console.log(_pageShare);
                    addNewNews(result.actualNews, link);
                    changeMetaTeg();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        };

        function addNewNews(resultNews, linkNews){
            $('.actual-news-wrapper').addClass('deleted');
            $('.ajax-wrapper-news').append(resultNews);
            if(linkNews != ''){
                changeUrlNews(linkNews);
            }
            setTimeout(function() {
                $('.actual-news-wrapper.deleted').remove();
                $('.actual-news-wrapper.desabled').removeClass('desabled');
                updateScrollbar();
            }, 500);
        };

        function addListNews(newsListAdd){
            $('.list-news').append(newsListAdd);
            stopFlagNews = true;
        }

        function changeListNews(resultListNews){
            $('.list-news').addClass('deleted');
            $('.wrapper-list-news .mCSB_container').append(resultListNews);
            setTimeout(function() {
                $('.list-news.deleted').remove();
                $('.list-news.desabled').removeClass('desabled');
                updateScrollbar();
            }, 700);
        };

        function updateScrollbar(){
            setTimeout(function() {
                $('.mCustomScrollbar ').mCustomScrollbar("update");
                $('.mCustomScrollbar-list-news').mCustomScrollbar("update");
            }, 300);
        };

        function changeMetaTeg(){
            $('#og_url').attr('content', _pageShare.link);
            $('#og_title').attr('content', _pageShare.title);
            $('#og_description').attr('content', _pageShare.description);
            $('#og_image').attr('content', _pageShare.image);
        };

        function changeUrlNews(linkNews){
            if($('.page-news').length){
                history.pushState('', '', linkNews);
            }
        }
    },
    'startupMessage':function(){
        var title = '';
        if (appConfig.startupMessage.title && appConfig.startupMessage.message) {
            var template = '<div class="fb-popup-default">';
            template += '<div class="fbp-title">'+appConfig.startupMessage.title+'</div>';
            template += '<div class="fbp-message">'+appConfig.startupMessage.message+'</div>';
            template += '</div>';

            $.fancybox({
                content: template,
                wrapCSS: 'message-wrapper-popUp',
                padding: [0],
                fitToView: false,
                openEffect: 'elastic',
                closeEffect: 'elastic',
                maxWidth: 500,
                maxHeight: 380,
            });
        }
    },
    'subscribe': function(){
        moduleApp.formValidation($('.js-form-subscribe'), function(){

            var $this = $('.js-form-subscribe'),
                url = $this.closest('form').attr('action'),
                value = $this.closest('form').serializeArray();

            $this.closest('form').find('.delivery-input').val('');
            $('.filled').removeClass('filled');
            moduleApp.ajaxSendForm(value,url);
            $this.removeClass('disabled');
        });

        $('.delivery-input').focusout(function(){
            $(this).closest('.form-input').removeClass('show-error');
        });
    },
    'searchHeader': function(){
        $('.js-search-header-btn').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $thisForm = $this.closest('form');

            if(!$this.hasClass('active')){
                $this.addClass('active');
                $this.closest('form').addClass('activated');
            }
            else{

                if($thisForm.find('.search-header-text').val().length > 3){
                    $thisForm.submit();
                }
                else{
                    $thisForm.find('.search-header-text').val('');
                    $this.removeClass('active');
                    $this.closest('form').removeClass('activated');
                }
            }
        });
    },
    'socialShare': function(){
        $(document).on('click', '[data-service]', function(e){
            e.preventDefault();

            var $this = $(this),
                shareService = $this.attr('data-service'),
                windowLink = '';
            console.log(_pageShare);

            windowLink += 'http://share.yandex.ru/go.xml?service=' + shareService;
            windowLink += '&title=' + _pageShare.title;

            if (shareService=='twitter') {
                windowLink += ' ' + _pageShare.twitter_description;
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
            }
            else if (shareService=='livejournal'){
                windowLink = 'http://www.livejournal.com/update.bml?';
                windowLink += 'subject=' + _pageShare.title;
                windowLink += '&event=' + _pageShare.description + ' <a href="' + _pageShare.link + '">' + _pageShare.link + '</a>';
            } else {
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
                windowLink += '&description=' + _pageShare.description;
                windowLink += '&image=' + _pageShare.image;
            }

            window.open(windowLink,'','toolbar=0,status=0,width=625,height=435');

        });
    }
};

$(document).ready(function () {
    moduleApp.init();
});