<div class="footer-container">
    <footer class="site-footer">
        <div class="footer-wrapper">
            <div class="footer-main-control block-clear">
                <div class="footer-menu block-clear">
                    <div class="menu-row">
                        <a href="#" class="title-menu-item">О компании</a><br>
                        <a href="#" class="footer-menu-item">Команда</a><br>
                        <a href="#" class="footer-menu-item">Новости</a><br>
                        <a href="#" class="footer-menu-item">Бренды</a><br>
                    </div>
                    <div class="menu-row">
                        <a href="#" class="title-menu-item">Бизнес для бизнеса</a><br>
                        <a href="#" class="footer-menu-item">Рестораны</a><br>
                        <a href="#" class="footer-menu-item">Корпоративные клиенты</a><br>
                        <a href="#" class="footer-menu-item">Дистрибьютеры</a><br>
                        <a href="#" class="footer-menu-item">СТМ и опт</a><br>
                    </div>
                    <div class="menu-row">
                        <a href="#" class="title-menu-item">Сервисы</a><br>
                        <a href="#" class="footer-menu-item">Технический центр</a><br>
                        <a href="#" class="footer-menu-item">Торговый центр</a><br>
                    </div>
                </div>
                <div class="shop-element">
                    <a href="#" class="personal-area">Личный кабинет</a><br>
                    <a href="#" class="online-store">Онлайн-магазин</a><br>
                    <div class="credit-cards-list">
                        <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="9" viewBox="0 0 30 9"><use xlink:href="#visa-icon"/></svg></div>
                        <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="18" viewBox="0 0 30 18"><use xlink:href="#mc-icon"/></svg></div>
                        <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="18" viewBox="0 0 30 18"><use xlink:href="#maestro-icon"/></svg></div>
                        <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="9" viewBox="0 0 30 9"><use xlink:href="#paypal-icon"/></svg></div>
                    </div>
                </div>
                <div class="contacts-wrapper">
                    <div class="title">контакты</div><br>
                    <a href="tel:84997557767" class="number">+7 499 755-77-67</a><br>
                    <div class="call-me js-call-me">Перезвоните мне</div><br>
                    <div class="send-message js-send-message">Отправить сообщение</div>
                    <div class="social-list">
                        <a href="#" class="social-item">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                                <use xlink:href="#f-icon"/>
                            </svg>
                        </a>
                        <a href="#" class="social-item">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                                <use xlink:href="#vk-icon"/>
                            </svg>
                        </a>
                        <a href="#" class="social-item">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                                <use xlink:href="#i-icon"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>

            <div class="footer-lower block-clear">
                <form action="" class="delivery-footer">
                    <span>Подписаться на рассылку</span>
                    <div class="form-input">
                        <label class="delivery-wrapper" >
                            <input class="delivery-input" type="text" data-validate="email-required">
                            <span class="form-item-label">Введите вашу электронную почту</span>
                            <input class="delivery-btn js-form-subscribe" type="submit" value="Подписаться">
                        </label>
                        <div class="form-item-error">Ошибка в поле</div>
                    </div>
                </form>
                <div class="developers">
                    Разработка <a href="#">ITECH.group</a>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="mobile-footer-container">
    <div class="mm-list">
        <div class="mm-item">
            <div class="mm-trigger js-mm-trigger">О нас <div class="plus"></div></div>
            <div class="mm-inner-item">
                <a href="" class="mm-link">О компании</a>
                <a href="" class="mm-link">Команда</a>
                <a href="" class="mm-link">Новости</a>
                <a href="" class="mm-link">Бренды</a>
            </div>
        </div>
        <div class="mm-item">
            <a href="#" class="mm-trigger">Онлайн-магазин</a>
        </div>
        <div class="mm-item">
            <div class="mm-trigger js-mm-trigger">Бизнес для бизнеса <div class="plus"></div></div>
            <div class="mm-inner-item">
                <a href="" class="mm-link">О компании</a>
                <a href="" class="mm-link">Команда</a>
                <a href="" class="mm-link">Новости</a>
                <a href="" class="mm-link">Бренды</a>
            </div>
        </div>
        <div class="mm-item">
            <div class="mm-trigger js-mm-trigger">Сервисы <div class="plus"></div></div>
            <div class="mm-inner-item">
                <a href="" class="mm-link">О компании</a>
                <a href="" class="mm-link">Команда</a>
                <a href="" class="mm-link">Новости</a>
                <a href="" class="mm-link">Бренды</a>
            </div>
        </div>

        <div class="mm-item">
            <a href="#" class="mm-trigger">Контакты</a>
        </div>

    </div>

    <div class="m-contacts-wrapper">
        <a href="tel:84997557767" class="number">+7 499 755-77-67</a><br>
        <div class="call-me js-call-me">Перезвоните мне</div><br>
        <div class="send-message js-send-message">Отправить сообщение</div>
        <div class="social-list">
            <a href="#" class="social-item">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                    <use xlink:href="#f-icon"/>
                </svg>
            </a>
            <a href="#" class="social-item">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                    <use xlink:href="#vk-icon"/>
                </svg>
            </a>
            <a href="#" class="social-item">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 30 30">
                    <use xlink:href="#i-icon"/>
                </svg>
            </a>
        </div>

        <div class="delivery-container">
            <form action="" class="delivery-footer">
                <span>Подписаться на рассылку</span>
                <div class="form-input">
                    <label class="delivery-wrapper" >
                        <input class="delivery-input" type="text" data-validate="email-required">
                        <span class="form-item-label">Введите вашу электронную почту</span>
                    </label>
                    <div class="form-item-error">Ошибка в поле</div>
                </div>
                <input class="delivery-btn js-form-subscribe"  type="submit" value="Подписаться">
            </form>
        </div>

        <div class="credit-cards-list">
            <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="9" viewBox="0 0 30 9"><use xlink:href="#visa-icon"/></svg></div>
            <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="18" viewBox="0 0 30 18"><use xlink:href="#mc-icon"/></svg></div>
            <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="18" viewBox="0 0 30 18"><use xlink:href="#maestro-icon"/></svg></div>
            <div class="credit-card"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="9" viewBox="0 0 30 9"><use xlink:href="#paypal-icon"/></svg></div>
        </div>

        <div class="developers">
            Разработка <a href="#">ITECH.group</a>
        </div>
    </div>
</div>
</div>

<div class="form-container">

    <div class="js-call-me-form">
        <div class="form-wrapper-pop-up">
            <h3>Перезвоните мне</h3>
            <form action="/ajax/call-me.php">
                <div class="form-input">
                    <label>
                        <input type="text" data-validate="text">
                        <span class="form-item-label">Имя</span>
                    </label>
                    <div class="form-item-error">Ошибка в поле</div>
                </div>

                <input type="submit" class="default-btn green js-form-reviews" value="ПОДРОБНЕЕ">
            </form>
        </div>
    </div>

    <div class="js-send-message-form">
        <div class="form-wrapper-pop-up">
            <h3>Отправить сообщение</h3>
            <form action="">
                <div class="form-input">
                    <label>
                        <input type="text" data-validate="text">
                        <span class="form-item-label">Имя</span>
                    </label>
                    <div class="form-item-error">Ошибка в поле</div>
                </div>

                <div class="form-input">
                    <label>
                        <input type="text" data-validate="email-required">
                        <span class="form-item-label">E-mail</span>
                    </label>
                    <div class="form-item-error">Ошибка в поле</div>
                </div>

                <div class="form-select">
                    <select class="" name="" id="">
                        <option value="1">Test1</option>
                        <option value="2">Test2</option>
                        <option value="3">Test3</option>
                    </select>
                    <div class="form-item-error">Ошибка</div>
                </div>

                <div class="form-textarea">
                    <label>
                        <textarea name="" id="" data-validate="textarea"></textarea>
                        <span class="form-item-label">Ваше сообщение</span>
                    </label>
                    <div class="form-item-error">Ошибка</div>
                </div>

                <input type="submit" class="default-btn green js-form-reviews" value="ПОДРОБНЕЕ">
            </form>
        </div>
    </div>
</div>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/jquery-3.1.0.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/swiper3.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/jquery.fancybox.js"></script>
<!--<script src="/src/js/vendors/mCastomScrollbar.js"></script>-->
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/nanoScroller.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/jquery.selectric.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/jquery.maskedinput.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/mCastomScrollbar.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/fotorama.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/jquery.datetimepicker.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/vendors/inview.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/src/js/app.js"></script>
</body>
</html>