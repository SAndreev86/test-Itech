<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "ELEMENT_ID" => array(
        "PARENT" => "BASE",
        "NAME" => "ID",
        "TYPE" => "STRING",
        "DEFAULT" => '={$_REQUEST["ELEMENT_ID"]}',
    ),

);
?>
