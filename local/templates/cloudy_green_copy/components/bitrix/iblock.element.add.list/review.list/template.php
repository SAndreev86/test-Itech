<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>



<div class="site-inner">
    <h3>Отзывы клиентов</h3>
    <? if (count($arResult["PROPERTIES"]) > 0): ?>
        <? foreach ($arResult["PROPERTIES"] as $property): ?>
            <h4><?= $property["~PROPERTY_NAME_VALUE"] ?></h4>
            <p><?= $property["~PROPERTY_TEXT_REVIEW_VALUE"] ?></p>
        <? endforeach ?>
    <? endif ?>
</div>


