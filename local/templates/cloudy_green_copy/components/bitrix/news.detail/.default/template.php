<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>



<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])): ?>

    <div class="image-left-wrapper block-clear">
        <div class="image">
            <? if (!empty($arResult["LAST_PAGE"])): ?>

            <? endif ?>

            <img src="<?=
            CFile::ResizeImageGet(
                $arResult["DETAIL_PICTURE"],
                array("width" => 200, "height" => 200)
            )["src"] ?>" alt="">
            <div class="title"><?= $arResult["NAME"] ?></div>
        </div>
        <p><?= $arResult["DETAIL_TEXT"] ?></p>
        <div class="site-inner">

        </div>
        <div class="container-table">
            <table>
                <tr>
                    <th>Автор книги</th>
                    <th>Год издания</th>
                    <th>Жанр</th>
                    <th>Стоимость книги</th>
                </tr>

                <tr>

                    <td><?= $arResult["PROPERTIES"]["AUTHOR"]["~VALUE"] ?></td>
                    <td><?= $arResult["PROPERTIES"]["YEAR"]["~VALUE"] ?> г.</td>
                    <td><?= $arResult["PROPERTIES"]["GENRE"]["~VALUE"] ?></td>
                    <td><?= $arResult["PROPERTIES"]["PRICE"]["~VALUE"] ?> руб.</td>
                </tr>


            </table>

        </div>




    </div>
<? endif ?>

