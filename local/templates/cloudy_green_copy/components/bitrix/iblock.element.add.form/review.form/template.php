<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult["ERRORS"])):?>
    <? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;?>

<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
    <div class="site-inner">
        <h3>Оставьте свой отзыв</h3>


        <?= bitrix_sessid_post() ?>
        <? if ($arParams["MAX_FILE_SIZE"] > 0): ?><input type="hidden" name="MAX_FILE_SIZE"
                                                         value="<?= $arParams["MAX_FILE_SIZE"] ?>" /><? endif ?>

        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>

            <? foreach ($arResult["PROPERTY_LIST"] as $index => $propertyID): ?>

                <?
                if (intval($propertyID) > 0) {
                    if (
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                        &&
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                    )
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                    elseif (
                        (
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                            ||
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                        )
                        &&
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                    )
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                    $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                    $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                } else {
                    $inputNum = 1;
                }

                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                    $INPUT_TYPE = "USER_TYPE";
                else
                    $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                switch ($INPUT_TYPE):


                    case "S":
                    case "N":
                        for ($i = 0; $i < $inputNum; $i++) {
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                            } elseif ($i == 0) {
                                $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                            } else {
                                $value = "";
                            }
                            ?>


                            <? if ($propertyID == "NAME"): ?>

                                <input type="hidden" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25"
                                       value="Отзыв"/>

                            <? endif ?>

                            <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "ID"): ?>

                                <input type="hidden" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25"
                                       value="<?= $_REQUEST["ELEMENT_ID"] ?>"/>

                            <? endif ?>


                            <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "TEXT_REVIEW"): ?>


                                <div class="form-textarea">
                                    <label>
                                                <textarea name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" id=""
                                                          data-validate="textarea"></textarea>
                                        <span class="form-item-label"><?=GetMessage("YOUR_MESSAGE")?></span>
                                    </label>
                                </div>

                            <? endif ?>


                            <? if ($propertyID != "NAME" && $arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] != "ID" && $arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] != "TEXT_REVIEW"): ?>

                                <div class="form-input">
                                    <label>
                                        <input type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                               data-validate="text">
                                        <span class="form-item-label"><?=GetMessage("YOUR_NAME")?></span>
                                    </label>
                                </div>

                            <? endif ?>


                            <?

                        }
                        break;


                endswitch; ?>
            <? endforeach; ?>
            <? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
                <?= GetMessage("IBLOCK_FORM_CAPTCHA_TITLE") ?><br/>

                <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                     height="40" alt="CAPTCHA"/><br/>

                <input type="text" name="captcha_word" maxlength="50" value=""><br/>

            <? endif ?>
        <? endif ?>

        <input type="submit" class="default-btn green" name="iblock_submit"
               value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
        <? if (strlen($arParams["LIST_URL"]) > 0): ?>
            <input type="submit" name="iblock_apply" value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
            <input
                    type="button"
                    name="iblock_cancel"
                    value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                    onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';"
            >
        <? endif ?>
    </div>
</form>