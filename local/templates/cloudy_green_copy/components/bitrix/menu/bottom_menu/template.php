<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<? if (!empty($arResult)): ?>

    <div class="main-menu">


        <?
        foreach ($arResult as $arItem):?>
            <div class="main-menu-item">
                <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            </div>

        <? endforeach ?>
    </div>

<? endif ?>

