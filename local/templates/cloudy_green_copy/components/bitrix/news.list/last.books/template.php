<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<!--  table  -->

<? if (count($arResult["ITEMS"]) != 0) : ?>
    <h3>Ранее вы смотрели...</h3>

    <tr>
        <td>
            <h4><?= $arResult["ITEMS"]["NAME"] ?></h4>
            <a href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"] ?>">
                <div class="image">
                    <img src="<?= CFile::ResizeImageGet(
                        $arResult["ITEMS"]["PREVIEW_PICTURE"],
                        array("width" => 200, "height" => 200)
                    )["src"] ?>" alt=""/>
                </div>
                <div class="default-btn green"><?= GetMessage("DETAILS") ?></div>
            </a>

        </td>
    </tr>


<? else: ?>

    <h1><?= GetMessage("ERROR") ?></h1>

<? endif ?>


<!--  end  -->