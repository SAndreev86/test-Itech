<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<!--  table  -->

<? if (count($arResult["ITEMS"]) != 0) : ?>

    <h3>Книги этого автора</h3>

    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <tr>
            <td>
                <h4><?= $arItem["NAME"] ?></h4>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                    <div class="image">
                        <img src="<?= CFile::ResizeImageGet(
                            $arItem["PREVIEW_PICTURE"],
                            array("width" => 200, "height" => 200)
                        )["src"] ?>" alt=""/>
                    </div>
                    <div class="default-btn green"><?=GetMessage("DETAILS")?></div>
                </a>

            </td>
        </tr>

    <? endforeach; ?>


<? else: ?>

    <h1><?=GetMessage("ERROR")?></h1>

<? endif ?>


<!--  end  -->