<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?


$db_props = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $_REQUEST["ELEMENT_ID"], array("sort" => "asc"), Array("CODE" => "AUTHOR"));
if ($ar_props = $db_props->Fetch()) {
    foreach ($arResult["ITEMS"] as $index => $item) {


        if ($item["PROPERTIES"]["AUTHOR"]["~VALUE"] != $ar_props["VALUE"]) {
            unset($arResult["ITEMS"][$index]);
        }

    }
}


?>