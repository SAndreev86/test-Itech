<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
        </div>
    <script src="/src/js/vendors/jquery-3.1.0.min.js"></script>
    <script src="/src/js/vendors/swiper3.min.js"></script>
    <script src="/src/js/vendors/jquery.fancybox.js"></script>
    <!--<script src="/src/js/vendors/mCastomScrollbar.js"></script>-->
    <script src="/src/js/vendors/nanoScroller.js"></script>
    <script src="/src/js/vendors/jquery.selectric.js"></script>
    <script src="/src/js/vendors/easeljs-0.8.0.min.js"></script>
    <script src="/src/js/vendors/vivus.js"></script>
    <script src="/src/js/app.js"></script>
</body>
</html>