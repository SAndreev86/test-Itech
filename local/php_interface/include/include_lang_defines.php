<?

global $curpage;
global $site_lang;
$curpage = $APPLICATION->GetCurPage();
$site_lang = "ru";
if(strripos($curpage, '/en/') !== false){
    $site_lang = 'en';
}
$file_define_lang = "define_".$site_lang.".php";
require($file_define_lang);