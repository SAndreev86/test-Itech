<?php
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error()
{
    if (defined('ERROR_404') && ERROR_404=='Y' && !defined('ADMIN_SECTION'))
    {
        GLOBAL $APPLICATION;
        $APPLICATION->RestartBuffer();
        $page = "404";

        $curpage = $APPLICATION->GetCurPage();
        $page404 = "/404.php";
        if(strripos($curpage, '/en/') !== false){
            $page404 = '/en/404.php';
        }

        require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header_404.php");
        require $_SERVER['DOCUMENT_ROOT'].$page404;
        require($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer_404.php");
    }
}