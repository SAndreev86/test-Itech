<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>


<div class="wrapper-page">
    <div class="bg-image"></div>
    <div class="content-wrapper">
        <div class="logo">
            <svg width="199px" height="35px" viewBox="0 0 199 35" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs>
                    <polygon id="path-1" points="78.7718355 29.5263184 0 29.5263184 0 14.8539691 0 0.18161975 78.7718355 0.18161975"></polygon>
                    <polygon id="path-3" points="117.206508 34.7699333 117.206508 0.453811029 58.768843 0.453811029 0.331177733 0.453811029 0.331177733 34.7699333"></polygon>
                </defs>
                <g id="main_page" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="main_page_v3.3_v2-copy" transform="translate(-860.000000, -56.000000)">
                        <g id="header">
                            <g id="malltech_violet" transform="translate(860.000000, 56.000000)">
                                <g id="Group-5" transform="translate(0.000000, 5.243615)">
                                    <mask id="mask-2" fill="white">
                                        <use xlink:href="#path-1"></use>
                                    </mask>
                                    <g id="Clip-2"></g>
                                    <polyline id="Fill-1" fill="#FFFFFF" mask="url(#mask-2)" points="4.10052201 0.18161975 13.8187117 25.934919 23.9070411 0.18161975 28.0075631 0.18161975 28.0075631 29.55492 25.2189231 29.55492 25.2189231 4.17201064 25.1371977 4.17201064 15.1310688 29.55492 12.5068297 29.55492 2.87036541 4.17201064 2.78864005 4.17201064 2.78864005 29.55492 0 29.55492 0 0.18161975 4.10052201 0.18161975"></polyline>
                                    <path d="M49.4124781,18.0361283 L43.7529975,2.97884992 L37.8892035,18.0361283 L49.4124781,18.0361283 L49.4124781,18.0361283 Z M45.4345441,0.18161975 L56.9164809,29.55492 L53.9225771,29.55492 L50.3551705,20.421973 L37.0282364,20.421973 L33.5016924,29.55492 L30.5496017,29.55492 L42.3180523,0.18161975 L45.4345441,0.18161975 L45.4345441,0.18161975 Z" id="Fill-3" fill="#FFFFFF" mask="url(#mask-2)"></path>
                                    <polyline id="Fill-4" fill="#FFFFFF" mask="url(#mask-2)" points="62.3692723 0.18161975 62.3692723 27.1690752 78.7718355 27.1690752 78.7718355 29.55492 59.5811074 29.55492 59.5811074 0.18161975 62.3692723 0.18161975"></polyline>
                                </g>
                                <polyline id="Fill-6" fill="#FFFFFF" points="143.000835 0.453811029 143.000835 2.84013247 125.573379 2.84013247 125.573379 13.4541623 141.893741 13.4541623 141.893741 15.840007 125.573379 15.840007 125.573379 27.4412665 143.205623 27.4412665 143.205623 29.8275879 122.784739 29.8275879 122.784739 0.453811029 143.000835 0.453811029"></polyline>
                                <path d="M167.296071,6.93348889 C166.735397,6.04255106 166.052135,5.2950976 165.24581,4.6916052 C164.43901,4.08858949 163.550484,3.62905816 162.580233,3.31348788 C161.609982,2.9983943 160.591266,2.84060916 159.525511,2.84060916 C157.584059,2.84060916 155.916766,3.21052235 154.522684,3.95082543 C153.128126,4.6916052 151.986822,5.67263786 151.098772,6.89249335 C150.210246,8.11330222 149.554068,9.49141954 149.130236,11.027322 C148.70593,12.5632244 148.494964,14.1401224 148.494964,15.758016 C148.494964,17.3492147 148.70593,18.919439 149.130236,20.4686888 C149.554068,22.0184154 150.210246,23.4036831 151.098772,24.6235386 C151.986822,25.8443474 153.128126,26.8249034 154.522684,27.5652065 C155.916766,28.3055096 157.584059,28.6758994 159.525511,28.6758994 C160.89251,28.6758994 162.122191,28.4289729 163.216456,27.9351197 C164.30882,27.4417432 165.251987,26.7700838 166.045483,25.9196648 C166.837554,25.0697225 167.480429,24.0753425 167.973156,22.9370014 C168.464934,21.7991369 168.778531,20.5716544 168.915849,19.2550304 L171.704489,19.2550304 C171.513004,21.0650309 171.089173,22.6972251 170.432994,24.1501832 C169.776816,25.6045712 168.929153,26.8387275 167.890956,27.8531286 C166.851809,28.8680064 165.628304,29.6497817 164.220917,30.1979778 C162.812581,30.7461739 161.247445,31.0207487 159.525511,31.0207487 C157.228648,31.0207487 155.212598,30.6022129 153.47736,29.7660946 C151.740696,28.9299764 150.299099,27.8121331 149.151143,26.413518 C148.002711,25.0144262 147.141744,23.3965327 146.567766,21.558884 C145.993788,19.7217121 145.706799,17.7882483 145.706799,15.758016 C145.706799,13.728737 145.993788,11.7952732 146.567766,9.95762457 C147.141744,8.11997591 148.002711,6.4954087 149.151143,5.08201616 C150.299099,3.67005369 151.740696,2.54601335 153.47736,1.70894174 C155.212598,0.872346831 157.228648,0.453811029 159.525511,0.453811029 C160.919593,0.453811029 162.27899,0.659742084 163.605602,1.07160419 C164.931738,1.48251292 166.133386,2.0931557 167.213871,2.90210246 C168.293881,3.71104922 169.195711,4.70542927 169.92031,5.88476592 C170.644435,7.06410256 171.102477,8.42172211 171.293961,9.95762457 L168.505321,9.95762457 C168.260145,8.83310753 167.85627,7.8253801 167.296071,6.93348889" id="Fill-7" fill="#FFFFFF"></path>
                                <g id="Group-11" transform="translate(81.725352, 0.000000)">
                                    <mask id="mask-4" fill="white">
                                        <use xlink:href="#path-3"></use>
                                    </mask>
                                    <g id="Clip-9"></g>
                                    <polyline id="Fill-8" fill="#FFFFFF" mask="url(#mask-4)" points="97.0317499 0.453811029 97.0317499 13.2072357 114.417868 13.2072357 114.417868 0.453811029 117.206508 0.453811029 117.206508 29.8275879 114.417868 29.8275879 114.417868 15.5935571 97.0317499 15.5935571 97.0317499 29.8275879 94.2431098 29.8275879 94.2431098 0.453811029 97.0317499 0.453811029"></polyline>
                                    <polyline id="Fill-10" fill="#974F8F" mask="url(#mask-4)" points="3.11981779 5.42523458 3.11981779 32.41269 19.522381 32.41269 19.522381 34.7985348 0.331177733 34.7985348 0.331177733 5.42523458 3.11981779 5.42523458"></polyline>
                                </g>
                                <polyline id="Fill-12" fill="#974F8F" points="108.491355 0.453811029 95.529809 0.453811029 95.529809 2.87731447 105.699864 2.87731447 105.699864 29.7761052 108.488504 29.7761052 108.491355 0.453811029"></polyline>
                                <polygon id="Fill-13" fill="#FFFFFF" points="108.491355 2.87159416 118.698471 2.87159416 118.698471 0.453811029 108.491355 0.453811029"></polygon>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
        <h1>ОШИБКА 404</h1>
        <div class="title-404">Запрошенная страница не найдена. <br> Попробуйте перейти на <a href="#">главную страницу</a> <br> или воспользуйтесь <a href="#">картой сайта.</a></div>
    </div>

    <div class="mask-image">
        <svg class="js-border-mask-2" width="615" height="815" viewBox="0 0 615 815" xmlns="http://www.w3.org/2000/svg">

            <symbol id="image">
                <image class="js-border-img" xlink:href="<?=SITE_TEMPLATE_PATH?>/dist/img/404_bg.jpg" preserveAspectRatio="xMinYMin slice" x="-107%" y="-11%" width="1920px" height="942px" style="/*transform: translate(-600px);*/"/>
            </symbol>

            <symbol id="s-mask">
                <g stroke="gray" fill="white" stroke-width="0">
                    <rect x="550" y="0" width="65" height="750"/>
                    <rect x="315" y="0" height="65" width="300"/>
                </g>
                <g stroke="gray"  fill="white" stroke-width="0" >
                    <rect x="0" y="750" height="65" width="430"/>
                    <rect x="0" y="50" width="65" height="750" />
                </g>
            </symbol>

            <defs>
                <clipPath id="new-mask" clipPathUnits="userSpaceOnUse">
                    <!--                        <g stroke="gray" fill="white" stroke-width="0">-->
                    <rect x="550" y="0" width="65" height="750"/>
                    <rect x="315" y="0" height="65" width="300"/>
                    <!--                        </g>-->
                    <!--                        <g stroke="gray"  fill="white" stroke-width="0" >-->
                    <rect x="0" y="750" height="65" width="430"/>
                    <rect x="0" y="50" width="65" height="750" />
                    <!--                        </g>-->
                </clipPath>
            </defs>

            <mask id="mask">
                <use xlink:href="#s-mask" />
            </mask>

            <g mask="url(#mask)">
                <use xlink:href="#image"  ></use>
            </g>

            <!--                <foreignObject mask="url(#mask)" x="0" y="0" width="100%" height="100%">-->
            <!--                    <div style="background: url('/dist/img/404_bg.jpg') center center no-repeat; background-size: cover;width: 100vw;height: 100vh;"></div>-->
            <!--                </foreignObject>-->
        </svg>

        <!--            <svg class="js-border-mask-2" preserveAspectRatio="xMidYMax slice" width="615px" height="815px" viewBox="0 0 615 815">-->
        <!--                <foreignObject clip-path="url(#new-mask)" x="-80%" y="0" width="100vw" height="100vh">-->
        <!--                    <div style="background: url('/dist/img/404_bg.jpg') center center no-repeat; background-size: cover;width: 100vw;height: 100vh;"></div>-->
        <!--                </foreignObject>-->
        <!--            </svg>-->
    </div>
</div>


