<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Главная");
?>


    <div class="container-table">
        <table>
            <tr>
                <th><h3>Название книги</h3></th>
                <th><h3>Автор книги</h3></th>
                <th><h3>Год издания</h3></th>
                <th><h3>Жанр книги</h3></th>
                <th><h3>Стоимость книги</h3></th>
            </tr>
            <? $APPLICATION->IncludeComponent(
                "books.filter",
                ".default",
                Array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => IBLOCK_ALL_BOOKS,
                    "FILTER_NAME" => "arrFilter",
                    "PROPERTY_CODE" => [ "NAME", "AUTHOR", "YEAR", "GENRE", "AFTER_PRICE_BOOK", "BEFORE_PRICE_BOOK" ],

                ),
                false
            ); ?>
            <? $APPLICATION->IncludeComponent("bitrix:news.list", "books", Array(

                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => IBLOCK_ALL_BOOKS,
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => "",
                    "PROPERTY_CODE" => Array("DESCRIPTION"),
                )
            ); ?>

        </table>

    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>