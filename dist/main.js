/*!
 * fancyBox - jQuery Plugin
 * version: 3.0.0 Beta 1 (Tue, 29 Jan 2013)
 * @requires jQuery v1.7 or later
 *
 * Examples at http://fancyapps.com/fancybox/
 * License: www.fancyapps.com/fancybox/#license
 *
 * Copyright 2013 Janis Skarnelis - janis@fancyapps.com
 *
 */

(function (window, document, $, undefined) {
	"use strict";

	var W  = $(window),
		D  = $(document),
		H  = $('html');

	var F = $.fancybox = function () {
		F.open.apply( this, arguments );
	};

	var isTouch = F.isTouch = (document.createTouch !== undefined || window.ontouchstart !== undefined);

	var isQuery = function(object) {
		return object && object.hasOwnProperty && object instanceof $;
	};

	var isString = function(str) {
		return str && $.type(str) === "string";
	};

	var isPercentage = function(str) {
		return isString(str) && str.indexOf('%') > 0;
	};

	var getScalar = function(orig, dim) {
		var value = parseFloat(orig, 10) || 0;

		if (dim && isPercentage(orig)) {
			value = F.getViewport()[ dim ] / 100 * value;
		}

		return Math.ceil(value);
	};

	var getValue = function(value, dim) {
		return getScalar(value, dim) + 'px';
	};

	var getTime = Date.now || function() {
		return +new Date;
	};

	var removeWrap = function(what) {
		var el = isString(what) ? $(what) : what;

		if (el && el.length) {
			el.removeClass('fancybox-wrap').stop(true).trigger('onReset').hide().unbind();

			try {
				el.find('iframe').unbind().attr('src', isTouch ? '' : '//about:blank');

				// Give the document in the iframe to get a chance to unload properly before remove
				setTimeout(function () {
					el.empty().remove();

					// Remove the lock if there are no elements
					if (F.lock && !F.coming && !F.current) {
						var scrollV, scrollH;

						$('.fancybox-margin').removeClass('fancybox-margin');

						scrollV = W.scrollTop();
						scrollH = W.scrollLeft();

						H.removeClass('fancybox-lock');

						F.lock.remove();

						F.lock = null;

						W.scrollTop( scrollV ).scrollLeft( scrollH );
					}
				}, 150);

			} catch(e) {}
		}
	};

	$.extend(F, {
		// The current version of fancyBox
		version: '3.0.0',

		defaults: {
			theme     : 'default',          // 'default', dark', 'light'
			padding   : 15,					// space inside box, around content
			margin    : [30, 55, 30, 55],	// space between viewport and the box
			loop      : true,               // Continuous gallery item loop

			arrows    : true,
			closeBtn  : true,
			expander  : !isTouch,

			caption : {
				type     : 'outside'	// 'float', 'inside', 'outside' or 'over',
			},

			overlay : {
				closeClick : true,      // if true, fancyBox will be closed when user clicks on the overlay
				speedIn    : 0,         // duration of fadeIn animation
				speedOut   : 250,       // duration of fadeOut animation
				showEarly  : true,      // indicates if should be opened immediately or wait until the content is ready
				css        : {}			// custom CSS properties
			},

			helpers : {},				// list of enabled helpers

			// Dimensions
			width       : 800,
			height      : 450,
			minWidth    : 100,
			minHeight   : 100,
			maxWidth    : 99999,
			maxHeight   : 99999,
			aspectRatio : false,
			fitToView   : true,

			autoHeight  : true,
			autoWidth   : true,
			autoResize  : true,

			// Location
			autoCenter  : !isTouch,
			topRatio    : 0.5,
			leftRatio   : 0.5,

			// Opening animation
			openEffect  : 'elastic',		// 'elastic', 'fade', 'drop' or 'none'
			openSpeed   : 350,
			openEasing  : 'easeOutQuad',

			// Closing animation
			closeEffect : 'elastic',		// 'elastic', 'fade', 'drop' or 'none'
			closeSpeed  : 350,
			closeEasing : 'easeOutQuad',

			// Animation for next gallery item
			nextEffect : 'elastic',		// 'elastic', 'fade', 'drop' or 'none'
			nextSpeed  : 350,
			nextEasing : 'easeOutQuad',

			// Animation for previous gallery item
			prevEffect : 'elastic',		// 'elastic', 'fade', 'drop' or 'none'
			prevSpeed  : 350,
			prevEasing : 'easeOutQuad',

			// Slideshow
			autoPlay   : false,
			playSpeed  : 3000,

			/*
				Advanced
			*/

			// Callbacks
			onCancel     : $.noop, // If canceling
			beforeLoad   : $.noop, // Before loading
			afterLoad    : $.noop, // After loading
			beforeShow   : $.noop, // Before changing in current item
			afterShow    : $.noop, // After opening
			beforeClose  : $.noop, // Before closing
			afterClose   : $.noop,  // After closing

			// Properties specific to content type
			ajax  : {
				dataType : 'html',
				headers  : { 'X-fancyBox': true }
			},

			iframe : {
				scrolling : 'auto',
				preload   : true
			},

			swf : {
				wmode             : 'transparent',
				allowfullscreen   : 'true',
				allowscriptaccess : 'always'
			},

			// Default keyboard
			keys  : {
				next : {
					13 : 'left', // enter
					34 : 'up',   // page down
					39 : 'left', // right arrow
					40 : 'up'    // down arrow
				},
				prev : {
					8  : 'right',  // backspace
					33 : 'down',   // page up
					37 : 'right',  // left arrow
					38 : 'down'    // up arrow
				},
				close  : [27], // escape key
				play   : [32], // space - start/stop slideshow
				toggle : [70]  // letter "f" - toggle fullscreen
			},

			// Default direction
			direction : {
				next : 'left',
				prev : 'right'
			},

			// HTML templates
			tpl: {
				wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-inner"></div></div>',
				iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true"></iframe>',
				error    : '<p class="fancybox-error">{{ERROR}}</p>',
				closeBtn : '<a title="{{CLOSE}}" class="fancybox-close" href="javascript:;"></a>',
				next     : '<a title="{{NEXT}}" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev     : '<a title="{{PREV}}" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			},

			// Localization
			locale  : 'en',
			locales : {
				'en' : {
					CLOSE      : 'Close',
					NEXT       : 'Next',
					PREV       : 'Previous',
					ERROR      : 'The requested content cannot be loaded. <br/> Please try again later.',
					EXPAND     : 'Display actual size',
					SHRINK     : 'Fit to the viewport',
					PLAY_START : 'Start slideshow',
					PLAY_STOP  : 'Pause slideshow'
				},
				'de' : {
					CLOSE      : 'Schliessen',
					NEXT       : 'Vorwärts',
					PREV       : 'Zurück',
					ERROR      : 'Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.',
					EXPAND     : '',
					SHRINK     : '',
					PLAY_START : '',
					PLAY_STOP  : ''
				}
			},

			// Override some properties
			index     : 0,
			content   : null,
			href      : null,

			// Various
			wrapCSS       : '',         // CSS class name for the box
			modal         : false,
			locked        : true,
			preload       : 3,			// Number of gallery images to preload
			mouseWheel    : true,		// Enable or disable mousewheel support
			scrolling     : 'auto',     // 'yes', 'no', any valid value for CSS "overflow" property
			scrollOutside : true		// If trye, fancyBox will try to set scrollbars outside the content
		},

		// Current state
		current  : null,
		coming   : null,
		group    : [],
		index    : 0,
		isActive : false,	// Is activated
		isOpen   : false,	// Is currently open
		isOpened : false,	// Have been fully opened at least once
		isMaximized : false,

		player : {
			timer    : null,
			isActive : false
		},

		// Loaders
		ajaxLoad   : null,
		imgPreload : null,

		// Object containing all helpers
		helpers    : {},

		// Open fancyBox
		open: function( items, options ) {
			if (!items) {
				return;
			}

			// Close if already active
			if (false === F.close(true)) {
				return;
			}

			if (!$.isPlainObject( options )) {
				options = {};
			}

			F.opts = $.extend(true, {}, F.defaults, options);

			F.populate( items );

			if (F.group.length) {
				F._start( F.opts.index );
			}
		},

		// Add new items to the group
		populate : function( items ) {
			var group = [];

			if ( !$.isArray( items )) {
				items = [ items ];
			}

			// Build group array, each item is object containing element
			// and most important attributes - href, title and type
			$.each(items, function(i, element) {
				var defaults = $.extend(true, {}, F.opts),
					item,
					obj,
					type,
					margin,
					padding;

				if ($.isPlainObject(element)) {
					item = element;

				} else if (isString(element)) {
					item = { href : element };

				} else if (isQuery(element) || $.type(element) === 'object' && element.nodeType) {
					obj  = $(element);
					item = $(obj).get(0);

					if (!item.href) {
						item = { href : element };
					}

					item = $.extend({
						href    : obj.data('fancybox-href')  || obj.attr('href')  || item.href,
						title   : obj.data('fancybox-title') || obj.attr('title') || item.title,
						type    : obj.data('fancybox-type'),
						element : obj
					}, obj.data('fancybox-options') );

				} else {
					return;
				}

				// If the type has not specified, then try to guess
				if (!item.type && (item.content || item.href)) {
					item.type = item.content ? "html" : F.guessType( obj, item.href );
				}

				// Adjust some defaults depending on content type
				type = item.type || F.opts.type;

				if (type === 'image' || type === 'swf') {
					defaults.autoWidth = defaults.autoHeight = false;
					defaults.scrolling = 'visible';
				}

				if (type === 'image') {
					defaults.aspectRatio = true;
				}

				if (type === 'iframe') {
					defaults.autoWidth = false;
					defaults.scrolling = isTouch ? 'scroll' : 'visible';
				}

				if (items.length < 2) {
					defaults.margin = 30;
				}

				item = $.extend(true, {}, defaults, item);

				// Recheck some parameters
				margin  = item.margin;
				padding = item.padding;

				// Convert margin and padding properties to array - top, right, bottom, left
				if ($.type(margin) === 'number') {
					item.margin = [margin, margin, margin, margin];
				}

				if ($.type(padding) === 'number') {
					item.padding = [padding, padding, padding, padding];
				}

				// 'modal' propery is just a shortcut
				if (item.modal) {
					$.extend(true, item, {
						closeBtn   : false,
						closeClick : false,
						nextClick  : false,
						arrows     : false,
						mouseWheel : false,
						keys       : null,
						overlay : {
							closeClick : false
						}
					});
				}

				if (item.autoSize !== undefined) {
					item.autoWidth = item.autoHeight = !!item.autoSize;
				}

				if (item.width === 'auto') {
					item.autoWidth = true;
				}

				if (item.height === 'auto') {
					item.autoHeight = true;
				}

				group.push( item );
			});

			F.group = F.group.concat( group );
		},

		// Cancel image loading and abort ajax request
		cancel: function () {
			var coming = F.coming;

			if (!coming || false === F.trigger('onCancel')) {
				return;
			}

			F.hideLoading();

			if (F.ajaxLoad) {
				F.ajaxLoad.abort();
			}

			if (F.imgPreload) {
				F.imgPreload.onload = F.imgPreload.onerror = null;
			}

			if (coming.wrap) {
				removeWrap( coming.wrap );
			}

			F.ajaxLoad = F.imgPreload = F.coming = null;

			// If the first item has been canceled, then clear everything
			if (!F.current) {
				F._afterZoomOut( coming );
			}
		},

		// Start closing or remove immediately if is opening/closing
		close: function (e) {
			if (e && $.type(e) === 'object') {
				e.preventDefault();
			}

			F.cancel();

			// Do not close if:
			//   - the script has not been activated
			//   - cancel event has triggered opening a new item
			//   - "beforeClose" trigger has returned false
			if (!F.isActive || F.coming || false === F.trigger('beforeClose')) {
				return;
			}

			F.unbind();

			F.isClosing = true;

			if (F.lock) {
				F.lock.css('overflow', 'hidden');
			}

			if (!F.isOpen || e === true) {
				F._afterZoomOut();

			} else {
				F.isOpen = F.isOpened = false;

				F.transitions.close();
			}
		},

		prev : function( direction ) {
			var current = F.current;

			if (current) {
				F.jumpto( current.index - 1, (isString(direction) ? direction : current.direction.prev) );
			}
		},

		next : function( direction ) {
			var current = F.current;

			if (current) {
				F.jumpto( current.index + 1, (isString(direction) ? direction : current.direction.next) );
			}
		},

		jumpto : function( index, direction ) {
			var current = F.current;

			if (!(F.coming && F.coming.index === index)) {
				F.cancel();

				if (current.index == index) {
					direction = null;

				} else if (!direction) {
					direction = current.direction[ index > current.index ? 'next' : 'prev' ];
				}

				F.direction = direction;

				F._start( index );
			}
		}
	});

	$.extend(F, {
		guessType : function(item, href) {
			var rez  = item && item.prop('class') ? item.prop('class').match(/fancybox\.(\w+)/) : 0,
				type = false;

			if (rez) {
				return rez[1];
			}

			if (isString(href)) {
				if (href.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp)((\?|#).*)?$)/i)) {
					type = 'image';

				} else if (href.match(/\.(swf)((\?|#).*)?$/i)) {
					type = 'swf';

				} else if (href.charAt(0) === '#') {
					type = 'inline';
				}

			} else if (isString(item)) {
				type = 'html';
			}

			return type;
		},

		trigger: function (event, o) {
			var ret, obj = o || F.coming || F.current;

			if (!obj) {
				return;
			}

			if ($.isFunction( obj[event] )) {
				ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
			}

			// Cancel further execution if afterClose callback has opened new instance
			if (ret === false || (event === 'afterClose' && F.isActive) ) {
				return false;
			}

			if (obj.helpers) {
				$.each(obj.helpers, function (helper, opts) {
					var helperObject = F.helpers[helper],
						helperOpts;

					if (opts && helperObject && $.isFunction(helperObject[event])) {
						helperOpts = $.extend(true, {}, helperObject.defaults, opts);

						helperObject.opts = helperOpts;

						helperObject[event](helperOpts, obj );
					}
				});
			}

			$.event.trigger(event);
		},

		// Center inside viewport
		reposition: function (e, object) {
			var obj  = object || F.current,
				wrap = obj && obj.wrap,
				pos;

			if (F.isOpen && wrap) {
				pos = F._getPosition( obj );

				if (e === false || (e && e.type === 'scroll')) {
					wrap.stop(true).animate(pos, 200).css('overflow', 'visible');

				} else {
					wrap.css(pos);
				}
			}
		},

		update: function (e) {
			var type    = (e && e.type),
				timeNow = getTime(),
				current = F.current,
				width;

			if (!current || !F.isOpen ) {
				return;
			}

			if (type === 'scroll') {
				if (F.wrap.outerHeight(true) > F.getViewport().h) {
					return;
				}

				if (F.didUpdate) {
					clearTimeout( F.didUpdate );
				}

				F.didUpdate = setTimeout(function() {
					F.reposition(e);

					F.didUpdate = null;
				}, 50);

				return;
			}

			if (F.lock) {
				F.lock.css('overflow', 'hidden');
			}

			F._setDimension();

			F.reposition(e);

			if (F.lock) {
				F.lock.css('overflow', 'auto');
			}

			// Re-center float type caption
			if (current.caption.type === 'float') {
				width = F.getViewport().w - (F.wrap.outerWidth(true)  - F.inner.width() );

				current.caption.wrap.css('width', width).css('marginLeft', (width * 0.5 - F.inner.width() * 0.5) * -1 );
			}

			if (current.expander) {
				if ( current.canShrink) {
					$(".fancybox-expand").show().attr('title', current.locales[ current.locale ].SHRINK  );

				} else if (current.canExpand) {
					$(".fancybox-expand").show().attr('title', current.locales[ current.locale ].EXPAND   );

				} else {
					$(".fancybox-expand").hide();
				}
			}

			F.trigger('onUpdate');
		},

		// Shrink content to fit inside viewport or restore if resized
		toggle: function ( action ) {
			var current = F.current;

			if (current && F.isOpen) {
				F.current.fitToView = $.type(action) === "boolean" ? action : !F.current.fitToView;

				F.update( true );
			}
		},

		hideLoading: function () {
			$('#fancybox-loading').remove();
		},

		showLoading: function () {
			var el, view;

			F.hideLoading();

			el = $('<div id="fancybox-loading"></div>').click(F.cancel).appendTo('body');

			if (!F.defaults.fixed) {
				view = F.getViewport();

				el.css({
					position : 'absolute',
					top  : (view.h * 0.5) + view.y,
					left : (view.w * 0.5) + view.x
				});
			}
		},

		getViewport: function () {
			var rez;

			if (F.lock) {
				rez = {
					x: F.lock.scrollLeft(),
					y: F.lock.scrollTop(),
					w: F.lock[0].clientWidth,
					h: F.lock[0].clientHeight
				};

			} else {
				rez = {
					x: W.scrollLeft(),
					y: W.scrollTop(),

					// See http://bugs.jquery.com/ticket/6724
					w : isTouch && window.innerWidth  ? window.innerWidth  : W.width(),
					h : isTouch && window.innerHeight ? window.innerHeight : W.height()
				};
			}

			return rez;
		},

		unbind : function() {
			if (isQuery(F.wrap)) {
				F.wrap.unbind('.fb');
			}

			if (isQuery(F.inner)) {
				F.inner.unbind('.fb');
			}

			D.unbind('.fb');
			W.unbind('.fb');
		},

		rebind: function () {
			var current = F.current,
				keys;

			F.unbind();

			if (!current || !F.isOpen) {
				return;
			}

			// Changing document height on iOS devices triggers a 'resize' event,
			// that can change document height... repeating infinitely
			W.bind('orientationchange.fb' + (isTouch ? '' : ' resize.fb') + (current.autoCenter && !current.locked ? ' scroll.fb' : ''), F.update);

			keys = current.keys;

			if (keys) {
				D.bind('keydown.fb', function (e) {
					var code   = e.which || e.keyCode,
						target = e.target || e.srcElement;

					// Skip esc key if loading, because showLoading will cancel preloading
					if (code === 27 && F.coming) {
						return false;
					}

					// Ignore key combinations and key events within form elements
					if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && !(target && (target.type || $(target).is('[contenteditable]')))) {
						$.each(keys, function(i, val) {
							//if (current.group.length > 1 && val[ code ] !== undefined) {
							if (val[ code ] !== undefined) {
								e.preventDefault();

								if (current.group.length > 1) {
									F[ i ]( val[ code ] );
								}

								return false;
							}

							if ($.inArray(code, val) > -1) {
								e.preventDefault();

								if (i === 'play') {
									F.slideshow.toggle();
								} else {
									F[ i ] ();
								}

								return false;
							}
						});
					}
				});
			}

			F.lastScroll = getTime();

			if (current.mouseWheel && F.group.length > 1) {
				F.wrap.bind('DOMMouseScroll.fb mousewheel.fb MozMousePixelScroll.fb', function (event) {
					var e       = event.originalEvent,
						el      = e.target || 0,
						delta   = (e.wheelDelta || e.detail || 0),
						deltaX  = e.wheelDeltaX || 0,
						deltaY  = e.wheelDeltaY || 0,
						now     = getTime();

					if (((el && el.style && !(el.style.overflow && el.style.overflow === 'hidden') && ((el.clientWidth && el.scrollWidth > el.clientWidth) || (el.clientHeight && el.scrollHeight > el.clientHeight)))) ) {
						return;
					}

					if (delta === 0 || (F.current && F.current.canShrink)) {
						return;
					}

					e.stopPropagation();

					if (F.lastScroll && (now - F.lastScroll) < 80) {
						F.lastScroll = now;
						return;
					}

					F.lastScroll = now;

					if (e.axis) {
						if (e.axis === e.HORIZONTAL_AXIS) {
							deltaX = delta * -1;

						} else if (e.axis === e.VERTICAL_AXIS) {
							deltaY = delta * -1;
						}
					}

					if ( deltaX === 0 ) {
						if (deltaY > 0) {
							F.prev( 'down' );

						} else {
							F.next( 'up' );
						}

					} else {
						if (deltaX > 0) {
							F.prev( 'right' );

						} else {
							F.next( 'left' );
						}
					}
				});
			}

			F.touch.init();
		},

		rebuild : function() {
			var current = F.current;

			current.wrap.find('.fancybox-nav, .fancybox-close, .fancybox-expand').remove();

			// Create navigation arrows
			if (current.arrows && F.group.length > 1) {
				if (current.loop || current.index > 0) {
					$( F._translate( current.tpl.prev) ).appendTo(F.inner).bind('click.fb', F.prev);
				}

				if (current.loop || current.index < F.group.length - 1) {
					$( F._translate( current.tpl.next) ).appendTo(F.inner).bind('click.fb', F.next);
				}
			}

			// Create a close button
			if (current.closeBtn) {
				$( F._translate( current.tpl.closeBtn) ).appendTo(F.wrap).bind('click.fb', F.close);
			}

			// Add expand button to image
			if (current.expander && current.type === 'image') {
				$('<a title="Expand image" class="fancybox-expand" href="javascript:;"></a>')
					.appendTo( F.inner )
					.bind('click.fb', F.toggle);

				if ( !current.canShrink && !current.canExpand) {

				}
			}
		},

		// Create upcoming object and prepare for loading the content
		_start: function( index ) {
			var coming,
				type;

			// Check index and get object from the groups
			if (F.opts.loop) {
				if (index < 0) {
					index = F.group.length + (index % F.group.length);
				}

				index = index % F.group.length;
			}

			coming = F.group[ index ];

			if (!coming) {
				return false;
			}

			// Add all properties
			coming = $.extend(true, {}, F.opts, coming);

			/*
			 * Add reference to the group, so it`s possible to access from callbacks, example:
			 * afterLoad : function() {
			 *     this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 * }
			 */

			coming.group  = F.group;
			coming.index  = index;

			// Give a chance for callback or helpers to update coming item (type, title, etc)
			F.coming = coming;

			if (false === F.trigger('beforeLoad')) {
				F.coming = null;

				return;
			}

			F.isActive = true;

			// Build the neccessary markup
			F._build();

			// If user will press the escape-button, the request will be canceled
			D.bind('keydown.loading', function(e) {
				if ((e.which || e.keyCode) === 27) {
					D.unbind('.loading');

					e.preventDefault();

					F.cancel();
				}
			});

			// Show overlay
			if (coming.overlay && coming.overlay.showEarly) {
				F.overlay.open( coming.overlay );
			}

			// Load content
			type = coming.type;

			if (type === 'image') {
				F._loadImage();

			} else if (type === 'ajax') {
				F._loadAjax();

			} else if (type === 'iframe') {
				F._loadIframe();

			} else if (type === 'inline') {
				F._loadInline();

			} else if (type === 'html' || type === 'swf') {
				F._afterLoad();

			} else {
				F._error();
			}
		},

		_build : function() {
			var coming  = F.coming,
				captionType = coming.caption.type,
				wrap,
				inner,
				scrollV,
				scrollH;

			coming.wrap  = wrap  = $('<div class="fancybox-wrap"></div>').appendTo( coming.parent || 'body' ).addClass('fancybox-' + coming.theme);
			coming.inner = inner = $('<div class="fancybox-inner"></div>').appendTo( wrap );

			coming[ captionType === 'outside' || captionType === 'float' ? 'inner' : 'wrap' ].addClass('fancybox-skin fancybox-' + coming.theme + '-skin');

			if (coming.locked && coming.overlay && F.defaults.fixed) {
				if (!F.lock) {
					F.lock = $('<div id="fancybox-lock"></div>').appendTo( wrap.parent() );
				}

				F.lock.unbind().append( wrap );

				if (coming.overlay.closeClick) {
					F.lock.click(function(e) {
						if ($(e.target).is(F.lock)) {
							F.close();
						}
					});
				}

				// Compensate missing page scrolling by increasing margin
				if (D.height() > W.height() || H.css('overflow-y') === 'scroll') {
					$('*:visible').filter(function(){
						return ($(this).css('position') === 'fixed' && !$(this).hasClass("fancybox-overlay") && $(this).attr('id') !== "fancybox-lock");
					}).addClass('fancybox-margin');

					H.addClass('fancybox-margin');
				}

				// Workaround for FF jumping bug
				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				H.addClass('fancybox-lock');

				W.scrollTop( scrollV ).scrollLeft( scrollH );
			}

			F.trigger('onReady');
		},

		_error: function ( type ) {
			if (!F.coming) {
				return;
			}

			$.extend(F.coming, {
				type       : 'html',
				autoWidth  : true,
				autoHeight : true,
				closeBtn   : true,
				minWidth   : 0,
				minHeight  : 0,
				padding    : [15, 15, 15, 15],
				scrolling  : 'visible',
				hasError   : type,
				content    : F._translate( F.coming.tpl.error )
			});

			F._afterLoad();
		},

		_loadImage: function () {
			// Reset preload image so it is later possible to check "complete" property
			var img = F.imgPreload = new Image();

			img.onload = function () {
				this.onload = this.onerror = null;

				$.extend(F.coming, {
					width   : this.width,
					height  : this.height,
					content : $(this).addClass('fancybox-image')
				});

				F._afterLoad();
			};

			img.onerror = function () {
				this.onload = this.onerror = null;

				F._error( 'image' );
			};

			img.src = F.coming.href;

			if (img.complete !== true || img.width < 1) {
				F.showLoading();
			}
		},

		_loadAjax: function () {
			var coming = F.coming,
				href   = coming.href,
				hrefParts,
				selector;

			hrefParts = href.split(/\s+/, 2);
			href      = hrefParts.shift();
			selector  = hrefParts.shift();

			F.showLoading();

			F.ajaxLoad = $.ajax($.extend({}, coming.ajax, {
				url: coming.href,
				error: function (jqXHR, textStatus) {
					if (F.coming && textStatus !== 'abort') {
						F._error( 'ajax', jqXHR );

					} else {
						F.hideLoading();
					}
				},
				success: function (data, textStatus) {
					if (textStatus === 'success') {
						if (selector) {
							data = $('<div>').html(data).find(selector);
						}

						coming.content = data;

						F._afterLoad();
					}
				}
			}));
		},

		_loadIframe: function() {
			var coming = F.coming,
				iframe;

			coming.content = iframe = $(coming.tpl.iframe.replace(/\{rnd\}/g, new Date().getTime()))
				.attr('scrolling', isTouch ? 'auto' : coming.iframe.scrolling);

			if (coming.iframe.preload) {
				F.showLoading();

				F._setDimension( coming );

				coming.wrap.addClass('fancybox-tmp');

				iframe.one('load.fb', function() {
					if (coming.iframe.preload) {
						$(this).data('ready', 1);

						$(this).bind('load.fb', F.update);

						F._afterLoad();
					}
				});
			}

			iframe.attr('src', coming.href).appendTo(coming.inner);

			if (!coming.iframe.preload) {
				F._afterLoad();

			} else if (iframe.data('ready') !== 1) {
				F.showLoading();
			}
		},

		_loadInline : function() {
			var coming = F.coming,
				href   = coming.href;

			coming.content = $( isString(href) ? href.replace(/.*(?=#[^\s]+$)/, '') : href ); //strip for ie7

			if (coming.content.length) {
				F._afterLoad();

			} else {
				F._error();
			}
		},

		_preloadImages: function() {
			var group   = F.group,
				current = F.current,
				len     = group.length,
				cnt     = current.preload ? Math.min(current.preload, len - 1) : 0,
				item,
				i;

			for (i = 1; i <= cnt; i += 1) {
				item = group[ (current.index + i ) % len ];

				if (item && item.type === 'image' && item.href) {
					new Image().src = item.href;
				}
			}
		},

		_afterLoad : function() {
			var current  = F.coming,
				previous = F.current;

			D.unbind('.loading');

			if (!current || F.isActive === false || false === F.trigger('afterLoad', current, previous)) {
				F.hideLoading();

				if (current && current.wrap) {
					removeWrap( current.wrap );
				}

				if (!previous) {
					F._afterZoomOut( current );
				}

				F.coming = null;

				return;
			}

			$.extend(F, {
				wrap     : current.wrap.addClass('fancybox-type-' + current.type + ' fancybox-' + (isTouch ? 'mobile' : 'desktop') + ' fancybox-' + current.theme + '-' +  (isTouch ? 'mobile' : 'desktop')  + ' ' + current.wrapCSS),
				inner    : current.inner,
				current  : current,
				previous : previous
			});

			// Set content, margin/padding, caption, etc
			F._prepare();

			// Give a chance for helpers or callbacks to update elements
			F.trigger('beforeShow', current, previous);

			F.isOpen = false;
			F.coming = null;

			// Set initial dimension
			F._setDimension();

			F.hideLoading();

			// Open overlay if is not yet open
			if (current.overlay && !F.overlay.el) {
				F.overlay.open( current.overlay );
			}

			F.transitions.open();
		},

		_prepare : function() {
			var current     = F.current,
				content     = current.content || '',
				wrap        = current.wrap,
				inner       = current.inner,
				margin      = current.margin,
				padding     = current.padding,
				href        = current.href,
				type        = current.type,
				scrolling   = current.scrolling,
				caption     = current.caption,
				captionText = current.title,
				captionType = caption.type,
				placeholder = 'fancybox-placeholder',
				display     = 'fancybox-display',
				embed;

			if (type !== 'iframe' && isQuery(content) && content.length) {
				if (!content.data(placeholder)) {
					content.data(display, content.css('display'))
						.data(placeholder, $('<div class="' + placeholder + '"></div>').insertAfter( content ).hide() );
				}

				content = content.show().detach();

				current.wrap.bind('onReset', function () {
					if ($(this).find(content).length) {
						content.css('display', content.data(display))
							.replaceAll( content.data(placeholder) )
							.data(placeholder, false)
							.data(display, false);
					}
				});
			}

			if (type === 'swf') {
				content = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + href + '"></param>';
				embed   = '';

				$.each(current.swf, function(name, val) {
					content += '<param name="' + name + '" value="' + val + '"></param>';
					embed   += ' ' + name + '="' + val + '"';
				});

				content += '<embed src="' + href + '" type="application/x-shockwave-flash" width="100%" height="100%"' + embed + '></embed></object>';
			}

			if (!(isQuery(content) && content.parent().is(current.inner))) {
				current.inner.append( content );

				current.content = current.inner.children(':last');
			}

			// Add margin / padding
			$.each(["Top", "Right", "Bottom", "Left"], function(i, v) {
				if (margin[ i ]) {
					wrap.css('margin' + v, getValue(margin[ i ]));
				}

				if (padding[ i ]) {
					if (!(v === 'Bottom' && captionType === 'outside')) {
						wrap.css('padding' + v, getValue(padding[ i ])  );
					}

					if (captionType === 'outside' || captionType === 'float') {

						inner.css('border' + v + 'Width', getValue(padding[ i ]));

						if (v === 'Top' || v === 'Left') {
							inner.css('margin' + v, getValue(padding[ i ] * -1));
						}
					}
				}
			});

			// Add caption
			if ($.isFunction(captionText)) {
				captionText = captionText.call(current.element, current);
			}

			if (isString(captionText) && $.trim(captionText) !== '') {
				current.caption.wrap = $('<div class="fancybox-title fancybox-title-' + captionType + '-wrap">' + captionText + '</div>').appendTo( current[ captionType === 'over' ? 'inner' : 'wrap' ] );

				if (captionType === 'float') {
					current.caption.wrap.width( F.getViewport().w - (F.wrap.outerWidth(true)  - F.inner.width() ) ).wrapInner('<div></div>');
				}
			}
		},

		_setDimension: function( object ) {
			var view      = F.getViewport(),
				current   = object || F.current,
				wrap      = current.wrap,
				inner     = current.inner,
				width     = current.width,
				height    = current.height,
				minWidth  = current.minWidth,
				minHeight = current.minHeight,
				maxWidth  = current.maxWidth,
				maxHeight = current.maxHeight,
				margin    = current.margin,
				scrollOut = current.scrollOutside ? current.scrollbarWidth : 0,
				margin    = current.margin,
				padding   = current.padding,
				scrolling = current.scrolling,
				steps     = 1,
				scrollingX,
				scrollingY,
				hSpace,
				wSpace,
				origWidth,
				origHeight,
				ratio,
				iframe,
				body,
				maxWidth_,
				maxHeight_,
				width_,
				height_,
				canShrink,
				canExpand;

			// Set scrolling
			scrolling  = scrolling.split(',');
			scrollingX = scrolling[0];
			scrollingY = scrolling[1] || scrollingX;

			current.inner
				.css('overflow-x', scrollingX === 'yes' ? 'scroll' : (scrollingX === 'no' ? 'hidden' : scrollingX))
				.css('overflow-y', scrollingY === 'yes' ? 'scroll' : (scrollingY === 'no' ? 'hidden' : scrollingY));

			wSpace = margin[1] + margin[3] + padding[1] + padding[3];
			hSpace = margin[0] + margin[2] + padding[0] + padding[2];

			// Calculations for the content
			minWidth  = getScalar( isPercentage(minWidth) ? getScalar(minWidth, 'w') - wSpace : minWidth );
			maxWidth  = getScalar( isPercentage(maxWidth) ? getScalar(maxWidth, 'w') - wSpace : maxWidth );

			minHeight = getScalar( isPercentage(minHeight) ? getScalar(minHeight, 'h') - hSpace : minHeight );
			maxHeight = getScalar( isPercentage(maxHeight) ? getScalar(maxHeight, 'h') - hSpace : maxHeight );

			origWidth  = getScalar( isPercentage(width)  ? getScalar(width,  'w') - wSpace : width  );
			origHeight = getScalar( isPercentage(height) ? getScalar(height, 'h') - hSpace : height );

			if (current.fitToView) {
				maxWidth  = Math.min(maxWidth,  getScalar('100%', 'w') - wSpace );
				maxHeight = Math.min(maxHeight, getScalar('100%', 'h') - hSpace );
			}

			maxWidth_  = view.w;
			maxHeight_ = view.h;

			if (current.type === 'iframe') {
				iframe = current.content;

				wrap.removeClass('fancybox-tmp');

				if ((current.autoWidth || current.autoHeight) && iframe && iframe.data('ready') === 1) {

					try {
						if (iframe[0].contentWindow && iframe[0].contentWindow.document.location) {
							body = iframe.contents().find('body');

							inner.addClass( 'fancybox-tmp' );

							inner.width( screen.width - wSpace ).height( 99999 );

							if (scrollOut) {
								body.css('overflow-x', 'hidden');
							}

							if (current.autoWidth) {
								origWidth = body.outerWidth(true);
							}

							if (current.autoHeight) {
								origHeight = body.outerHeight(true);
							}

							inner.removeClass( 'fancybox-tmp' );
						}

					} catch (e) {}
				}

			} else if ( (current.autoWidth || current.autoHeight) && !(current.type === 'image' || current.type === 'swf') ) {
				inner.addClass( 'fancybox-tmp' );

				// Set width or height in case we need to calculate only one dimension
				if (current.autoWidth) {
					inner.width( 'auto' );

				} else {
					inner.width( maxWidth );
				}

				if (current.autoHeight) {
					inner.height( 'auto' );

				} else {
					inner.height( maxHeight );
				}

				if (current.autoWidth) {
					origWidth = inner[0].scrollWidth || inner.width();
				}

				if (current.autoHeight) {
					origHeight = inner[0].scrollHeight || inner.height();
				}

				inner.removeClass( 'fancybox-tmp' );
			}

			width  = origWidth;
			height = origHeight;
			ratio  = origWidth / origHeight;

			if (!current.autoResize) {
				wrap.css({
					width  : getValue( width ),
					height : 'auto'
				});

				inner.css({
					width  : getValue( width ),
					height : getValue( height )
				});
				return;
			}

			if (current.aspectRatio) {
				if (width > maxWidth) {
					width  = maxWidth;
					height = width / ratio;
				}

				if (height > maxHeight) {
					height = maxHeight;
					width  = height * ratio;
				}

				if (width < minWidth) {
					width  = minWidth;
					height = width / ratio;
				}

				if (height < minHeight) {
					height = minHeight;
					width  = height * ratio;
				}

			} else {
				width = Math.max(minWidth, Math.min(width, maxWidth));

				if (current.autoHeight && current.type !== 'iframe') {
					inner.width( width );

					origHeight = height = inner[0].scrollHeight;
				}

				height = Math.max(minHeight, Math.min(height, maxHeight));
			}

			// Wrap element has to have fixed width, because long title can expand it
			wrap.css({
				width  : getValue( width ),
				height : 'auto'
			});

			inner.css({
				width  : getValue( width ),
				height : getValue( height )
			});

			width_  = getScalar( wrap.outerWidth(true) );
			height_ = getScalar( wrap.outerHeight(true) );

			if (current.fitToView) {
				// Since we do not know how many lines will be at the final, we need to
				// resize box till it fits inside max dimensions
				if (current.aspectRatio) {
					while ((width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight) {
						if (steps++ > 30) {
							break;
						}

						height = Math.max(minHeight, Math.min(maxHeight, height - 10));
						width  = getScalar(height * ratio);

						if (width < minWidth) {
							width  = minWidth;
							height = getScalar(width / ratio);
						}

						if (width > maxWidth) {
							width  = maxWidth;
							height = getScalar(width / ratio);
						}

						wrap.css({
							width  : getValue( width )
						});

						inner.css({
							width  : getValue( width ),
							height : getValue( height )
						});

						width_  = getScalar( wrap.outerWidth(true) );
						height_ = getScalar( wrap.outerHeight(true) );
					}

				} else {
					width  = Math.max(minWidth,  Math.min(width,  width  - (width_  - maxWidth_  )));
					height = Math.max(minHeight, Math.min(height, height - (height_ - maxHeight_ )));
				}
			}


			if (scrollOut && scrollingX === 'auto' && (height < inner[0].scrollHeight || (isQuery(current.content) && current.content[0] && height < current.content[0].offsetHeight)) && (width + wSpace + scrollOut) < maxWidth) {
				width += scrollOut;
			}

			wrap.css({
				width  : width
			});

			inner.css({
				width  : getValue( width ),
				height : getValue( height )
			});

			width_  = getScalar( wrap.outerWidth(true) );
			height_ = getScalar( wrap.outerHeight(true) );

			canShrink = (width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight;
			canExpand = (width_ < maxWidth_ || height_ < maxHeight_) && ( current.aspectRatio ? (width < maxWidth && height < maxHeight && width < origWidth && height < origHeight) : ((width < maxWidth || height < maxHeight) && (width < origWidth || height < origHeight)) );

			current.canShrink = canShrink;
			current.canExpand = canExpand;

			if (!iframe && current.autoHeight && height > minHeight && height < maxHeight && !canExpand) {
				inner.height('auto');
			}
		},

		_getPosition: function( object ) {
			var obj   = object || F.current,
				wrap  = obj.wrap,
				view  = F.getViewport(),
				pos   = {},
				top   = view.y,
				left  = view.x;

			pos = {
				top    : getValue( Math.max(top,  top  + ((view.h - wrap.outerHeight(true)) * obj.topRatio)) ),
				left   : getValue( Math.max(left, left + ((view.w - wrap.outerWidth(true))  * obj.leftRatio)) ),
				width  : getValue( wrap.width() ),
				height : getValue( wrap.height() )
			};

			return pos;
		},

		_afterZoomIn : function() {
			var current   = F.current;

			if (!current) {
				return;
			}

			if (F.lock) {
				F.lock.css('overflow', 'auto');
			}

			F.isOpen = F.isOpened = true;

			F.rebuild();

			F.rebind();

			if (current.caption && current.caption.wrap) {
				current.caption.wrap.show().css({
					'visibility' : 'visible',
					'opacity'    : 0,
					'left'       : 0
				})
				.animate({opacity:1}, "fast");
			}

			F.update();

			F.wrap.css('overflow', 'visible').addClass('fancybox-open').focus();

			F[ F.wrap.hasClass('fancybox-skin') ? 'wrap' : 'inner' ].addClass('fancybox-' + current.theme + '-skin-open');

			if (current.caption && current.caption.wrap) {
				current.caption.wrap.show().css('left', 0).animate({opacity:1}, "fast");
			}

			// Add empty element to simulate bottom margin, this trick helps to avoid extra element
			if (current.margin[2] > 0) {
				$('<div class="fancybox-spacer"></div>').css('height', getValue( current.margin[2] - 2) ).appendTo( F.wrap );
			}

			F.trigger('afterShow');

			F._preloadImages();

			if (current.autoPlay && !F.slideshow.isActive) {
				F.slideshow.start();
			}
		},

		_afterZoomOut : function(obj) {
			var cleanup = function() {
				removeWrap('.fancybox-wrap');
			};

			F.hideLoading();

			obj = obj || F.current;

			if (obj && obj.wrap) {
				obj.wrap.hide();
			}

			$.extend(F, {
				group     : [],
				opts      : {},
				coming    : null,
				current   : null,
				isActive  : false,
				isOpened  : false,
				isOpen    : false,
				isClosing : false,
				wrap      : null,
				skin      : null,
				inner     : null
			});

			F.trigger('afterClose', obj);

			if (!F.coming && !F.current) {
				if (obj.overlay) {
					F.overlay.close( obj.overlay, cleanup );
				} else {
					cleanup();
				}
			}
		},

		_translate : function( str ) {
			var obj = F.coming || F.current,
				arr = obj.locales[ obj.locale ];

			return str.replace(/\{\{(\w+)\}\}/g, function(match, n) {
				var value = arr[n];

				if (value === undefined) {
					return match;
				}

				return value;
			});
		}
	});

	/*
	 *	Transition object
	 */

	F.transitions = {
		_getOrig: function( object ) {
			var obj     = object || F.current,
				wrap    = obj.wrap,
				element = obj.element,
				orig    = obj.orig,
				view    = F.getViewport(),
				pos     = {},
				width   = 50,
				height  = 50;

			if (!orig && element && element.is(':visible')) {
				orig = element.find('img:first:visible');

				if (!orig.length) {
					orig = element;
				}
			}

			// If there is no orig element, maybe only the first thumbnail is visible
			if (!orig && obj.group[0].element) {
				orig = obj.group[0].element.find('img:visible:first');
			}

			if (isQuery(orig) && orig.is(':visible')) {
				pos = orig.offset();

				if (orig.is('img')) {
					width  = orig.outerWidth();
					height = orig.outerHeight();
				}

				if (F.lock) {
					pos.top  -= W.scrollTop();
					pos.left -= W.scrollLeft();
				}

			} else {
				pos.top  = view.y + (view.h - height) * obj.topRatio;
				pos.left = view.x + (view.w - width)  * obj.leftRatio;
			}

			pos = {
				top      : getValue( pos.top  - (wrap.outerHeight(true) - wrap.height() ) * 0.5 ),
				left     : getValue( pos.left - (wrap.outerWidth(true)  - wrap.width() )  * 0.5 ),
				width    : getValue( width ),
				height   : getValue( height )
			};

			return pos;
		},

		_getCenter: function( object ) {
			var obj   = object || F.current,
				wrap  = obj.wrap,
				view  = F.getViewport(),
				pos   = {},
				top   = view.y,
				left  = view.x;

			pos = {
				top    : getValue( Math.max(top,  top  + ((view.h - wrap.outerHeight(true)) * obj.topRatio)) ),
				left   : getValue( Math.max(left, left + ((view.w - wrap.outerWidth(true))  * obj.leftRatio)) ),
				width  : getValue( wrap.width() ),
				height : getValue( wrap.height() )
			};

			return pos;
		},

		_prepare : function( object, forClosing ) {
			var obj   = object || F.current,
				wrap  = obj.wrap,
				inner = obj.inner;

			// Little trick to avoid animating both elements and to improve performance
			wrap.height( wrap.height() );

			inner.css({
				'width'  : (inner.width() *  100 / wrap.width() )  + '%',
				'height' : (Math.floor( (inner.height() * 100 / wrap.height() ) * 100 ) / 100 ) + '%'
			});

			if (forClosing === true) {
				wrap.find('.fancybox-title, .fancybox-spacer, .fancybox-close, .fancybox-nav').remove();
			}

			inner.css('overflow', 'hidden');
		},

		fade : function( object, stage ) {
			var pos = this._getCenter( object ),
				opa = {opacity: 0};

			return ((stage === 'open' || stage === 'changeIn') ? [ $.extend(pos, opa), {opacity: 1} ] : [ {}, opa ]);
		},

		drop : function( object, stage ) {
			var a = $.extend(this._getCenter( object ), {opacity: 1}),
				b = $.extend({}, a, {opacity: 0, top: getValue( Math.max( F.getViewport().y - object.margin[0], getScalar( a.top ) - 200 ) )});

			return ((stage === 'open' || stage === 'changeIn') ? [ b, a ] : [ {}, b ]);
		},

		elastic : function( object, stage ) {
			var wrap      = object.wrap,
				margin    = object.margin,
				view      = F.getViewport(),
				direction = F.direction,
				pos       = this._getCenter( object ),
				from      = $.extend({}, pos),
				to        = $.extend({}, pos),
				prop,
				amount,
				value;

			if (stage === 'open') {
				from = this._getOrig( object );

			} else if (stage === 'close') {
				from = {};
				to   = this._getOrig( object );

			} else if (direction) {
				// Calculate max distance and try to avoid scrollbars
				prop    = (direction === 'up' || direction === 'down') ? 'top' : 'left';
				amount  = (direction === 'up' || direction === 'left') ? 200 : -200;

				if (stage === 'changeIn') {
					value = getScalar(from[ prop ]) + amount;

					if (direction === 'left') {
						// from viewport right to center
						value = Math.min( value, view.x + view.w - margin[3] - wrap.outerWidth() - 1 );

					} else if (direction === 'right') {
						// from viewport left to center
						value = Math.max( value, view.x - margin[1] );

					} else if (direction === 'up') {
						// from viewport bottom to center
						value = Math.min( value, view.y + view.h - margin[0] - wrap.outerHeight() - 1);

					} else {
						// down - from viewport top to center
						value = Math.max( value, view.y - margin[2] );
					}

					from[ prop ] = value;

				} else {
					value = getScalar(wrap.css(prop)) - amount;
					from  = {};

					if (direction === 'left') {
						// from viewport center to left
						value = Math.max( value, view.x - margin[3] );

					} else if (direction === 'right') {
						// from viewport center to right
						value = Math.min( value, view.x + view.w - margin[1]  - wrap.outerWidth() - 1 );

					} else if (direction === 'up') {
						// from viewport center to top
						value = Math.max( value, view.y - margin[0]  );

					} else {
						// down - from center to bottom
						value = Math.min( value, view.y + view.h - margin[2] - wrap.outerHeight() - 1 );
					}

					to[ prop ] = value;
				}
			}

			if (stage === 'open' || stage === 'changeIn') {
				from.opacity = 0;
				to.opacity   = 1;

			} else {
				to.opacity   = 0;
			}

			return [ from, to ];
		},

		open : function() {
			var current   = F.current,
				previous  = F.previous,
				direction = F.direction,
				effect,
				pos,
				speed,
				easing,
				stage;

			if (previous) {
				previous.wrap.stop(true).removeClass('fancybox-opened');
			}

    		if ( F.isOpened ) {
    			effect = current.nextEffect,
				speed  = current.nextSpeed;
				easing = current.nextEasing;
				stage  = 'changeIn';

			} else {
				effect = current.openEffect;
				speed  = current.openSpeed;
				easing = current.openEasing;
				stage  = 'open';
			}

			/*
			 *	Open current item
			 */

			if (effect === 'none') {
				F._afterZoomIn();

			} else {
				pos = this[ effect ]( current, stage );

				if (effect === 'elastic') {
					this._prepare( current );
				}

				current.wrap.css( pos[ 0 ] );

				current.wrap.animate(
					pos[ 1 ],
					speed,
					easing,
					F._afterZoomIn
				);
			}

			/*
			 *	Close previous item
			 */
			if (previous) {
				if (!F.isOpened || previous.prevEffect === 'none') {
					// Remove previous item if it has not fully opened
					removeWrap( $('.fancybox-wrap').not( current.wrap ) );

				} else {
					previous.wrap.stop(true).removeClass('fancybox-opened');

					pos = this[ previous.prevEffect ]( previous, 'changeOut' );

					this._prepare( previous, true );

					previous.wrap.animate(
						pos[ 1 ],
						previous.prevSpeed,
						previous.prevEasing,
						function() {
							removeWrap( previous.wrap );
						}
					);
				}
			}
		},

		close : function() {
			var current  = F.current,
				wrap     = current.wrap.stop(true).removeClass('fancybox-opened'),
				effect   = current.closeEffect,
				pos;

			if (effect === 'none') {
				return F._afterZoomOut();
			}

			this._prepare( current, true );

			pos = this[ effect ]( current, 'close' );

			wrap.addClass('fancybox-animating')
				.animate(
					pos[ 1 ],
					current.closeSpeed,
					current.closeEasing,
					F._afterZoomOut
				);
		}
	};

	/*
	 *	Slideshow object
	 */

	F.slideshow = {
		_clear : function () {
			if (this._timer) {
				clearTimeout(this._timer);
			}
		},
		_set : function () {
			this._clear();

			if (F.current && this.isActive) {
				this._timer = setTimeout(F.next, this._speed);
			}
		},

		_timer   : null,
		_speed   : null,

		isActive : false,

		start : function ( speed ) {
			var current = F.current;

			if (current && (current.loop || current.index < current.group.length - 1)) {
				this.stop();

				this.isActive = true;
				this._speed   = speed || current.playSpeed;

				D.bind({
					'beforeLoad.player' : $.proxy(this._clear, this),
					'onUpdate.player'   : $.proxy(this._set, this),
					'onCancel.player beforeClose.player' : $.proxy(this.stop, this)
				});

				this._set();

				F.trigger('onPlayStart');
			}
		},

		stop : function () {
			this._clear();

			D.unbind('.player');

			this.isActive = false;
			this._timer   = this._speed = null;

			F.trigger('onPlayEnd');
		},

		toggle : function() {
			if (this.isActive) {
				this.stop();

			} else {
				this.start.apply(this, arguments );
			}
		}
	};

	/*
	 *	Overlay object
	 */

	F.overlay = {
		el    : null,  // current handle
		theme : '',    // current theme

		// Public methods
		open : function(opts) {
			var that  = this,
				el    = this.el,
				fixed = F.defaults.fixed,
				opacity,
				theme;

			opts = $.extend({}, F.defaults.overlay, opts);

			if (el) {
				el.stop(true).removeAttr('style').unbind('.overlay');

			} else {
				el = $('<div class="fancybox-overlay' + (fixed ? ' fancybox-overlay-fixed' : '' ) + '"></div>').appendTo( opts.parent || 'body' );
			}

			if (opts.closeClick) {
				el.bind('click.overlay', function(e) {
					// fix Android touch event bubbling issue
					if (F.lastTouch && (getTime() - F.lastTouch) < 300) {
						return false;
					}

					if (F.isActive) {
						F.close();
					} else {
						that.close();
					}

					return false;
				});
			}

			theme = opts.theme || (F.coming ? F.coming.theme : 'default');

			if (theme !== this.theme) {
				el.removeClass('fancybox-' + this.theme + '-overlay')
			}

			this.theme = theme;

			el.addClass('fancybox-' + theme + '-overlay').css( opts.css );

			opacity = el.css('opacity');

			if (!this.el && opacity < 1 && opts.speedIn) {
				el.css({
					opacity : 0,
					filter  : 'alpha(opacity=0)'  // This fixes IE flickering
				})
				.fadeTo( opts.speedIn, opacity );
			}

			this.el = el;

			if (!fixed) {
				W.bind('resize.overlay', $.proxy( this.update, this) );

				this.update();
			}
		},

		close : function(opts, callback) {
			opts = $.extend({}, F.defaults.overlay, opts);

			if (this.el) {
				this.el.stop(true).fadeOut(opts.speedOut, function() {
					W.unbind('resize.overlay');

					$('.fancybox-overlay').remove();

					F.overlay.el = null;

					if ($.isFunction(callback)) {
						callback();
					}
				});
			}
		},

		update : function () {
			// Reset width/height so it will not mess
			this.el.css({width: '100%', height: '100%'});

			this.el.width(D.width()).height(D.height());
		}
	};

	/*
	 *	Touch object - adds swipe left/right events
	 */

	F.touch = {
		startX   : 0,
		wrapX    : 0,
		dx       : 0,
		isMoving : false,

		_start : function(e) {
			var current = F.current,
				data    = e.originalEvent.touches ? e.originalEvent.touches[0] : e,
				now     = getTime();

			if (!F.isOpen || F.wrap.is(':animated')  || !( $(e.target).is(F.inner) || $(e.target).parent().is(F.inner) )) {
				return;
			}

			if (F.lastTouch && (now - F.lastTouch) < 300) {
				e.preventDefault();

				F.lastTouch = now;

				this._cancel(true);

				F.toggle();

				return false;
			}

			F.lastTouch = now;

			if (F.wrap &&  F.wrap.outerWidth() > F.getViewport().w) {
				return;
			}

			e.preventDefault();

			if (data && F.wrap &&  F.wrap.outerWidth() < F.getViewport().w) {
				this.startX   = data.pageX;
				this.wrapX    = F.wrap.position().left;
				this.isMoving = true;

				F.inner
					.bind('touchmove.fb', $.proxy(this._move, this) )
					.one("touchend.fb touchcancel.fb", $.proxy(this._cancel, this) );
			}
		},

		_move : function(e) {
			var data = e.originalEvent.touches ? e.originalEvent.touches[0] : e,
				dx   = this.startX - data.pageX;

			if (!this.isMoving || !F.isOpen) {
				return;
			}

			this.dx = dx;

			if (F.current.wrap.outerWidth(true) <= W.width()) {

				if (Math.abs(dx) >= 50) {
					e.preventDefault();

					this.last = 0;

					this._cancel(true);

					if (dx > 0) {
						F.next('left');

					} else {
						F.prev('right');
					}

				} else if (Math.abs(dx) > 3) {
					e.preventDefault();

					this.last = 0;

					F.wrap.css('left', this.wrapX - dx);
				}
			}
		},

		_clear : function() {
			this.startX   = this.wrapX = this.dx = 0;
			this.isMoving = false;
		},

		_cancel : function( stop ) {
			if (F.inner) {
				F.inner.unbind('touchmove.fb');
			}

			if (F.isOpen && Math.abs(this.dx) > 3) {
				F.reposition(false);
			}

			this._clear();
		},

		init : function() {
			var that = this;

			if (F.inner && F.touch) {
				this._cancel(true);

				F.inner.bind('touchstart.fb', $.proxy(this._start, this));
			}
		}
	};

	/*
	 *	Add default easing
	 */

	if (!$.easing.easeOutQuad) {
		$.easing.easeOutQuad = function (x, t, b, c, d) {
			return -c *(t/=d)*(t-2) + b;
		};
	}

	/*
	 *
	 */

	D.ready(function() {
		var w1, w2, scrollV, scrollH;

		// Tests that need a body at doc ready
		if ( $.scrollbarWidth === undefined ) {
			$.scrollbarWidth = function() {
				var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body'),
					child  = parent.children(),
					width  = child.innerWidth() - child.height( 99 ).innerWidth();

				parent.remove();

				return width;
			};
		}

		if ( $.support.fixedPosition === undefined ) {
			$.support.fixedPosition = (function() {
				var elem  = $('<div style="position:fixed;top:20px;padding:0;margin:0;border:0;"></div>').appendTo('body'),
					fixed = elem.css( 'position' ) === 'fixed' && ((elem[0].offsetTop > 18 && elem[0].offsetTop < 22) || elem[0].offsetTop === 15 );

				elem.remove();

				return fixed;
			}());
		}

		$.extend(F.defaults, {
			scrollbarWidth : $.scrollbarWidth(),
			fixed  : $.support.fixedPosition,
			parent : $('body')
		});

		// Quick and dirty code to get page scroll-bar width and create CSS style
		// Workaround for FF jumping bug
		scrollV = W.scrollTop();
		scrollH = W.scrollLeft();

		w1 = $(window).width();

		H.addClass('fancybox-lock-test');

		w2 = $(window).width();

		H.removeClass('fancybox-lock-test');

		W.scrollTop( scrollV ).scrollLeft( scrollH );

		F.lockMargin = (w2 - w1);

		$("<style type='text/css'>.fancybox-margin{margin-right:" + F.lockMargin + "px;}</style>").appendTo("head");

	});

	// jQuery plugin initialization
	$.fn.fancybox = function (options) {
		var that     = this,
			selector = this.length ? this.selector : false,
			live     = (selector && selector.indexOf('()') < 0 && !(options && options.live === false));

		var handler  = function(e) {
			var collection = live ? $(selector) : that,
				group  = $(this).blur(),
				param  = options.groupAttr || 'data-fancybox-group',
				value  = group.attr( param ),
				tmp    = this.rel;

			if (!value && tmp && tmp !== 'nofollow') {
				param = 'rel';
				value = tmp;
			}

			if (value) {
				group = collection.filter('[' + param + '="' + value + '"]');

				options.index = group.index( this );
			}

			if (group.length) {
				e.preventDefault();

				F.open(group.get(), options);
			}
		};

		options = options || {};

		if (live) {
			D.undelegate(selector, 'click.fb-start').delegate(selector + ":not('.fancybox-close,.fancybox-nav,.fancybox-wrap')", 'click.fb-start', handler);

		} else {
			that.unbind('click.fb-start').bind('click.fb-start', handler);
		}

		return this;
	};

}(window, document, jQuery));
/*
 * Swiper 2.7.6
 * Mobile touch slider and framework with hardware accelerated transitions
 *
 * http://www.idangero.us/sliders/swiper/
 *
 * Copyright 2010-2015, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 *
 * Licensed under GPL & MIT
 *
 * Released on: February 11, 2015
 */
var Swiper=function(a,b){"use strict";function c(a,b){return document.querySelectorAll?(b||document).querySelectorAll(a):jQuery(a,b)}function d(a){return"[object Array]"===Object.prototype.toString.apply(a)?!0:!1}function e(){var a=G-J;return b.freeMode&&(a=G-J),b.slidesPerView>D.slides.length&&!b.centeredSlides&&(a=0),0>a&&(a=0),a}function f(){function a(a){var c,d,e=function(){"undefined"!=typeof D&&null!==D&&(void 0!==D.imagesLoaded&&D.imagesLoaded++,D.imagesLoaded===D.imagesToLoad.length&&(D.reInit(),b.onImagesReady&&D.fireCallback(b.onImagesReady,D)))};a.complete?e():(d=a.currentSrc||a.getAttribute("src"),d?(c=new Image,c.onload=e,c.onerror=e,c.src=d):e())}var d=D.h.addEventListener,e="wrapper"===b.eventTarget?D.wrapper:D.container;if(D.browser.ie10||D.browser.ie11?(d(e,D.touchEvents.touchStart,p),d(document,D.touchEvents.touchMove,q),d(document,D.touchEvents.touchEnd,r)):(D.support.touch&&(d(e,"touchstart",p),d(e,"touchmove",q),d(e,"touchend",r)),b.simulateTouch&&(d(e,"mousedown",p),d(document,"mousemove",q),d(document,"mouseup",r))),b.autoResize&&d(window,"resize",D.resizeFix),g(),D._wheelEvent=!1,b.mousewheelControl){if(void 0!==document.onmousewheel&&(D._wheelEvent="mousewheel"),!D._wheelEvent)try{new WheelEvent("wheel"),D._wheelEvent="wheel"}catch(f){}D._wheelEvent||(D._wheelEvent="DOMMouseScroll"),D._wheelEvent&&d(D.container,D._wheelEvent,j)}if(b.keyboardControl&&d(document,"keydown",i),b.updateOnImagesReady){D.imagesToLoad=c("img",D.container);for(var h=0;h<D.imagesToLoad.length;h++)a(D.imagesToLoad[h])}}function g(){var a,d=D.h.addEventListener;if(b.preventLinks){var e=c("a",D.container);for(a=0;a<e.length;a++)d(e[a],"click",n)}if(b.releaseFormElements){var f=c("input, textarea, select",D.container);for(a=0;a<f.length;a++)d(f[a],D.touchEvents.touchStart,o,!0),D.support.touch&&b.simulateTouch&&d(f[a],"mousedown",o,!0)}if(b.onSlideClick)for(a=0;a<D.slides.length;a++)d(D.slides[a],"click",k);if(b.onSlideTouch)for(a=0;a<D.slides.length;a++)d(D.slides[a],D.touchEvents.touchStart,l)}function h(){var a,d=D.h.removeEventListener;if(b.onSlideClick)for(a=0;a<D.slides.length;a++)d(D.slides[a],"click",k);if(b.onSlideTouch)for(a=0;a<D.slides.length;a++)d(D.slides[a],D.touchEvents.touchStart,l);if(b.releaseFormElements){var e=c("input, textarea, select",D.container);for(a=0;a<e.length;a++)d(e[a],D.touchEvents.touchStart,o,!0),D.support.touch&&b.simulateTouch&&d(e[a],"mousedown",o,!0)}if(b.preventLinks){var f=c("a",D.container);for(a=0;a<f.length;a++)d(f[a],"click",n)}}function i(a){var b=a.keyCode||a.charCode;if(!(a.shiftKey||a.altKey||a.ctrlKey||a.metaKey)){if(37===b||39===b||38===b||40===b){for(var c=!1,d=D.h.getOffset(D.container),e=D.h.windowScroll().left,f=D.h.windowScroll().top,g=D.h.windowWidth(),h=D.h.windowHeight(),i=[[d.left,d.top],[d.left+D.width,d.top],[d.left,d.top+D.height],[d.left+D.width,d.top+D.height]],j=0;j<i.length;j++){var k=i[j];k[0]>=e&&k[0]<=e+g&&k[1]>=f&&k[1]<=f+h&&(c=!0)}if(!c)return}N?((37===b||39===b)&&(a.preventDefault?a.preventDefault():a.returnValue=!1),39===b&&D.swipeNext(),37===b&&D.swipePrev()):((38===b||40===b)&&(a.preventDefault?a.preventDefault():a.returnValue=!1),40===b&&D.swipeNext(),38===b&&D.swipePrev())}}function j(a){var c=D._wheelEvent,d=0;if(a.detail)d=-a.detail;else if("mousewheel"===c)if(b.mousewheelControlForceToAxis)if(N){if(!(Math.abs(a.wheelDeltaX)>Math.abs(a.wheelDeltaY)))return;d=a.wheelDeltaX}else{if(!(Math.abs(a.wheelDeltaY)>Math.abs(a.wheelDeltaX)))return;d=a.wheelDeltaY}else d=a.wheelDelta;else if("DOMMouseScroll"===c)d=-a.detail;else if("wheel"===c)if(b.mousewheelControlForceToAxis)if(N){if(!(Math.abs(a.deltaX)>Math.abs(a.deltaY)))return;d=-a.deltaX}else{if(!(Math.abs(a.deltaY)>Math.abs(a.deltaX)))return;d=-a.deltaY}else d=Math.abs(a.deltaX)>Math.abs(a.deltaY)?-a.deltaX:-a.deltaY;if(b.freeMode){var f=D.getWrapperTranslate()+d;if(f>0&&(f=0),f<-e()&&(f=-e()),D.setWrapperTransition(0),D.setWrapperTranslate(f),D.updateActiveSlide(f),0===f||f===-e())return}else(new Date).getTime()-V>60&&(0>d?D.swipeNext():D.swipePrev()),V=(new Date).getTime();return b.autoplay&&D.stopAutoplay(!0),a.preventDefault?a.preventDefault():a.returnValue=!1,!1}function k(a){D.allowSlideClick&&(m(a),D.fireCallback(b.onSlideClick,D,a))}function l(a){m(a),D.fireCallback(b.onSlideTouch,D,a)}function m(a){if(a.currentTarget)D.clickedSlide=a.currentTarget;else{var c=a.srcElement;do{if(c.className.indexOf(b.slideClass)>-1)break;c=c.parentNode}while(c);D.clickedSlide=c}D.clickedSlideIndex=D.slides.indexOf(D.clickedSlide),D.clickedSlideLoopIndex=D.clickedSlideIndex-(D.loopedSlides||0)}function n(a){return D.allowLinks?void 0:(a.preventDefault?a.preventDefault():a.returnValue=!1,b.preventLinksPropagation&&"stopPropagation"in a&&a.stopPropagation(),!1)}function o(a){return a.stopPropagation?a.stopPropagation():a.returnValue=!1,!1}function p(a){if(b.preventLinks&&(D.allowLinks=!0),D.isTouched||b.onlyExternal)return!1;var c=a.target||a.srcElement;document.activeElement&&document.activeElement!==document.body&&document.activeElement!==c&&document.activeElement.blur();var d="input select textarea".split(" ");if(b.noSwiping&&c&&t(c))return!1;if(_=!1,D.isTouched=!0,$="touchstart"===a.type,!$&&"which"in a&&3===a.which)return D.isTouched=!1,!1;if(!$||1===a.targetTouches.length){D.callPlugins("onTouchStartBegin"),!$&&!D.isAndroid&&d.indexOf(c.tagName.toLowerCase())<0&&(a.preventDefault?a.preventDefault():a.returnValue=!1);var e=$?a.targetTouches[0].pageX:a.pageX||a.clientX,f=$?a.targetTouches[0].pageY:a.pageY||a.clientY;D.touches.startX=D.touches.currentX=e,D.touches.startY=D.touches.currentY=f,D.touches.start=D.touches.current=N?e:f,D.setWrapperTransition(0),D.positions.start=D.positions.current=D.getWrapperTranslate(),D.setWrapperTranslate(D.positions.start),D.times.start=(new Date).getTime(),I=void 0,b.moveStartThreshold>0&&(X=!1),b.onTouchStart&&D.fireCallback(b.onTouchStart,D,a),D.callPlugins("onTouchStartEnd")}}function q(a){if(D.isTouched&&!b.onlyExternal&&(!$||"mousemove"!==a.type)){var c=$?a.targetTouches[0].pageX:a.pageX||a.clientX,d=$?a.targetTouches[0].pageY:a.pageY||a.clientY;if("undefined"==typeof I&&N&&(I=!!(I||Math.abs(d-D.touches.startY)>Math.abs(c-D.touches.startX))),"undefined"!=typeof I||N||(I=!!(I||Math.abs(d-D.touches.startY)<Math.abs(c-D.touches.startX))),I)return void(D.isTouched=!1);if(N){if(!b.swipeToNext&&c<D.touches.startX||!b.swipeToPrev&&c>D.touches.startX)return}else if(!b.swipeToNext&&d<D.touches.startY||!b.swipeToPrev&&d>D.touches.startY)return;if(a.assignedToSwiper)return void(D.isTouched=!1);if(a.assignedToSwiper=!0,b.preventLinks&&(D.allowLinks=!1),b.onSlideClick&&(D.allowSlideClick=!1),b.autoplay&&D.stopAutoplay(!0),!$||1===a.touches.length){if(D.isMoved||(D.callPlugins("onTouchMoveStart"),b.loop&&(D.fixLoop(),D.positions.start=D.getWrapperTranslate()),b.onTouchMoveStart&&D.fireCallback(b.onTouchMoveStart,D)),D.isMoved=!0,a.preventDefault?a.preventDefault():a.returnValue=!1,D.touches.current=N?c:d,D.positions.current=(D.touches.current-D.touches.start)*b.touchRatio+D.positions.start,D.positions.current>0&&b.onResistanceBefore&&D.fireCallback(b.onResistanceBefore,D,D.positions.current),D.positions.current<-e()&&b.onResistanceAfter&&D.fireCallback(b.onResistanceAfter,D,Math.abs(D.positions.current+e())),b.resistance&&"100%"!==b.resistance){var f;if(D.positions.current>0&&(f=1-D.positions.current/J/2,D.positions.current=.5>f?J/2:D.positions.current*f),D.positions.current<-e()){var g=(D.touches.current-D.touches.start)*b.touchRatio+(e()+D.positions.start);f=(J+g)/J;var h=D.positions.current-g*(1-f)/2,i=-e()-J/2;D.positions.current=i>h||0>=f?i:h}}if(b.resistance&&"100%"===b.resistance&&(D.positions.current>0&&(!b.freeMode||b.freeModeFluid)&&(D.positions.current=0),D.positions.current<-e()&&(!b.freeMode||b.freeModeFluid)&&(D.positions.current=-e())),!b.followFinger)return;if(b.moveStartThreshold)if(Math.abs(D.touches.current-D.touches.start)>b.moveStartThreshold||X){if(!X)return X=!0,void(D.touches.start=D.touches.current);D.setWrapperTranslate(D.positions.current)}else D.positions.current=D.positions.start;else D.setWrapperTranslate(D.positions.current);return(b.freeMode||b.watchActiveIndex)&&D.updateActiveSlide(D.positions.current),b.grabCursor&&(D.container.style.cursor="move",D.container.style.cursor="grabbing",D.container.style.cursor="-moz-grabbin",D.container.style.cursor="-webkit-grabbing"),Y||(Y=D.touches.current),Z||(Z=(new Date).getTime()),D.velocity=(D.touches.current-Y)/((new Date).getTime()-Z)/2,Math.abs(D.touches.current-Y)<2&&(D.velocity=0),Y=D.touches.current,Z=(new Date).getTime(),D.callPlugins("onTouchMoveEnd"),b.onTouchMove&&D.fireCallback(b.onTouchMove,D,a),!1}}}function r(a){if(I&&D.swipeReset(),!b.onlyExternal&&D.isTouched){D.isTouched=!1,b.grabCursor&&(D.container.style.cursor="move",D.container.style.cursor="grab",D.container.style.cursor="-moz-grab",D.container.style.cursor="-webkit-grab"),D.positions.current||0===D.positions.current||(D.positions.current=D.positions.start),b.followFinger&&D.setWrapperTranslate(D.positions.current),D.times.end=(new Date).getTime(),D.touches.diff=D.touches.current-D.touches.start,D.touches.abs=Math.abs(D.touches.diff),D.positions.diff=D.positions.current-D.positions.start,D.positions.abs=Math.abs(D.positions.diff);var c=D.positions.diff,d=D.positions.abs,f=D.times.end-D.times.start;5>d&&300>f&&D.allowLinks===!1&&(b.freeMode||0===d||D.swipeReset(),b.preventLinks&&(D.allowLinks=!0),b.onSlideClick&&(D.allowSlideClick=!0)),setTimeout(function(){"undefined"!=typeof D&&null!==D&&(b.preventLinks&&(D.allowLinks=!0),b.onSlideClick&&(D.allowSlideClick=!0))},100);var g=e();if(!D.isMoved&&b.freeMode)return D.isMoved=!1,b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd");if(!D.isMoved||D.positions.current>0||D.positions.current<-g)return D.swipeReset(),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd");if(D.isMoved=!1,b.freeMode){if(b.freeModeFluid){var h,i=1e3*b.momentumRatio,j=D.velocity*i,k=D.positions.current+j,l=!1,m=20*Math.abs(D.velocity)*b.momentumBounceRatio;-g>k&&(b.momentumBounce&&D.support.transitions?(-m>k+g&&(k=-g-m),h=-g,l=!0,_=!0):k=-g),k>0&&(b.momentumBounce&&D.support.transitions?(k>m&&(k=m),h=0,l=!0,_=!0):k=0),0!==D.velocity&&(i=Math.abs((k-D.positions.current)/D.velocity)),D.setWrapperTranslate(k),D.setWrapperTransition(i),b.momentumBounce&&l&&D.wrapperTransitionEnd(function(){_&&(b.onMomentumBounce&&D.fireCallback(b.onMomentumBounce,D),D.callPlugins("onMomentumBounce"),D.setWrapperTranslate(h),D.setWrapperTransition(300))}),D.updateActiveSlide(k)}return(!b.freeModeFluid||f>=300)&&D.updateActiveSlide(D.positions.current),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),void D.callPlugins("onTouchEnd")}H=0>c?"toNext":"toPrev","toNext"===H&&300>=f&&(30>d||!b.shortSwipes?D.swipeReset():D.swipeNext(!0,!0)),"toPrev"===H&&300>=f&&(30>d||!b.shortSwipes?D.swipeReset():D.swipePrev(!0,!0));var n=0;if("auto"===b.slidesPerView){for(var o,p=Math.abs(D.getWrapperTranslate()),q=0,r=0;r<D.slides.length;r++)if(o=N?D.slides[r].getWidth(!0,b.roundLengths):D.slides[r].getHeight(!0,b.roundLengths),q+=o,q>p){n=o;break}n>J&&(n=J)}else n=F*b.slidesPerView;"toNext"===H&&f>300&&(d>=n*b.longSwipesRatio?D.swipeNext(!0,!0):D.swipeReset()),"toPrev"===H&&f>300&&(d>=n*b.longSwipesRatio?D.swipePrev(!0,!0):D.swipeReset()),b.onTouchEnd&&D.fireCallback(b.onTouchEnd,D,a),D.callPlugins("onTouchEnd")}}function s(a,b){return a&&a.getAttribute("class")&&a.getAttribute("class").indexOf(b)>-1}function t(a){var c=!1;do s(a,b.noSwipingClass)&&(c=!0),a=a.parentElement;while(!c&&a.parentElement&&!s(a,b.wrapperClass));return!c&&s(a,b.wrapperClass)&&s(a,b.noSwipingClass)&&(c=!0),c}function u(a,b){var c,d=document.createElement("div");return d.innerHTML=b,c=d.firstChild,c.className+=" "+a,c.outerHTML}function v(a,c,d){function e(){var f=+new Date,l=f-g;h+=i*l/(1e3/60),k="toNext"===j?h>a:a>h,k?(D.setWrapperTranslate(Math.ceil(h)),D._DOMAnimating=!0,window.setTimeout(function(){e()},1e3/60)):(b.onSlideChangeEnd&&("to"===c?d.runCallbacks===!0&&D.fireCallback(b.onSlideChangeEnd,D,j):D.fireCallback(b.onSlideChangeEnd,D,j)),D.setWrapperTranslate(a),D._DOMAnimating=!1)}var f="to"===c&&d.speed>=0?d.speed:b.speed,g=+new Date;if(D.support.transitions||!b.DOMAnimation)D.setWrapperTranslate(a),D.setWrapperTransition(f);else{var h=D.getWrapperTranslate(),i=Math.ceil((a-h)/f*(1e3/60)),j=h>a?"toNext":"toPrev",k="toNext"===j?h>a:a>h;if(D._DOMAnimating)return;e()}D.updateActiveSlide(a),b.onSlideNext&&"next"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlideNext,D,a),b.onSlidePrev&&"prev"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlidePrev,D,a),b.onSlideReset&&"reset"===c&&d.runCallbacks===!0&&D.fireCallback(b.onSlideReset,D,a),"next"!==c&&"prev"!==c&&"to"!==c||d.runCallbacks!==!0||w(c)}function w(a){if(D.callPlugins("onSlideChangeStart"),b.onSlideChangeStart)if(b.queueStartCallbacks&&D.support.transitions){if(D._queueStartCallbacks)return;D._queueStartCallbacks=!0,D.fireCallback(b.onSlideChangeStart,D,a),D.wrapperTransitionEnd(function(){D._queueStartCallbacks=!1})}else D.fireCallback(b.onSlideChangeStart,D,a);if(b.onSlideChangeEnd)if(D.support.transitions)if(b.queueEndCallbacks){if(D._queueEndCallbacks)return;D._queueEndCallbacks=!0,D.wrapperTransitionEnd(function(c){D.fireCallback(b.onSlideChangeEnd,c,a)})}else D.wrapperTransitionEnd(function(c){D.fireCallback(b.onSlideChangeEnd,c,a)});else b.DOMAnimation||setTimeout(function(){D.fireCallback(b.onSlideChangeEnd,D,a)},10)}function x(){var a=D.paginationButtons;if(a)for(var b=0;b<a.length;b++)D.h.removeEventListener(a[b],"click",z)}function y(){var a=D.paginationButtons;if(a)for(var b=0;b<a.length;b++)D.h.addEventListener(a[b],"click",z)}function z(a){for(var c,d=a.target||a.srcElement,e=D.paginationButtons,f=0;f<e.length;f++)d===e[f]&&(c=f);b.autoplay&&D.stopAutoplay(!0),D.swipeTo(c)}function A(){ab=setTimeout(function(){b.loop?(D.fixLoop(),D.swipeNext(!0,!0)):D.swipeNext(!0,!0)||(b.autoplayStopOnLast?(clearTimeout(ab),ab=void 0):D.swipeTo(0)),D.wrapperTransitionEnd(function(){"undefined"!=typeof ab&&A()})},b.autoplay)}function B(){D.calcSlides(),b.loader.slides.length>0&&0===D.slides.length&&D.loadSlides(),b.loop&&D.createLoop(),D.init(),f(),b.pagination&&D.createPagination(!0),b.loop||b.initialSlide>0?D.swipeTo(b.initialSlide,0,!1):D.updateActiveSlide(0),b.autoplay&&D.startAutoplay(),D.centerIndex=D.activeIndex,b.onSwiperCreated&&D.fireCallback(b.onSwiperCreated,D),D.callPlugins("onSwiperCreated")}if(!document.body.outerHTML&&document.body.__defineGetter__&&HTMLElement){var C=HTMLElement.prototype;C.__defineGetter__&&C.__defineGetter__("outerHTML",function(){return(new XMLSerializer).serializeToString(this)})}if(window.getComputedStyle||(window.getComputedStyle=function(a){return this.el=a,this.getPropertyValue=function(b){var c=/(\-([a-z]){1})/g;return"float"===b&&(b="styleFloat"),c.test(b)&&(b=b.replace(c,function(){return arguments[2].toUpperCase()})),a.currentStyle[b]?a.currentStyle[b]:null},this}),Array.prototype.indexOf||(Array.prototype.indexOf=function(a,b){for(var c=b||0,d=this.length;d>c;c++)if(this[c]===a)return c;return-1}),(document.querySelectorAll||window.jQuery)&&"undefined"!=typeof a&&(a.nodeType||0!==c(a).length)){var D=this;D.touches={start:0,startX:0,startY:0,current:0,currentX:0,currentY:0,diff:0,abs:0},D.positions={start:0,abs:0,diff:0,current:0},D.times={start:0,end:0},D.id=(new Date).getTime(),D.container=a.nodeType?a:c(a)[0],D.isTouched=!1,D.isMoved=!1,D.activeIndex=0,D.centerIndex=0,D.activeLoaderIndex=0,D.activeLoopIndex=0,D.previousIndex=null,D.velocity=0,D.snapGrid=[],D.slidesGrid=[],D.imagesToLoad=[],D.imagesLoaded=0,D.wrapperLeft=0,D.wrapperRight=0,D.wrapperTop=0,D.wrapperBottom=0,D.isAndroid=navigator.userAgent.toLowerCase().indexOf("android")>=0;var E,F,G,H,I,J,K={eventTarget:"wrapper",mode:"horizontal",touchRatio:1,speed:300,freeMode:!1,freeModeFluid:!1,momentumRatio:1,momentumBounce:!0,momentumBounceRatio:1,slidesPerView:1,slidesPerGroup:1,slidesPerViewFit:!0,simulateTouch:!0,followFinger:!0,shortSwipes:!0,longSwipesRatio:.5,moveStartThreshold:!1,onlyExternal:!1,createPagination:!0,pagination:!1,paginationElement:"span",paginationClickable:!1,paginationAsRange:!0,resistance:!0,scrollContainer:!1,preventLinks:!0,preventLinksPropagation:!1,noSwiping:!1,noSwipingClass:"swiper-no-swiping",initialSlide:0,keyboardControl:!1,mousewheelControl:!1,mousewheelControlForceToAxis:!1,useCSS3Transforms:!0,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,loop:!1,loopAdditionalSlides:0,roundLengths:!1,calculateHeight:!1,cssWidthAndHeight:!1,updateOnImagesReady:!0,releaseFormElements:!0,watchActiveIndex:!1,visibilityFullFit:!1,offsetPxBefore:0,offsetPxAfter:0,offsetSlidesBefore:0,offsetSlidesAfter:0,centeredSlides:!1,queueStartCallbacks:!1,queueEndCallbacks:!1,autoResize:!0,resizeReInit:!1,DOMAnimation:!0,loader:{slides:[],slidesHTMLType:"inner",surroundGroups:1,logic:"reload",loadAllSlides:!1},swipeToPrev:!0,swipeToNext:!0,slideElement:"div",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",wrapperClass:"swiper-wrapper",paginationElementClass:"swiper-pagination-switch",paginationActiveClass:"swiper-active-switch",paginationVisibleClass:"swiper-visible-switch"};b=b||{};for(var L in K)if(L in b&&"object"==typeof b[L])for(var M in K[L])M in b[L]||(b[L][M]=K[L][M]);else L in b||(b[L]=K[L]);D.params=b,b.scrollContainer&&(b.freeMode=!0,b.freeModeFluid=!0),b.loop&&(b.resistance="100%");var N="horizontal"===b.mode,O=["mousedown","mousemove","mouseup"];D.browser.ie10&&(O=["MSPointerDown","MSPointerMove","MSPointerUp"]),D.browser.ie11&&(O=["pointerdown","pointermove","pointerup"]),D.touchEvents={touchStart:D.support.touch||!b.simulateTouch?"touchstart":O[0],touchMove:D.support.touch||!b.simulateTouch?"touchmove":O[1],touchEnd:D.support.touch||!b.simulateTouch?"touchend":O[2]};for(var P=D.container.childNodes.length-1;P>=0;P--)if(D.container.childNodes[P].className)for(var Q=D.container.childNodes[P].className.split(/\s+/),R=0;R<Q.length;R++)Q[R]===b.wrapperClass&&(E=D.container.childNodes[P]);D.wrapper=E,D._extendSwiperSlide=function(a){return a.append=function(){return b.loop?a.insertAfter(D.slides.length-D.loopedSlides):(D.wrapper.appendChild(a),D.reInit()),a},a.prepend=function(){return b.loop?(D.wrapper.insertBefore(a,D.slides[D.loopedSlides]),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):D.wrapper.insertBefore(a,D.wrapper.firstChild),D.reInit(),a},a.insertAfter=function(c){if("undefined"==typeof c)return!1;var d;return b.loop?(d=D.slides[c+1+D.loopedSlides],d?D.wrapper.insertBefore(a,d):D.wrapper.appendChild(a),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):(d=D.slides[c+1],D.wrapper.insertBefore(a,d)),D.reInit(),a},a.clone=function(){return D._extendSwiperSlide(a.cloneNode(!0))},a.remove=function(){D.wrapper.removeChild(a),D.reInit()},a.html=function(b){return"undefined"==typeof b?a.innerHTML:(a.innerHTML=b,a)},a.index=function(){for(var b,c=D.slides.length-1;c>=0;c--)a===D.slides[c]&&(b=c);return b},a.isActive=function(){return a.index()===D.activeIndex?!0:!1},a.swiperSlideDataStorage||(a.swiperSlideDataStorage={}),a.getData=function(b){return a.swiperSlideDataStorage[b]},a.setData=function(b,c){return a.swiperSlideDataStorage[b]=c,a},a.data=function(b,c){return"undefined"==typeof c?a.getAttribute("data-"+b):(a.setAttribute("data-"+b,c),a)},a.getWidth=function(b,c){return D.h.getWidth(a,b,c)},a.getHeight=function(b,c){return D.h.getHeight(a,b,c)},a.getOffset=function(){return D.h.getOffset(a)},a},D.calcSlides=function(a){var c=D.slides?D.slides.length:!1;D.slides=[],D.displaySlides=[];for(var d=0;d<D.wrapper.childNodes.length;d++)if(D.wrapper.childNodes[d].className)for(var e=D.wrapper.childNodes[d].className,f=e.split(/\s+/),i=0;i<f.length;i++)f[i]===b.slideClass&&D.slides.push(D.wrapper.childNodes[d]);for(d=D.slides.length-1;d>=0;d--)D._extendSwiperSlide(D.slides[d]);c!==!1&&(c!==D.slides.length||a)&&(h(),g(),D.updateActiveSlide(),D.params.pagination&&D.createPagination(),D.callPlugins("numberOfSlidesChanged"))},D.createSlide=function(a,c,d){c=c||D.params.slideClass,d=d||b.slideElement;var e=document.createElement(d);return e.innerHTML=a||"",e.className=c,D._extendSwiperSlide(e)},D.appendSlide=function(a,b,c){return a?a.nodeType?D._extendSwiperSlide(a).append():D.createSlide(a,b,c).append():void 0},D.prependSlide=function(a,b,c){return a?a.nodeType?D._extendSwiperSlide(a).prepend():D.createSlide(a,b,c).prepend():void 0},D.insertSlideAfter=function(a,b,c,d){return"undefined"==typeof a?!1:b.nodeType?D._extendSwiperSlide(b).insertAfter(a):D.createSlide(b,c,d).insertAfter(a)},D.removeSlide=function(a){if(D.slides[a]){if(b.loop){if(!D.slides[a+D.loopedSlides])return!1;D.slides[a+D.loopedSlides].remove(),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()}else D.slides[a].remove();return!0}return!1},D.removeLastSlide=function(){return D.slides.length>0?(b.loop?(D.slides[D.slides.length-1-D.loopedSlides].remove(),D.removeLoopedSlides(),D.calcSlides(),D.createLoop()):D.slides[D.slides.length-1].remove(),!0):!1},D.removeAllSlides=function(){for(var a=D.slides.length,b=D.slides.length-1;b>=0;b--)D.slides[b].remove(),b===a-1&&D.setWrapperTranslate(0)},D.getSlide=function(a){return D.slides[a]},D.getLastSlide=function(){return D.slides[D.slides.length-1]},D.getFirstSlide=function(){return D.slides[0]},D.activeSlide=function(){return D.slides[D.activeIndex]},D.fireCallback=function(){var a=arguments[0];if("[object Array]"===Object.prototype.toString.call(a))for(var c=0;c<a.length;c++)"function"==typeof a[c]&&a[c](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);else"[object String]"===Object.prototype.toString.call(a)?b["on"+a]&&D.fireCallback(b["on"+a],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]):a(arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},D.addCallback=function(a,b){var c,e=this;return e.params["on"+a]?d(this.params["on"+a])?this.params["on"+a].push(b):"function"==typeof this.params["on"+a]?(c=this.params["on"+a],this.params["on"+a]=[],this.params["on"+a].push(c),this.params["on"+a].push(b)):void 0:(this.params["on"+a]=[],this.params["on"+a].push(b))},D.removeCallbacks=function(a){D.params["on"+a]&&(D.params["on"+a]=null)};var S=[];for(var T in D.plugins)if(b[T]){var U=D.plugins[T](D,b[T]);U&&S.push(U)}D.callPlugins=function(a,b){b||(b={});for(var c=0;c<S.length;c++)a in S[c]&&S[c][a](b)},!D.browser.ie10&&!D.browser.ie11||b.onlyExternal||D.wrapper.classList.add("swiper-wp8-"+(N?"horizontal":"vertical")),b.freeMode&&(D.container.className+=" swiper-free-mode"),D.initialized=!1,D.init=function(a,c){var d=D.h.getWidth(D.container,!1,b.roundLengths),e=D.h.getHeight(D.container,!1,b.roundLengths);if(d!==D.width||e!==D.height||a){D.width=d,D.height=e;var f,g,h,i,j,k,l;J=N?d:e;var m=D.wrapper;if(a&&D.calcSlides(c),"auto"===b.slidesPerView){var n=0,o=0;b.slidesOffset>0&&(m.style.paddingLeft="",m.style.paddingRight="",m.style.paddingTop="",m.style.paddingBottom=""),m.style.width="",m.style.height="",b.offsetPxBefore>0&&(N?D.wrapperLeft=b.offsetPxBefore:D.wrapperTop=b.offsetPxBefore),b.offsetPxAfter>0&&(N?D.wrapperRight=b.offsetPxAfter:D.wrapperBottom=b.offsetPxAfter),b.centeredSlides&&(N?(D.wrapperLeft=(J-this.slides[0].getWidth(!0,b.roundLengths))/2,D.wrapperRight=(J-D.slides[D.slides.length-1].getWidth(!0,b.roundLengths))/2):(D.wrapperTop=(J-D.slides[0].getHeight(!0,b.roundLengths))/2,D.wrapperBottom=(J-D.slides[D.slides.length-1].getHeight(!0,b.roundLengths))/2)),N?(D.wrapperLeft>=0&&(m.style.paddingLeft=D.wrapperLeft+"px"),D.wrapperRight>=0&&(m.style.paddingRight=D.wrapperRight+"px")):(D.wrapperTop>=0&&(m.style.paddingTop=D.wrapperTop+"px"),D.wrapperBottom>=0&&(m.style.paddingBottom=D.wrapperBottom+"px")),k=0;var p=0;for(D.snapGrid=[],D.slidesGrid=[],h=0,l=0;l<D.slides.length;l++){f=D.slides[l].getWidth(!0,b.roundLengths),g=D.slides[l].getHeight(!0,b.roundLengths),b.calculateHeight&&(h=Math.max(h,g));var q=N?f:g;if(b.centeredSlides){var r=l===D.slides.length-1?0:D.slides[l+1].getWidth(!0,b.roundLengths),s=l===D.slides.length-1?0:D.slides[l+1].getHeight(!0,b.roundLengths),t=N?r:s;if(q>J){if(b.slidesPerViewFit)D.snapGrid.push(k+D.wrapperLeft),D.snapGrid.push(k+q-J+D.wrapperLeft);else for(var u=0;u<=Math.floor(q/(J+D.wrapperLeft));u++)D.snapGrid.push(0===u?k+D.wrapperLeft:k+D.wrapperLeft+J*u);D.slidesGrid.push(k+D.wrapperLeft)}else D.snapGrid.push(p),D.slidesGrid.push(p);p+=q/2+t/2}else{if(q>J)if(b.slidesPerViewFit)D.snapGrid.push(k),D.snapGrid.push(k+q-J);else if(0!==J)for(var v=0;v<=Math.floor(q/J);v++)D.snapGrid.push(k+J*v);else D.snapGrid.push(k);else D.snapGrid.push(k);D.slidesGrid.push(k)}k+=q,n+=f,o+=g}b.calculateHeight&&(D.height=h),N?(G=n+D.wrapperRight+D.wrapperLeft,b.cssWidthAndHeight&&"height"!==b.cssWidthAndHeight||(m.style.width=n+"px"),b.cssWidthAndHeight&&"width"!==b.cssWidthAndHeight||(m.style.height=D.height+"px")):(b.cssWidthAndHeight&&"height"!==b.cssWidthAndHeight||(m.style.width=D.width+"px"),b.cssWidthAndHeight&&"width"!==b.cssWidthAndHeight||(m.style.height=o+"px"),G=o+D.wrapperTop+D.wrapperBottom)}else if(b.scrollContainer)m.style.width="",m.style.height="",i=D.slides[0].getWidth(!0,b.roundLengths),j=D.slides[0].getHeight(!0,b.roundLengths),G=N?i:j,m.style.width=i+"px",m.style.height=j+"px",F=N?i:j;else{if(b.calculateHeight){for(h=0,j=0,N||(D.container.style.height=""),m.style.height="",l=0;l<D.slides.length;l++)D.slides[l].style.height="",h=Math.max(D.slides[l].getHeight(!0),h),N||(j+=D.slides[l].getHeight(!0));g=h,D.height=g,N?j=g:(J=g,D.container.style.height=J+"px")}else g=N?D.height:D.height/b.slidesPerView,b.roundLengths&&(g=Math.ceil(g)),j=N?D.height:D.slides.length*g;for(f=N?D.width/b.slidesPerView:D.width,b.roundLengths&&(f=Math.ceil(f)),i=N?D.slides.length*f:D.width,F=N?f:g,b.offsetSlidesBefore>0&&(N?D.wrapperLeft=F*b.offsetSlidesBefore:D.wrapperTop=F*b.offsetSlidesBefore),b.offsetSlidesAfter>0&&(N?D.wrapperRight=F*b.offsetSlidesAfter:D.wrapperBottom=F*b.offsetSlidesAfter),b.offsetPxBefore>0&&(N?D.wrapperLeft=b.offsetPxBefore:D.wrapperTop=b.offsetPxBefore),b.offsetPxAfter>0&&(N?D.wrapperRight=b.offsetPxAfter:D.wrapperBottom=b.offsetPxAfter),b.centeredSlides&&(N?(D.wrapperLeft=(J-F)/2,D.wrapperRight=(J-F)/2):(D.wrapperTop=(J-F)/2,D.wrapperBottom=(J-F)/2)),N?(D.wrapperLeft>0&&(m.style.paddingLeft=D.wrapperLeft+"px"),D.wrapperRight>0&&(m.style.paddingRight=D.wrapperRight+"px")):(D.wrapperTop>0&&(m.style.paddingTop=D.wrapperTop+"px"),D.wrapperBottom>0&&(m.style.paddingBottom=D.wrapperBottom+"px")),G=N?i+D.wrapperRight+D.wrapperLeft:j+D.wrapperTop+D.wrapperBottom,parseFloat(i)>0&&(!b.cssWidthAndHeight||"height"===b.cssWidthAndHeight)&&(m.style.width=i+"px"),parseFloat(j)>0&&(!b.cssWidthAndHeight||"width"===b.cssWidthAndHeight)&&(m.style.height=j+"px"),k=0,D.snapGrid=[],D.slidesGrid=[],l=0;l<D.slides.length;l++)D.snapGrid.push(k),D.slidesGrid.push(k),k+=F,parseFloat(f)>0&&(!b.cssWidthAndHeight||"height"===b.cssWidthAndHeight)&&(D.slides[l].style.width=f+"px"),parseFloat(g)>0&&(!b.cssWidthAndHeight||"width"===b.cssWidthAndHeight)&&(D.slides[l].style.height=g+"px")}D.initialized?(D.callPlugins("onInit"),b.onInit&&D.fireCallback(b.onInit,D)):(D.callPlugins("onFirstInit"),b.onFirstInit&&D.fireCallback(b.onFirstInit,D)),D.initialized=!0}},D.reInit=function(a){D.init(!0,a)},D.resizeFix=function(a){D.callPlugins("beforeResizeFix"),D.init(b.resizeReInit||a),b.freeMode?D.getWrapperTranslate()<-e()&&(D.setWrapperTransition(0),D.setWrapperTranslate(-e())):(D.swipeTo(b.loop?D.activeLoopIndex:D.activeIndex,0,!1),b.autoplay&&(D.support.transitions&&"undefined"!=typeof ab?"undefined"!=typeof ab&&(clearTimeout(ab),ab=void 0,D.startAutoplay()):"undefined"!=typeof bb&&(clearInterval(bb),bb=void 0,D.startAutoplay()))),D.callPlugins("afterResizeFix")},D.destroy=function(a){var c=D.h.removeEventListener,d="wrapper"===b.eventTarget?D.wrapper:D.container;if(D.browser.ie10||D.browser.ie11?(c(d,D.touchEvents.touchStart,p),c(document,D.touchEvents.touchMove,q),c(document,D.touchEvents.touchEnd,r)):(D.support.touch&&(c(d,"touchstart",p),c(d,"touchmove",q),c(d,"touchend",r)),b.simulateTouch&&(c(d,"mousedown",p),c(document,"mousemove",q),c(document,"mouseup",r))),b.autoResize&&c(window,"resize",D.resizeFix),h(),b.paginationClickable&&x(),b.mousewheelControl&&D._wheelEvent&&c(D.container,D._wheelEvent,j),b.keyboardControl&&c(document,"keydown",i),b.autoplay&&D.stopAutoplay(),a){D.wrapper.removeAttribute("style");for(var e=0;e<D.slides.length;e++)D.slides[e].removeAttribute("style")}D.callPlugins("onDestroy"),window.jQuery&&window.jQuery(D.container).data("swiper")&&window.jQuery(D.container).removeData("swiper"),window.Zepto&&window.Zepto(D.container).data("swiper")&&window.Zepto(D.container).removeData("swiper"),D=null},D.disableKeyboardControl=function(){b.keyboardControl=!1,D.h.removeEventListener(document,"keydown",i)},D.enableKeyboardControl=function(){b.keyboardControl=!0,D.h.addEventListener(document,"keydown",i)};var V=(new Date).getTime();if(D.disableMousewheelControl=function(){return D._wheelEvent?(b.mousewheelControl=!1,D.h.removeEventListener(D.container,D._wheelEvent,j),!0):!1},D.enableMousewheelControl=function(){return D._wheelEvent?(b.mousewheelControl=!0,D.h.addEventListener(D.container,D._wheelEvent,j),!0):!1},b.grabCursor){var W=D.container.style;W.cursor="move",W.cursor="grab",W.cursor="-moz-grab",W.cursor="-webkit-grab"}D.allowSlideClick=!0,D.allowLinks=!0;var X,Y,Z,$=!1,_=!0;D.swipeNext=function(a,c){"undefined"==typeof a&&(a=!0),!c&&b.loop&&D.fixLoop(),!c&&b.autoplay&&D.stopAutoplay(!0),D.callPlugins("onSwipeNext");var d=D.getWrapperTranslate().toFixed(2),f=d;if("auto"===b.slidesPerView){for(var g=0;g<D.snapGrid.length;g++)if(-d>=D.snapGrid[g].toFixed(2)&&-d<D.snapGrid[g+1].toFixed(2)){f=-D.snapGrid[g+1];break}}else{var h=F*b.slidesPerGroup;f=-(Math.floor(Math.abs(d)/Math.floor(h))*h+h)}return f<-e()&&(f=-e()),f===d?!1:(v(f,"next",{runCallbacks:a}),!0)},D.swipePrev=function(a,c){"undefined"==typeof a&&(a=!0),!c&&b.loop&&D.fixLoop(),!c&&b.autoplay&&D.stopAutoplay(!0),D.callPlugins("onSwipePrev");var d,e=Math.ceil(D.getWrapperTranslate());if("auto"===b.slidesPerView){d=0;for(var f=1;f<D.snapGrid.length;f++){if(-e===D.snapGrid[f]){d=-D.snapGrid[f-1];break}if(-e>D.snapGrid[f]&&-e<D.snapGrid[f+1]){d=-D.snapGrid[f];break}}}else{var g=F*b.slidesPerGroup;d=-(Math.ceil(-e/g)-1)*g}return d>0&&(d=0),d===e?!1:(v(d,"prev",{runCallbacks:a}),!0)},D.swipeReset=function(a){"undefined"==typeof a&&(a=!0),D.callPlugins("onSwipeReset");{var c,d=D.getWrapperTranslate(),f=F*b.slidesPerGroup;-e()}if("auto"===b.slidesPerView){c=0;for(var g=0;g<D.snapGrid.length;g++){if(-d===D.snapGrid[g])return;if(-d>=D.snapGrid[g]&&-d<D.snapGrid[g+1]){c=D.positions.diff>0?-D.snapGrid[g+1]:-D.snapGrid[g];break}}-d>=D.snapGrid[D.snapGrid.length-1]&&(c=-D.snapGrid[D.snapGrid.length-1]),d<=-e()&&(c=-e())}else c=0>d?Math.round(d/f)*f:0,d<=-e()&&(c=-e());return b.scrollContainer&&(c=0>d?d:0),c<-e()&&(c=-e()),b.scrollContainer&&J>F&&(c=0),c===d?!1:(v(c,"reset",{runCallbacks:a}),!0)},D.swipeTo=function(a,c,d){a=parseInt(a,10),D.callPlugins("onSwipeTo",{index:a,speed:c}),b.loop&&(a+=D.loopedSlides);var f=D.getWrapperTranslate();if(!(!isFinite(a)||a>D.slides.length-1||0>a)){var g;return g="auto"===b.slidesPerView?-D.slidesGrid[a]:-a*F,g<-e()&&(g=-e()),g===f?!1:("undefined"==typeof d&&(d=!0),v(g,"to",{index:a,speed:c,runCallbacks:d}),!0)}},D._queueStartCallbacks=!1,D._queueEndCallbacks=!1,D.updateActiveSlide=function(a){if(D.initialized&&0!==D.slides.length){D.previousIndex=D.activeIndex,"undefined"==typeof a&&(a=D.getWrapperTranslate()),a>0&&(a=0);var c;if("auto"===b.slidesPerView){if(D.activeIndex=D.slidesGrid.indexOf(-a),D.activeIndex<0){for(c=0;c<D.slidesGrid.length-1&&!(-a>D.slidesGrid[c]&&-a<D.slidesGrid[c+1]);c++);var d=Math.abs(D.slidesGrid[c]+a),e=Math.abs(D.slidesGrid[c+1]+a);
    D.activeIndex=e>=d?c:c+1}}else D.activeIndex=Math[b.visibilityFullFit?"ceil":"round"](-a/F);if(D.activeIndex===D.slides.length&&(D.activeIndex=D.slides.length-1),D.activeIndex<0&&(D.activeIndex=0),D.slides[D.activeIndex]){if(D.calcVisibleSlides(a),D.support.classList){var f;for(c=0;c<D.slides.length;c++)f=D.slides[c],f.classList.remove(b.slideActiveClass),D.visibleSlides.indexOf(f)>=0?f.classList.add(b.slideVisibleClass):f.classList.remove(b.slideVisibleClass);D.slides[D.activeIndex].classList.add(b.slideActiveClass)}else{var g=new RegExp("\\s*"+b.slideActiveClass),h=new RegExp("\\s*"+b.slideVisibleClass);for(c=0;c<D.slides.length;c++)D.slides[c].className=D.slides[c].className.replace(g,"").replace(h,""),D.visibleSlides.indexOf(D.slides[c])>=0&&(D.slides[c].className+=" "+b.slideVisibleClass);D.slides[D.activeIndex].className+=" "+b.slideActiveClass}if(b.loop){var i=D.loopedSlides;D.activeLoopIndex=D.activeIndex-i,D.activeLoopIndex>=D.slides.length-2*i&&(D.activeLoopIndex=D.slides.length-2*i-D.activeLoopIndex),D.activeLoopIndex<0&&(D.activeLoopIndex=D.slides.length-2*i+D.activeLoopIndex),D.activeLoopIndex<0&&(D.activeLoopIndex=0)}else D.activeLoopIndex=D.activeIndex;b.pagination&&D.updatePagination(a)}}},D.createPagination=function(a){if(b.paginationClickable&&D.paginationButtons&&x(),D.paginationContainer=b.pagination.nodeType?b.pagination:c(b.pagination)[0],b.createPagination){var d="",e=D.slides.length,f=e;b.loop&&(f-=2*D.loopedSlides);for(var g=0;f>g;g++)d+="<"+b.paginationElement+' class="'+b.paginationElementClass+'"></'+b.paginationElement+">";D.paginationContainer.innerHTML=d}D.paginationButtons=c("."+b.paginationElementClass,D.paginationContainer),a||D.updatePagination(),D.callPlugins("onCreatePagination"),b.paginationClickable&&y()},D.updatePagination=function(a){if(b.pagination&&!(D.slides.length<1)){var d=c("."+b.paginationActiveClass,D.paginationContainer);if(d){var e=D.paginationButtons;if(0!==e.length){for(var f=0;f<e.length;f++)e[f].className=b.paginationElementClass;var g=b.loop?D.loopedSlides:0;if(b.paginationAsRange){D.visibleSlides||D.calcVisibleSlides(a);var h,i=[];for(h=0;h<D.visibleSlides.length;h++){var j=D.slides.indexOf(D.visibleSlides[h])-g;b.loop&&0>j&&(j=D.slides.length-2*D.loopedSlides+j),b.loop&&j>=D.slides.length-2*D.loopedSlides&&(j=D.slides.length-2*D.loopedSlides-j,j=Math.abs(j)),i.push(j)}for(h=0;h<i.length;h++)e[i[h]]&&(e[i[h]].className+=" "+b.paginationVisibleClass);b.loop?void 0!==e[D.activeLoopIndex]&&(e[D.activeLoopIndex].className+=" "+b.paginationActiveClass):e[D.activeIndex]&&(e[D.activeIndex].className+=" "+b.paginationActiveClass)}else b.loop?e[D.activeLoopIndex]&&(e[D.activeLoopIndex].className+=" "+b.paginationActiveClass+" "+b.paginationVisibleClass):e[D.activeIndex]&&(e[D.activeIndex].className+=" "+b.paginationActiveClass+" "+b.paginationVisibleClass)}}}},D.calcVisibleSlides=function(a){var c=[],d=0,e=0,f=0;N&&D.wrapperLeft>0&&(a+=D.wrapperLeft),!N&&D.wrapperTop>0&&(a+=D.wrapperTop);for(var g=0;g<D.slides.length;g++){d+=e,e="auto"===b.slidesPerView?N?D.h.getWidth(D.slides[g],!0,b.roundLengths):D.h.getHeight(D.slides[g],!0,b.roundLengths):F,f=d+e;var h=!1;b.visibilityFullFit?(d>=-a&&-a+J>=f&&(h=!0),-a>=d&&f>=-a+J&&(h=!0)):(f>-a&&-a+J>=f&&(h=!0),d>=-a&&-a+J>d&&(h=!0),-a>d&&f>-a+J&&(h=!0)),h&&c.push(D.slides[g])}0===c.length&&(c=[D.slides[D.activeIndex]]),D.visibleSlides=c};var ab,bb;D.startAutoplay=function(){if(D.support.transitions){if("undefined"!=typeof ab)return!1;if(!b.autoplay)return;D.callPlugins("onAutoplayStart"),b.onAutoplayStart&&D.fireCallback(b.onAutoplayStart,D),A()}else{if("undefined"!=typeof bb)return!1;if(!b.autoplay)return;D.callPlugins("onAutoplayStart"),b.onAutoplayStart&&D.fireCallback(b.onAutoplayStart,D),bb=setInterval(function(){b.loop?(D.fixLoop(),D.swipeNext(!0,!0)):D.swipeNext(!0,!0)||(b.autoplayStopOnLast?(clearInterval(bb),bb=void 0):D.swipeTo(0))},b.autoplay)}},D.stopAutoplay=function(a){if(D.support.transitions){if(!ab)return;ab&&clearTimeout(ab),ab=void 0,a&&!b.autoplayDisableOnInteraction&&D.wrapperTransitionEnd(function(){A()}),D.callPlugins("onAutoplayStop"),b.onAutoplayStop&&D.fireCallback(b.onAutoplayStop,D)}else bb&&clearInterval(bb),bb=void 0,D.callPlugins("onAutoplayStop"),b.onAutoplayStop&&D.fireCallback(b.onAutoplayStop,D)},D.loopCreated=!1,D.removeLoopedSlides=function(){if(D.loopCreated)for(var a=0;a<D.slides.length;a++)D.slides[a].getData("looped")===!0&&D.wrapper.removeChild(D.slides[a])},D.createLoop=function(){if(0!==D.slides.length){D.loopedSlides="auto"===b.slidesPerView?b.loopedSlides||1:Math.floor(b.slidesPerView)+b.loopAdditionalSlides,D.loopedSlides>D.slides.length&&(D.loopedSlides=D.slides.length);var a,c="",d="",e="",f=D.slides.length,g=Math.floor(D.loopedSlides/f),h=D.loopedSlides%f;for(a=0;g*f>a;a++){var i=a;if(a>=f){var j=Math.floor(a/f);i=a-f*j}e+=D.slides[i].outerHTML}for(a=0;h>a;a++)d+=u(b.slideDuplicateClass,D.slides[a].outerHTML);for(a=f-h;f>a;a++)c+=u(b.slideDuplicateClass,D.slides[a].outerHTML);var k=c+e+E.innerHTML+e+d;for(E.innerHTML=k,D.loopCreated=!0,D.calcSlides(),a=0;a<D.slides.length;a++)(a<D.loopedSlides||a>=D.slides.length-D.loopedSlides)&&D.slides[a].setData("looped",!0);D.callPlugins("onCreateLoop")}},D.fixLoop=function(){var a;D.activeIndex<D.loopedSlides?(a=D.slides.length-3*D.loopedSlides+D.activeIndex,D.swipeTo(a,0,!1)):("auto"===b.slidesPerView&&D.activeIndex>=2*D.loopedSlides||D.activeIndex>D.slides.length-2*b.slidesPerView)&&(a=-D.slides.length+D.activeIndex+D.loopedSlides,D.swipeTo(a,0,!1))},D.loadSlides=function(){var a="";D.activeLoaderIndex=0;for(var c=b.loader.slides,d=b.loader.loadAllSlides?c.length:b.slidesPerView*(1+b.loader.surroundGroups),e=0;d>e;e++)a+="outer"===b.loader.slidesHTMLType?c[e]:"<"+b.slideElement+' class="'+b.slideClass+'" data-swiperindex="'+e+'">'+c[e]+"</"+b.slideElement+">";D.wrapper.innerHTML=a,D.calcSlides(!0),b.loader.loadAllSlides||D.wrapperTransitionEnd(D.reloadSlides,!0)},D.reloadSlides=function(){var a=b.loader.slides,c=parseInt(D.activeSlide().data("swiperindex"),10);if(!(0>c||c>a.length-1)){D.activeLoaderIndex=c;var d=Math.max(0,c-b.slidesPerView*b.loader.surroundGroups),e=Math.min(c+b.slidesPerView*(1+b.loader.surroundGroups)-1,a.length-1);if(c>0){var f=-F*(c-d);D.setWrapperTranslate(f),D.setWrapperTransition(0)}var g;if("reload"===b.loader.logic){D.wrapper.innerHTML="";var h="";for(g=d;e>=g;g++)h+="outer"===b.loader.slidesHTMLType?a[g]:"<"+b.slideElement+' class="'+b.slideClass+'" data-swiperindex="'+g+'">'+a[g]+"</"+b.slideElement+">";D.wrapper.innerHTML=h}else{var i=1e3,j=0;for(g=0;g<D.slides.length;g++){var k=D.slides[g].data("swiperindex");d>k||k>e?D.wrapper.removeChild(D.slides[g]):(i=Math.min(k,i),j=Math.max(k,j))}for(g=d;e>=g;g++){var l;i>g&&(l=document.createElement(b.slideElement),l.className=b.slideClass,l.setAttribute("data-swiperindex",g),l.innerHTML=a[g],D.wrapper.insertBefore(l,D.wrapper.firstChild)),g>j&&(l=document.createElement(b.slideElement),l.className=b.slideClass,l.setAttribute("data-swiperindex",g),l.innerHTML=a[g],D.wrapper.appendChild(l))}}D.reInit(!0)}},B()}};Swiper.prototype={plugins:{},wrapperTransitionEnd:function(a,b){"use strict";function c(h){if(h.target===f&&(a(e),e.params.queueEndCallbacks&&(e._queueEndCallbacks=!1),!b))for(d=0;d<g.length;d++)e.h.removeEventListener(f,g[d],c)}var d,e=this,f=e.wrapper,g=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"];if(a)for(d=0;d<g.length;d++)e.h.addEventListener(f,g[d],c)},getWrapperTranslate:function(a){"use strict";var b,c,d,e,f=this.wrapper;return"undefined"==typeof a&&(a="horizontal"===this.params.mode?"x":"y"),this.support.transforms&&this.params.useCSS3Transforms?(d=window.getComputedStyle(f,null),window.WebKitCSSMatrix?e=new WebKitCSSMatrix("none"===d.webkitTransform?"":d.webkitTransform):(e=d.MozTransform||d.OTransform||d.MsTransform||d.msTransform||d.transform||d.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),b=e.toString().split(",")),"x"===a&&(c=window.WebKitCSSMatrix?e.m41:parseFloat(16===b.length?b[12]:b[4])),"y"===a&&(c=window.WebKitCSSMatrix?e.m42:parseFloat(16===b.length?b[13]:b[5]))):("x"===a&&(c=parseFloat(f.style.left,10)||0),"y"===a&&(c=parseFloat(f.style.top,10)||0)),c||0},setWrapperTranslate:function(a,b,c){"use strict";var d,e=this.wrapper.style,f={x:0,y:0,z:0};3===arguments.length?(f.x=a,f.y=b,f.z=c):("undefined"==typeof b&&(b="horizontal"===this.params.mode?"x":"y"),f[b]=a),this.support.transforms&&this.params.useCSS3Transforms?(d=this.support.transforms3d?"translate3d("+f.x+"px, "+f.y+"px, "+f.z+"px)":"translate("+f.x+"px, "+f.y+"px)",e.webkitTransform=e.MsTransform=e.msTransform=e.MozTransform=e.OTransform=e.transform=d):(e.left=f.x+"px",e.top=f.y+"px"),this.callPlugins("onSetWrapperTransform",f),this.params.onSetWrapperTransform&&this.fireCallback(this.params.onSetWrapperTransform,this,f)},setWrapperTransition:function(a){"use strict";var b=this.wrapper.style;b.webkitTransitionDuration=b.MsTransitionDuration=b.msTransitionDuration=b.MozTransitionDuration=b.OTransitionDuration=b.transitionDuration=a/1e3+"s",this.callPlugins("onSetWrapperTransition",{duration:a}),this.params.onSetWrapperTransition&&this.fireCallback(this.params.onSetWrapperTransition,this,a)},h:{getWidth:function(a,b,c){"use strict";var d=window.getComputedStyle(a,null).getPropertyValue("width"),e=parseFloat(d);return(isNaN(e)||d.indexOf("%")>0||0>e)&&(e=a.offsetWidth-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-left"))-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-right"))),b&&(e+=parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-left"))+parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-right"))),c?Math.ceil(e):e},getHeight:function(a,b,c){"use strict";if(b)return a.offsetHeight;var d=window.getComputedStyle(a,null).getPropertyValue("height"),e=parseFloat(d);return(isNaN(e)||d.indexOf("%")>0||0>e)&&(e=a.offsetHeight-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-top"))-parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-bottom"))),b&&(e+=parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-top"))+parseFloat(window.getComputedStyle(a,null).getPropertyValue("padding-bottom"))),c?Math.ceil(e):e},getOffset:function(a){"use strict";var b=a.getBoundingClientRect(),c=document.body,d=a.clientTop||c.clientTop||0,e=a.clientLeft||c.clientLeft||0,f=window.pageYOffset||a.scrollTop,g=window.pageXOffset||a.scrollLeft;return document.documentElement&&!window.pageYOffset&&(f=document.documentElement.scrollTop,g=document.documentElement.scrollLeft),{top:b.top+f-d,left:b.left+g-e}},windowWidth:function(){"use strict";return window.innerWidth?window.innerWidth:document.documentElement&&document.documentElement.clientWidth?document.documentElement.clientWidth:void 0},windowHeight:function(){"use strict";return window.innerHeight?window.innerHeight:document.documentElement&&document.documentElement.clientHeight?document.documentElement.clientHeight:void 0},windowScroll:function(){"use strict";return"undefined"!=typeof pageYOffset?{left:window.pageXOffset,top:window.pageYOffset}:document.documentElement?{left:document.documentElement.scrollLeft,top:document.documentElement.scrollTop}:void 0},addEventListener:function(a,b,c,d){"use strict";"undefined"==typeof d&&(d=!1),a.addEventListener?a.addEventListener(b,c,d):a.attachEvent&&a.attachEvent("on"+b,c)},removeEventListener:function(a,b,c,d){"use strict";"undefined"==typeof d&&(d=!1),a.removeEventListener?a.removeEventListener(b,c,d):a.detachEvent&&a.detachEvent("on"+b,c)}},setTransform:function(a,b){"use strict";var c=a.style;c.webkitTransform=c.MsTransform=c.msTransform=c.MozTransform=c.OTransform=c.transform=b},setTranslate:function(a,b){"use strict";var c=a.style,d={x:b.x||0,y:b.y||0,z:b.z||0},e=this.support.transforms3d?"translate3d("+d.x+"px,"+d.y+"px,"+d.z+"px)":"translate("+d.x+"px,"+d.y+"px)";c.webkitTransform=c.MsTransform=c.msTransform=c.MozTransform=c.OTransform=c.transform=e,this.support.transforms||(c.left=d.x+"px",c.top=d.y+"px")},setTransition:function(a,b){"use strict";var c=a.style;c.webkitTransitionDuration=c.MsTransitionDuration=c.msTransitionDuration=c.MozTransitionDuration=c.OTransitionDuration=c.transitionDuration=b+"ms"},support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){"use strict";return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){"use strict";var a=document.createElement("div").style;return"webkitPerspective"in a||"MozPerspective"in a||"OPerspective"in a||"MsPerspective"in a||"perspective"in a}(),transforms:window.Modernizr&&Modernizr.csstransforms===!0||function(){"use strict";var a=document.createElement("div").style;return"transform"in a||"WebkitTransform"in a||"MozTransform"in a||"msTransform"in a||"MsTransform"in a||"OTransform"in a}(),transitions:window.Modernizr&&Modernizr.csstransitions===!0||function(){"use strict";var a=document.createElement("div").style;return"transition"in a||"WebkitTransition"in a||"MozTransition"in a||"msTransition"in a||"MsTransition"in a||"OTransition"in a}(),classList:function(){"use strict";var a=document.createElement("div");return"classList"in a}()},browser:{ie8:function(){"use strict";var a=-1;if("Microsoft Internet Explorer"===navigator.appName){var b=navigator.userAgent,c=new RegExp(/MSIE ([0-9]{1,}[\.0-9]{0,})/);null!==c.exec(b)&&(a=parseFloat(RegExp.$1))}return-1!==a&&9>a}(),ie10:window.navigator.msPointerEnabled,ie11:window.navigator.pointerEnabled}},(window.jQuery||window.Zepto)&&!function(a){"use strict";a.fn.swiper=function(b){var c;return this.each(function(d){var e=a(this),f=new Swiper(e[0],b);d||(c=f),e.data("swiper",f)}),c}}(window.jQuery||window.Zepto),"undefined"!=typeof module?module.exports=Swiper:"function"==typeof define&&define.amd&&define([],function(){"use strict";return Swiper});
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){function i(){var b,c,d={height:f.innerHeight,width:f.innerWidth};return d.height||(b=e.compatMode,(b||!a.support.boxModel)&&(c="CSS1Compat"===b?g:e.body,d={height:c.clientHeight,width:c.clientWidth})),d}function j(){return{top:f.pageYOffset||g.scrollTop||e.body.scrollTop,left:f.pageXOffset||g.scrollLeft||e.body.scrollLeft}}function k(){if(b.length){var e=0,f=a.map(b,function(a){var b=a.data.selector,c=a.$element;return b?c.find(b):c});for(c=c||i(),d=d||j();e<b.length;e++)if(a.contains(g,f[e][0])){var h=a(f[e]),k={height:h[0].offsetHeight,width:h[0].offsetWidth},l=h.offset(),m=h.data("inview");if(!d||!c)return;l.top+k.height>d.top&&l.top<d.top+c.height&&l.left+k.width>d.left&&l.left<d.left+c.width?m||h.data("inview",!0).trigger("inview",[!0]):m&&h.data("inview",!1).trigger("inview",[!1])}}}var c,d,h,b=[],e=document,f=window,g=e.documentElement;a.event.special.inview={add:function(c){b.push({data:c,$element:a(this),element:this}),!h&&b.length&&(h=setInterval(k,250))},remove:function(a){for(var c=0;c<b.length;c++){var d=b[c];if(d.element===this&&d.data.guid===a.guid){b.splice(c,1);break}}b.length||(clearInterval(h),h=null)}},a(f).bind("scroll resize scrollstop",function(){c=d=null}),!g.addEventListener&&g.attachEvent&&g.attachEvent("onfocusin",function(){d=null})});
/*
 Sticky-kit v1.1.1 | WTFPL | Leaf Corcoran 2014 | http://leafo.net
 */
(function(){var k,e;k=this.jQuery||window.jQuery;e=k(window);k.fn.stick_in_parent=function(d){var v,y,n,p,h,C,s,G,q,H;null==d&&(d={});s=d.sticky_class;y=d.inner_scrolling;C=d.recalc_every;h=d.parent;p=d.offset_top;n=d.spacer;v=d.bottoming;null==p&&(p=0);null==h&&(h=void 0);null==y&&(y=!0);null==s&&(s="is_stuck");null==v&&(v=!0);G=function(a,d,q,z,D,t,r,E){var u,F,m,A,c,f,B,w,x,g,b;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);f=a.parent();null!=h&&(f=f.closest(h));if(!f.length)throw"failed to find stick parent";
    u=m=!1;(g=null!=n?n&&a.closest(n):k("<div />"))&&g.css("position",a.css("position"));B=function(){var c,e,l;if(!E&&(c=parseInt(f.css("border-top-width"),10),e=parseInt(f.css("padding-top"),10),d=parseInt(f.css("padding-bottom"),10),q=f.offset().top+c+e,z=f.height(),m&&(u=m=!1,null==n&&(a.insertAfter(g),g.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(s),l=!0),D=a.offset().top-parseInt(a.css("margin-top"),10)-p,t=a.outerHeight(!0),r=a.css("float"),g&&g.css({width:a.outerWidth(!0),
            height:t,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),l))return b()};B();if(t!==z)return A=void 0,c=p,x=C,b=function(){var b,k,l,h;if(!E&&(null!=x&&(--x,0>=x&&(x=C,B())),l=e.scrollTop(),null!=A&&(k=l-A),A=l,m?(v&&(h=l+t+c>z+q,u&&!h&&(u=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom"))),l<D&&(m=!1,c=p,null==n&&("left"!==r&&"right"!==r||a.insertAfter(g),g.detach()),b={position:"",width:"",top:""},a.css(b).removeClass(s).trigger("sticky_kit:unstick")),
        y&&(b=e.height(),t+p>b&&!u&&(c-=k,c=Math.max(b-t,c),c=Math.min(p,c),m&&a.css({top:c+"px"})))):l>D&&(m=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(b).addClass(s),null==n&&(a.after(g),"left"!==r&&"right"!==r||g.append(a)),a.trigger("sticky_kit:stick")),m&&v&&(null==h&&(h=l+t+c>z+q),!u&&h)))return u=!0,"static"===f.css("position")&&f.css({position:"relative"}),a.css({position:"absolute",bottom:d,top:"auto"}).trigger("sticky_kit:bottom")},
        w=function(){B();return b()},F=function(){E=!0;e.off("touchmove",b);e.off("scroll",b);e.off("resize",w);k(document.body).off("sticky_kit:recalc",w);a.off("sticky_kit:detach",F);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});f.position("position","");if(m)return null==n&&("left"!==r&&"right"!==r||a.insertAfter(g),g.remove()),a.removeClass(s)},e.on("touchmove",b),e.on("scroll",b),e.on("resize",w),k(document.body).on("sticky_kit:recalc",w),a.on("sticky_kit:detach",F),setTimeout(b,
        0)}};q=0;for(H=this.length;q<H;q++)d=this[q],G(k(d));return this}}).call(this);
/*! noUiSlider - 7.0.10 - 2014-12-27 14:50:47 */

!function(){"use strict";function a(a){return a.split("").reverse().join("")}function b(a,b){return a.substring(0,b.length)===b}function c(a,b){return a.slice(-1*b.length)===b}function d(a,b,c){if((a[b]||a[c])&&a[b]===a[c])throw new Error(b)}function e(a){return"number"==typeof a&&isFinite(a)}function f(a,b){var c=Math.pow(10,b);return(Math.round(a*c)/c).toFixed(b)}function g(b,c,d,g,h,i,j,k,l,m,n,o){var p,q,r,s=o,t="",u="";return i&&(o=i(o)),e(o)?(b!==!1&&0===parseFloat(o.toFixed(b))&&(o=0),0>o&&(p=!0,o=Math.abs(o)),b!==!1&&(o=f(o,b)),o=o.toString(),-1!==o.indexOf(".")?(q=o.split("."),r=q[0],d&&(t=d+q[1])):r=o,c&&(r=a(r).match(/.{1,3}/g),r=a(r.join(a(c)))),p&&k&&(u+=k),g&&(u+=g),p&&l&&(u+=l),u+=r,u+=t,h&&(u+=h),m&&(u=m(u,s)),u):!1}function h(a,d,f,g,h,i,j,k,l,m,n,o){var p,q="";return n&&(o=n(o)),o&&"string"==typeof o?(k&&b(o,k)&&(o=o.replace(k,""),p=!0),g&&b(o,g)&&(o=o.replace(g,"")),l&&b(o,l)&&(o=o.replace(l,""),p=!0),h&&c(o,h)&&(o=o.slice(0,-1*h.length)),d&&(o=o.split(d).join("")),f&&(o=o.replace(f,".")),p&&(q+="-"),q+=o,q=q.replace(/[^0-9\.\-.]/g,""),""===q?!1:(q=Number(q),j&&(q=j(q)),e(q)?q:!1)):!1}function i(a){var b,c,e,f={};for(b=0;b<l.length;b+=1)if(c=l[b],e=a[c],void 0===e)f[c]="negative"!==c||f.negativeBefore?"mark"===c&&"."!==f.thousand?".":!1:"-";else if("decimals"===c){if(!(e>=0&&8>e))throw new Error(c);f[c]=e}else if("encoder"===c||"decoder"===c||"edit"===c||"undo"===c){if("function"!=typeof e)throw new Error(c);f[c]=e}else{if("string"!=typeof e)throw new Error(c);f[c]=e}return d(f,"mark","thousand"),d(f,"prefix","negative"),d(f,"prefix","negativeBefore"),f}function j(a,b,c){var d,e=[];for(d=0;d<l.length;d+=1)e.push(a[l[d]]);return e.push(c),b.apply("",e)}function k(a){return this instanceof k?void("object"==typeof a&&(a=i(a),this.to=function(b){return j(a,g,b)},this.from=function(b){return j(a,h,b)})):new k(a)}var l=["decimals","thousand","mark","prefix","postfix","encoder","decoder","negativeBefore","negative","edit","undo"];window.wNumb=k}(),function(a){"use strict";function b(b){return b instanceof a||a.zepto&&a.zepto.isZ(b)}function c(b,c){return"string"==typeof b&&0===b.indexOf("-inline-")?(this.method=c||"html",this.target=this.el=a(b.replace("-inline-","")||"<div/>"),!0):void 0}function d(b){if("string"==typeof b&&0!==b.indexOf("-")){this.method="val";var c=document.createElement("input");return c.name=b,c.type="hidden",this.target=this.el=a(c),!0}}function e(a){return"function"==typeof a?(this.target=!1,this.method=a,!0):void 0}function f(a,c){return b(a)&&!c?(a.is("input, select, textarea")?(this.method="val",this.target=a.on("change.liblink",this.changeHandler)):(this.target=a,this.method="html"),!0):void 0}function g(a,c){return b(a)&&("function"==typeof c||"string"==typeof c&&a[c])?(this.method=c,this.target=a,!0):void 0}function h(b,c,d){var e=this,f=!1;if(this.changeHandler=function(b){var c=e.formatInstance.from(a(this).val());return c===!1||isNaN(c)?(a(this).val(e.lastSetValue),!1):void e.changeHandlerMethod.call("",b,c)},this.el=!1,this.formatInstance=d,a.each(k,function(a,d){return f=d.call(e,b,c),!f}),!f)throw new RangeError("(Link) Invalid Link.")}function i(a){this.items=[],this.elements=[],this.origin=a}function j(b,c,d,e){0===b&&(b=this.LinkDefaultFlag),this.linkAPI||(this.linkAPI={}),this.linkAPI[b]||(this.linkAPI[b]=new i(this));var f=new h(c,d,e||this.LinkDefaultFormatter);f.target||(f.target=a(this)),f.changeHandlerMethod=this.LinkConfirm(b,f.el),this.linkAPI[b].push(f,f.el),this.LinkUpdate(b)}var k=[c,d,e,f,g];h.prototype.set=function(a){var b=Array.prototype.slice.call(arguments),c=b.slice(1);this.lastSetValue=this.formatInstance.to(a),c.unshift(this.lastSetValue),("function"==typeof this.method?this.method:this.target[this.method]).apply(this.target,c)},i.prototype.push=function(a,b){this.items.push(a),b&&this.elements.push(b)},i.prototype.reconfirm=function(a){var b;for(b=0;b<this.elements.length;b+=1)this.origin.LinkConfirm(a,this.elements[b])},i.prototype.remove=function(){var a;for(a=0;a<this.items.length;a+=1)this.items[a].target.off(".liblink");for(a=0;a<this.elements.length;a+=1)this.elements[a].remove()},i.prototype.change=function(a){if(this.origin.LinkIsEmitting)return!1;this.origin.LinkIsEmitting=!0;var b,c=Array.prototype.slice.call(arguments,1);for(c.unshift(a),b=0;b<this.items.length;b+=1)this.items[b].set.apply(this.items[b],c);this.origin.LinkIsEmitting=!1},a.fn.Link=function(b){var c=this;if(b===!1)return c.each(function(){this.linkAPI&&(a.map(this.linkAPI,function(a){a.remove()}),delete this.linkAPI)});if(void 0===b)b=0;else if("string"!=typeof b)throw new Error("Flag must be string.");return{to:function(a,d,e){return c.each(function(){j.call(this,b,a,d,e)})}}}}(window.jQuery||window.Zepto),function(a){"use strict";function b(b){return a.grep(b,function(c,d){return d===a.inArray(c,b)})}function c(a,b){return Math.round(a/b)*b}function d(a){return"number"==typeof a&&!isNaN(a)&&isFinite(a)}function e(a){var b=Math.pow(10,7);return Number((Math.round(a*b)/b).toFixed(7))}function f(a,b,c){a.addClass(b),setTimeout(function(){a.removeClass(b)},c)}function g(a){return Math.max(Math.min(a,100),0)}function h(b){return a.isArray(b)?b:[b]}function i(a){var b=a.split(".");return b.length>1?b[1].length:0}function j(a,b){return 100/(b-a)}function k(a,b){return 100*b/(a[1]-a[0])}function l(a,b){return k(a,a[0]<0?b+Math.abs(a[0]):b-a[0])}function m(a,b){return b*(a[1]-a[0])/100+a[0]}function n(a,b){for(var c=1;a>=b[c];)c+=1;return c}function o(a,b,c){if(c>=a.slice(-1)[0])return 100;var d,e,f,g,h=n(c,a);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],f+l([d,e],c)/j(f,g)}function p(a,b,c){if(c>=100)return a.slice(-1)[0];var d,e,f,g,h=n(c,b);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],m([d,e],(c-f)*j(f,g))}function q(a,b,d,e){if(100===e)return e;var f,g,h=n(e,a);return d?(f=a[h-1],g=a[h],e-f>(g-f)/2?g:f):b[h-1]?a[h-1]+c(e-a[h-1],b[h-1]):e}function r(a,b,c){var e;if("number"==typeof b&&(b=[b]),"[object Array]"!==Object.prototype.toString.call(b))throw new Error("noUiSlider: 'range' contains invalid value.");if(e="min"===a?0:"max"===a?100:parseFloat(a),!d(e)||!d(b[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");c.xPct.push(e),c.xVal.push(b[0]),e?c.xSteps.push(isNaN(b[1])?!1:b[1]):isNaN(b[1])||(c.xSteps[0]=b[1])}function s(a,b,c){return b?void(c.xSteps[a]=k([c.xVal[a],c.xVal[a+1]],b)/j(c.xPct[a],c.xPct[a+1])):!0}function t(a,b,c,d){this.xPct=[],this.xVal=[],this.xSteps=[d||!1],this.xNumSteps=[!1],this.snap=b,this.direction=c;var e,f=[];for(e in a)a.hasOwnProperty(e)&&f.push([a[e],e]);for(f.sort(function(a,b){return a[0]-b[0]}),e=0;e<f.length;e++)r(f[e][1],f[e][0],this);for(this.xNumSteps=this.xSteps.slice(0),e=0;e<this.xNumSteps.length;e++)s(e,this.xNumSteps[e],this)}function u(a,b){if(!d(b))throw new Error("noUiSlider: 'step' is not numeric.");a.singleStep=b}function v(b,c){if("object"!=typeof c||a.isArray(c))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===c.min||void 0===c.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");b.spectrum=new t(c,b.snap,b.dir,b.singleStep)}function w(b,c){if(c=h(c),!a.isArray(c)||!c.length||c.length>2)throw new Error("noUiSlider: 'start' option is incorrect.");b.handles=c.length,b.start=c}function x(a,b){if(a.snap=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function y(a,b){if(a.animate=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function z(a,b){if("lower"===b&&1===a.handles)a.connect=1;else if("upper"===b&&1===a.handles)a.connect=2;else if(b===!0&&2===a.handles)a.connect=3;else{if(b!==!1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");a.connect=0}}function A(a,b){switch(b){case"horizontal":a.ort=0;break;case"vertical":a.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function B(a,b){if(!d(b))throw new Error("noUiSlider: 'margin' option must be numeric.");if(a.margin=a.spectrum.getMargin(b),!a.margin)throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function C(a,b){if(!d(b))throw new Error("noUiSlider: 'limit' option must be numeric.");if(a.limit=a.spectrum.getMargin(b),!a.limit)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")}function D(a,b){switch(b){case"ltr":a.dir=0;break;case"rtl":a.dir=1,a.connect=[0,2,1,3][a.connect];break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function E(a,b){if("string"!=typeof b)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var c=b.indexOf("tap")>=0,d=b.indexOf("drag")>=0,e=b.indexOf("fixed")>=0,f=b.indexOf("snap")>=0;a.events={tap:c||f,drag:d,fixed:e,snap:f}}function F(a,b){if(a.format=b,"function"==typeof b.to&&"function"==typeof b.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function G(b){var c,d={margin:0,limit:0,animate:!0,format:Z};return c={step:{r:!1,t:u},start:{r:!0,t:w},connect:{r:!0,t:z},direction:{r:!0,t:D},snap:{r:!1,t:x},animate:{r:!1,t:y},range:{r:!0,t:v},orientation:{r:!1,t:A},margin:{r:!1,t:B},limit:{r:!1,t:C},behaviour:{r:!0,t:E},format:{r:!1,t:F}},b=a.extend({connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal"},b),a.each(c,function(a,c){if(void 0===b[a]){if(c.r)throw new Error("noUiSlider: '"+a+"' is required.");return!0}c.t(d,b[a])}),d.style=d.ort?"top":"left",d}function H(a,b,c){var d=a+b[0],e=a+b[1];return c?(0>d&&(e+=Math.abs(d)),e>100&&(d-=e-100),[g(d),g(e)]):[d,e]}function I(a){a.preventDefault();var b,c,d=0===a.type.indexOf("touch"),e=0===a.type.indexOf("mouse"),f=0===a.type.indexOf("pointer"),g=a;return 0===a.type.indexOf("MSPointer")&&(f=!0),a.originalEvent&&(a=a.originalEvent),d&&(b=a.changedTouches[0].pageX,c=a.changedTouches[0].pageY),(e||f)&&(f||void 0!==window.pageXOffset||(window.pageXOffset=document.documentElement.scrollLeft,window.pageYOffset=document.documentElement.scrollTop),b=a.clientX+window.pageXOffset,c=a.clientY+window.pageYOffset),g.points=[b,c],g.cursor=e,g}function J(b,c){var d=a("<div><div/></div>").addClass(Y[2]),e=["-lower","-upper"];return b&&e.reverse(),d.children().addClass(Y[3]+" "+Y[3]+e[c]),d}function K(a,b,c){switch(a){case 1:b.addClass(Y[7]),c[0].addClass(Y[6]);break;case 3:c[1].addClass(Y[6]);case 2:c[0].addClass(Y[7]);case 0:b.addClass(Y[6])}}function L(a,b,c){var d,e=[];for(d=0;a>d;d+=1)e.push(J(b,d).appendTo(c));return e}function M(b,c,d){return d.addClass([Y[0],Y[8+b],Y[4+c]].join(" ")),a("<div/>").appendTo(d).addClass(Y[1])}function N(b,c,d){function e(){return C[["width","height"][c.ort]]()}function j(a){var b,c=[E.val()];for(b=0;b<a.length;b+=1)E.trigger(a[b],c)}function k(a){return 1===a.length?a[0]:c.dir?a.reverse():a}function l(a){return function(b,c){E.val([a?null:c,a?c:null],!0)}}function m(b){var c=a.inArray(b,N);E[0].linkAPI&&E[0].linkAPI[b]&&E[0].linkAPI[b].change(J[c],D[c].children(),E)}function n(b,d){var e=a.inArray(b,N);return d&&d.appendTo(D[e].children()),c.dir&&c.handles>1&&(e=1===e?0:1),l(e)}function o(){var a,b;for(a=0;a<N.length;a+=1)this.linkAPI&&this.linkAPI[b=N[a]]&&this.linkAPI[b].reconfirm(b)}function p(a,b,d,e){return a=a.replace(/\s/g,W+" ")+W,b.on(a,function(a){return E.attr("disabled")?!1:E.hasClass(Y[14])?!1:(a=I(a),a.calcPoint=a.points[c.ort],void d(a,e))})}function q(a,b){var c,d=b.handles||D,f=!1,g=100*(a.calcPoint-b.start)/e(),h=d[0][0]!==D[0][0]?1:0;c=H(g,b.positions,d.length>1),f=v(d[0],c[h],1===d.length),d.length>1&&(f=v(d[1],c[h?0:1],!1)||f),f&&j(["slide"])}function r(b){a("."+Y[15]).removeClass(Y[15]),b.cursor&&a("body").css("cursor","").off(W),U.off(W),E.removeClass(Y[12]),j(["set","change"])}function s(b,c){1===c.handles.length&&c.handles[0].children().addClass(Y[15]),b.stopPropagation(),p(X.move,U,q,{start:b.calcPoint,handles:c.handles,positions:[F[0],F[D.length-1]]}),p(X.end,U,r,null),b.cursor&&(a("body").css("cursor",a(b.target).css("cursor")),D.length>1&&E.addClass(Y[12]),a("body").on("selectstart"+W,!1))}function t(b){var d,g=b.calcPoint,h=0;b.stopPropagation(),a.each(D,function(){h+=this.offset()[c.style]}),h=h/2>g||1===D.length?0:1,g-=C.offset()[c.style],d=100*g/e(),c.events.snap||f(E,Y[14],300),v(D[h],d),j(["slide","set","change"]),c.events.snap&&s(b,{handles:[D[h]]})}function u(a){var b,c;if(!a.fixed)for(b=0;b<D.length;b+=1)p(X.start,D[b].children(),s,{handles:[D[b]]});a.tap&&p(X.start,C,t,{handles:D}),a.drag&&(c=C.find("."+Y[7]).addClass(Y[10]),a.fixed&&(c=c.add(C.children().not(c).children())),p(X.start,c,s,{handles:D}))}function v(a,b,d){var e=a[0]!==D[0][0]?1:0,f=F[0]+c.margin,h=F[1]-c.margin,i=F[0]+c.limit,j=F[1]-c.limit;return D.length>1&&(b=e?Math.max(b,f):Math.min(b,h)),d!==!1&&c.limit&&D.length>1&&(b=e?Math.min(b,i):Math.max(b,j)),b=G.getStep(b),b=g(parseFloat(b.toFixed(7))),b===F[e]?!1:(a.css(c.style,b+"%"),a.is(":first-child")&&a.toggleClass(Y[17],b>50),F[e]=b,J[e]=G.fromStepping(b),m(N[e]),!0)}function w(a,b){var d,e,f;for(c.limit&&(a+=1),d=0;a>d;d+=1)e=d%2,f=b[e],null!==f&&f!==!1&&("number"==typeof f&&(f=String(f)),f=c.format.from(f),(f===!1||isNaN(f)||v(D[e],G.toStepping(f),d===3-c.dir)===!1)&&m(N[e]))}function x(a){if(E[0].LinkIsEmitting)return this;var b,d=h(a);return c.dir&&c.handles>1&&d.reverse(),c.animate&&-1!==F[0]&&f(E,Y[14],300),b=D.length>1?3:1,1===d.length&&(b=1),w(b,d),j(["set"]),this}function y(){var a,b=[];for(a=0;a<c.handles;a+=1)b[a]=c.format.to(J[a]);return k(b)}function z(){return a(this).off(W).removeClass(Y.join(" ")).empty(),delete this.LinkUpdate,delete this.LinkConfirm,delete this.LinkDefaultFormatter,delete this.LinkDefaultFlag,delete this.reappend,delete this.vGet,delete this.vSet,delete this.getCurrentStep,delete this.getInfo,delete this.destroy,d}function A(){var b=a.map(F,function(a,b){var c=G.getApplicableStep(a),d=i(String(c[2])),e=J[b],f=100===a?null:c[2],g=Number((e-c[2]).toFixed(d)),h=0===a?null:g>=c[1]?c[2]:c[0]||!1;return[[h,f]]});return k(b)}function B(){return d}var C,D,E=a(b),F=[-1,-1],G=c.spectrum,J=[],N=["lower","upper"].slice(0,c.handles);if(c.dir&&N.reverse(),b.LinkUpdate=m,b.LinkConfirm=n,b.LinkDefaultFormatter=c.format,b.LinkDefaultFlag="lower",b.reappend=o,E.hasClass(Y[0]))throw new Error("Slider was already initialized.");C=M(c.dir,c.ort,E),D=L(c.handles,c.dir,C),K(c.connect,E,D),u(c.events),b.vSet=x,b.vGet=y,b.destroy=z,b.getCurrentStep=A,b.getOriginalOptions=B,b.getInfo=function(){return[G,c.style,c.ort]},E.val(c.start)}function O(a){var b=G(a,this);return this.each(function(){N(this,b,a)})}function P(b){return this.each(function(){if(!this.destroy)return void a(this).noUiSlider(b);var c=a(this).val(),d=this.destroy(),e=a.extend({},d,b);a(this).noUiSlider(e),this.reappend(),d.start===e.start&&a(this).val(c)})}function Q(){return this[0][arguments.length?"vSet":"vGet"].apply(this[0],arguments)}function R(b,c,d,e){if("range"===c||"steps"===c)return b.xVal;if("count"===c){var f,g=100/(d-1),h=0;for(d=[];(f=h++*g)<=100;)d.push(f);c="positions"}return"positions"===c?a.map(d,function(a){return b.fromStepping(e?b.getStep(a):a)}):"values"===c?e?a.map(d,function(a){return b.fromStepping(b.getStep(b.toStepping(a)))}):d:void 0}function S(c,d,e,f){var g=c.direction,h={},i=c.xVal[0],j=c.xVal[c.xVal.length-1],k=!1,l=!1,m=0;return c.direction=0,f=b(f.slice().sort(function(a,b){return a-b})),f[0]!==i&&(f.unshift(i),k=!0),f[f.length-1]!==j&&(f.push(j),l=!0),a.each(f,function(b){var g,i,j,n,o,p,q,r,s,t,u=f[b],v=f[b+1];if("steps"===e&&(g=c.xNumSteps[b]),g||(g=v-u),u!==!1&&void 0!==v)for(i=u;v>=i;i+=g){for(n=c.toStepping(i),o=n-m,r=o/d,s=Math.round(r),t=o/s,j=1;s>=j;j+=1)p=m+j*t,h[p.toFixed(5)]=["x",0];q=a.inArray(i,f)>-1?1:"steps"===e?2:0,!b&&k&&(q=0),i===v&&l||(h[n.toFixed(5)]=[i,q]),m=n}}),c.direction=g,h}function T(b,c,d,e,f,g){function h(a){return["-normal","-large","-sub"][a]}function i(a,c,d){return'class="'+c+" "+c+"-"+k+" "+c+h(d[1],d[0])+'" style="'+b+": "+a+'%"'}function j(a,b){d&&(a=100-a),b[1]=b[1]&&f?f(b[0],b[1]):b[1],l.append("<div "+i(a,"noUi-marker",b)+"></div>"),b[1]&&l.append("<div "+i(a,"noUi-value",b)+">"+g.to(b[0])+"</div>")}var k=["horizontal","vertical"][c],l=a("<div/>");return l.addClass("noUi-pips noUi-pips-"+k),a.each(e,j),l}var U=a(document),V=a.fn.val,W=".nui",X=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},Y=["noUi-target","noUi-base","noUi-origin","noUi-handle","noUi-horizontal","noUi-vertical","noUi-background","noUi-connect","noUi-ltr","noUi-rtl","noUi-dragable","","noUi-state-drag","","noUi-state-tap","noUi-active","","noUi-stacking"];t.prototype.getMargin=function(a){return 2===this.xPct.length?k(this.xVal,a):!1},t.prototype.toStepping=function(a){return a=o(this.xVal,this.xPct,a),this.direction&&(a=100-a),a},t.prototype.fromStepping=function(a){return this.direction&&(a=100-a),e(p(this.xVal,this.xPct,a))},t.prototype.getStep=function(a){return this.direction&&(a=100-a),a=q(this.xPct,this.xSteps,this.snap,a),this.direction&&(a=100-a),a},t.prototype.getApplicableStep=function(a){var b=n(a,this.xPct),c=100===a?2:1;return[this.xNumSteps[b-2],this.xVal[b-c],this.xNumSteps[b-c]]},t.prototype.convert=function(a){return this.getStep(this.toStepping(a))};var Z={to:function(a){return a.toFixed(2)},from:Number};a.fn.val=function(b){function c(a){return a.hasClass(Y[0])?Q:V}if(!arguments.length){var d=a(this[0]);return c(d).call(d)}var e=a.isFunction(b);return this.each(function(d){var f=b,g=a(this);e&&(f=b.call(this,d,g.val())),c(g).call(g,f)})},a.fn.noUiSlider=function(a,b){switch(a){case"step":return this[0].getCurrentStep();case"options":return this[0].getOriginalOptions()}return(b?P:O).call(this,a)},a.fn.noUiSlider_pips=function(b){var c=b.mode,d=b.density||1,e=b.filter||!1,f=b.values||!1,g=b.format||{to:Math.round},h=b.stepped||!1;return this.each(function(){var b=this.getInfo(),i=R(b[0],c,f,h),j=S(b[0],d,c,i);return a(this).append(T(b[1],b[2],b[0].direction,j,e,g))})}}(window.jQuery||window.Zepto);
(function(factory) {
  /* global define */
  /* istanbul ignore next */
  if ( typeof define === 'function' && define.amd ) {
    define(['jquery'], factory);
  } else if ( typeof module === 'object' && module.exports ) {
    // Node/CommonJS
    module.exports = function( root, jQuery ) {
      if ( jQuery === undefined ) {
        if ( typeof window !== 'undefined' ) {
          jQuery = require('jquery');
        } else {
          jQuery = require('jquery')(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function($) {
  'use strict';

  var $doc = $(document);
  var $win = $(window);

  var pluginName = 'selectric';
  var classList = 'Input Items Open Disabled TempShow HideSelect Wrapper Focus Hover Responsive Above Scroll Group GroupLabel';
  var bindSufix = '.sl';

  var chars = ['a', 'e', 'i', 'o', 'u', 'n', 'c', 'y'];
  var diacritics = [
    /[\xE0-\xE5]/g, // a
    /[\xE8-\xEB]/g, // e
    /[\xEC-\xEF]/g, // i
    /[\xF2-\xF6]/g, // o
    /[\xF9-\xFC]/g, // u
    /[\xF1]/g,      // n
    /[\xE7]/g,      // c
    /[\xFD-\xFF]/g  // y
  ];

  /**
   * Create an instance of Selectric
   *
   * @constructor
   * @param {Node} element - The &lt;select&gt; element
   * @param {object}  opts - Options
   */
  var Selectric = function(element, opts) {
    var _this = this;

    _this.element = element;
    _this.$element = $(element);

    _this.state = {
      enabled     : false,
      opened      : false,
      currValue   : -1,
      selectedIdx : -1
    };

    _this.eventTriggers = {
      open    : _this.open,
      close   : _this.close,
      destroy : _this.destroy,
      refresh : _this.refresh,
      init    : _this.init
    };

    _this.init(opts);
  };

  Selectric.prototype = {
    utils: {
      /**
       * Detect mobile browser
       *
       * @return {boolean}
       */
      isMobile: function() {
        return /android|ip(hone|od|ad)/i.test(navigator.userAgent);
      },

      /**
       * Escape especial characters in string (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)
       *
       * @param  {string} str - The string to be escaped
       * @return {string}       The string with the special characters escaped
       */
      escapeRegExp: function(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
      },

      /**
       * Replace diacritics
       *
       * @param  {string} str - The string to replace the diacritics
       * @return {string}       The string with diacritics replaced with ascii characters
       */
      replaceDiacritics: function(str) {
        var k = diacritics.length;

        while (k--) {
          str = str.toLowerCase().replace(diacritics[k], chars[k]);
        }

        return str;
      },

      /**
       * Format string
       * https://gist.github.com/atesgoral/984375
       *
       * @param  {string} f - String to be formated
       * @return {string}     String formated
       */
      format: function (f) {
        var a = arguments; // store outer arguments
        return ('' + f) // force format specifier to String
          .replace( // replace tokens in format specifier
            /\{(?:(\d+)|(\w+))\}/g, // match {token} references
            function (
              s, // the matched string (ignored)
              i, // an argument index
              p // a property name
            ) {
              return p && a[1] // if property name and first argument exist
                ? a[1][p] // return property from first argument
                : a[i]; // assume argument index and return i-th argument
            });
      },

      /**
       * Get the next enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The next enabled item.
       */
      nextEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected + 1) % selectItems.length ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Get the previous enabled item in the options list.
       *
       * @param  {object} selectItems - The options object.
       * @param  {number}    selected - Index of the currently selected option.
       * @return {object}               The previous enabled item.
       */
      previousEnabledItem: function(selectItems, selected) {
        while ( selectItems[ selected = (selected > 0 ? selected : selectItems.length) - 1 ].disabled ) {
          // empty
        }
        return selected;
      },

      /**
       * Transform camelCase string to dash-case.
       *
       * @param  {string} str - The camelCased string.
       * @return {string}       The string transformed to dash-case.
       */
      toDash: function(str) {
        return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
      },

      /**
       * Calls the events and hooks registered with function name.
       *
       * @param {string}    fn - The name of the function.
       * @param {number} scope - Scope that should be set on the function.
       */
      triggerCallback: function(fn, scope) {
        var elm = scope.element;
        var func = scope.options['on' + fn];

        if ( $.isFunction(func) ) {
          func.call(elm, elm, scope);
        }

        if ( $.fn[pluginName].hooks[fn] ) {
          $.each($.fn[pluginName].hooks[fn], function() {
            this.call(elm, elm, scope);
          });
        }

        $(elm).trigger(pluginName + '-' + this.toDash(fn), scope);
      }
    },

    /** Initializes */
    init: function(opts) {
      var _this = this;

      // Set options
      _this.options = $.extend(true, {}, $.fn[pluginName].defaults, _this.options, opts);

      _this.utils.triggerCallback('BeforeInit', _this);

      // Preserve data
      _this.destroy(true);

      // Disable on mobile browsers
      if ( _this.options.disableOnMobile && _this.utils.isMobile() ) {
        _this.disableOnMobile = true;
        return;
      }

      // Get classes
      _this.classes = _this.getClassNames();

      // Create elements
      var input        = $('<input/>', { 'class': _this.classes.input, 'readonly': _this.utils.isMobile() });
      var items        = $('<div/>',   { 'class': _this.classes.items, 'tabindex': -1 });
      var itemsScroll  = $('<div/>',   { 'class': _this.classes.scroll });
      var wrapper      = $('<div/>',   { 'class': _this.classes.prefix, 'html': _this.options.arrowButtonMarkup });
      var label        = $('<span/>',  { 'class': 'label' });
      var outerWrapper = _this.$element.wrap('<div/>').parent().append(wrapper.prepend(label), items, input);

      _this.elements = {
        input        : input,
        items        : items,
        itemsScroll  : itemsScroll,
        wrapper      : wrapper,
        label        : label,
        outerWrapper : outerWrapper
      };

      _this.$element
        .on(_this.eventTriggers)
        .wrap('<div class="' + _this.classes.hideselect + '"/>');

      _this.originalTabindex = _this.$element.prop('tabindex');
      _this.$element.prop('tabindex', false);

      _this.populate();
      _this.activate();

      _this.utils.triggerCallback('Init', _this);
    },

    /** Activates the plugin */
    activate: function() {
      var _this = this;
      var originalWidth = _this.$element.width();

      _this.utils.triggerCallback('BeforeActivate', _this);

      _this.elements.outerWrapper.prop('class', [
        _this.classes.wrapper,
        _this.$element.prop('class').replace(/\S+/g, _this.classes.prefix + '-$&'),
        _this.options.responsive ? _this.classes.responsive : ''
      ].join(' '));

      if ( _this.options.inheritOriginalWidth && originalWidth > 0 ) {
        _this.elements.outerWrapper.width(originalWidth);
      }

      if ( !_this.$element.prop('disabled') ) {
        _this.state.enabled = true;

        // Not disabled, so... Removing disabled class
        _this.elements.outerWrapper.removeClass(_this.classes.disabled);

        // Remove styles from items box
        // Fix incorrect height when refreshed is triggered with fewer options
        _this.$li = _this.elements.items.removeAttr('style').find('li');

        _this.bindEvents();
      } else {
        _this.elements.outerWrapper.addClass(_this.classes.disabled);
        _this.elements.input.prop('disabled', true);
      }

      _this.utils.triggerCallback('Activate', _this);
    },

    /**
     * Generate classNames for elements
     *
     * @return {object} Classes object
     */
    getClassNames: function() {
      var _this = this;
      var customClass = _this.options.customClass;
      var classesObj  = {};

      $.each(classList.split(' '), function(i, currClass) {
        var c = customClass.prefix + currClass;
        classesObj[currClass.toLowerCase()] = customClass.camelCase ? c : _this.utils.toDash(c);
      });

      classesObj.prefix = customClass.prefix;

      return classesObj;
    },

    /** Set the label text */
    setLabel: function() {
      var _this = this;
      var labelBuilder = _this.options.labelBuilder;
      var currItem = _this.lookupItems[_this.state.currValue];

      _this.elements.label.html(
        $.isFunction(labelBuilder)
          ? labelBuilder(currItem)
          : _this.utils.format(labelBuilder, currItem)
      );
    },

    /** Get and save the available options */
    populate: function() {
      var _this = this;
      var $options = _this.$element.children();
      var $justOptions = _this.$element.find('option');
      var selectedIndex = $justOptions.index($justOptions.filter(':selected'));
      var currIndex = 0;

      _this.state.currValue = (_this.state.selected = ~selectedIndex ? selectedIndex : 0);
      _this.state.selectedIdx = _this.state.currValue;
      _this.items = [];
      _this.lookupItems = [];

      if ( $options.length ) {
        // Build options markup
        $options.each(function(i) {
          var $elm = $(this);

          if ( $elm.is('optgroup') ) {

            var optionsGroup = {
              element       : $elm,
              label         : $elm.prop('label'),
              groupDisabled : $elm.prop('disabled'),
              items         : []
            };

            $elm.children().each(function(i) {
              var $elm = $(this);
              var optionText = $elm.html();

              optionsGroup.items[i] = {
                index    : currIndex,
                element  : $elm,
                value    : $elm.val(),
                text     : optionText,
                slug     : _this.utils.replaceDiacritics(optionText),
                disabled : optionsGroup.groupDisabled
              };

              _this.lookupItems[currIndex] = optionsGroup.items[i];

              currIndex++;
            });

            _this.items[i] = optionsGroup;

          } else {

            var optionText = $elm.html();

            _this.items[i] = {
              index    : currIndex,
              element  : $elm,
              value    : $elm.val(),
              text     : optionText,
              slug     : _this.utils.replaceDiacritics(optionText),
              disabled : $elm.prop('disabled')
            };

            _this.lookupItems[currIndex] = _this.items[i];

            currIndex++;

          }
        });

        _this.setLabel();
        _this.elements.items.append( _this.elements.itemsScroll.html( _this.getItemsMarkup(_this.items) ) );
      }
    },

    /**
     * Generate options markup
     *
     * @param  {object} items - Object containing all available options
     * @return {string}         HTML for the options box
     */
    getItemsMarkup: function(items) {
      var _this = this;
      var markup = '<ul>';

      $.each(items, function(i, elm) {
        if ( elm.label !== undefined ) {

          markup += _this.utils.format('<ul class="{1}"><li class="{2}">{3}</li>',
            $.trim([_this.classes.group, elm.groupDisabled ? 'disabled' : '', elm.element.prop('class')].join(' ')),
            _this.classes.grouplabel,
            elm.element.prop('label')
          );

          $.each(elm.items, function(i, elm) {
            markup += _this.getItemMarkup(elm.index, elm);
          });

          markup += '</ul>';

        } else {

          markup += _this.getItemMarkup(elm.index, elm);

        }
      });

      return markup + '</ul>';
    },

    /**
     * Generate every option markup
     *
     * @param  {number} i   - Index of current item
     * @param  {object} elm - Current item
     * @return {string}       HTML for the option
     */
    getItemMarkup: function(i, elm) {
      var _this = this;
      var itemBuilder = _this.options.optionsItemBuilder;

      return _this.utils.format('<li data-index="{1}" class="{2}">{3}</li>',
        i,
        $.trim([
          i === _this.state.currValue  ? 'selected' : '',
          i === _this.items.length - 1 ? 'last'     : '',
          elm.disabled                 ? 'disabled' : ''
        ].join(' ')),
        $.isFunction(itemBuilder) ? itemBuilder(elm, elm.element, i) : _this.utils.format(itemBuilder, elm)
      );
    },

    /** Bind events on the elements */
    bindEvents: function() {
      var _this = this;

      _this.elements.wrapper
        .add(_this.$element)
        .add(_this.elements.outerWrapper)
        .add(_this.elements.input)
        .off(bindSufix);

      _this.elements.outerWrapper.on('mouseenter' + bindSufix + ' mouseleave' + bindSufix, function(e) {
        $(this).toggleClass(_this.classes.hover, e.type === 'mouseenter');

        // Delay close effect when openOnHover is true
        if ( _this.options.openOnHover ) {
          clearTimeout(_this.closeTimer);

          if ( e.type === 'mouseleave' ) {
            _this.closeTimer = setTimeout($.proxy(_this.close, _this), _this.options.hoverIntentTimeout);
          } else {
            _this.open();
          }
        }
      });

      // Toggle open/close
      _this.elements.wrapper.on('click' + bindSufix, function(e) {
        _this.state.opened ? _this.close() : _this.open(e);
      });

      _this.elements.input
        .prop({ tabindex: _this.originalTabindex, disabled: false })
        .on('keydown' + bindSufix, $.proxy(_this.handleKeys, _this))
        .on('focusin' + bindSufix, function(e) {
          _this.elements.outerWrapper.addClass(_this.classes.focus);

          // Prevent the flicker when focusing out and back again in the browser window
          _this.elements.input.one('blur', function() {
            _this.elements.input.blur();
          });

          if ( _this.options.openOnFocus && !_this.state.opened ) {
            _this.open(e);
          }
        })
        .on('focusout' + bindSufix, function() {
          _this.elements.outerWrapper.removeClass(_this.classes.focus);
        })
        .on('input propertychange', function() {
          var val = _this.elements.input.val();

          // Clear search
          clearTimeout(_this.resetStr);
          _this.resetStr = setTimeout(function() {
            _this.elements.input.val('');
          }, _this.options.keySearchTimeout);

          if ( val.length ) {
            // Search in select options
            $.each(_this.items, function(i, elm) {
              if ( RegExp('^' + _this.utils.escapeRegExp(val), 'i').test(elm.slug) && !elm.disabled ) {
                _this.select(i);
                return false;
              }
            });
          }
        });

      _this.$li.on({
        // Prevent <input> blur on Chrome
        mousedown: function(e) {
          e.preventDefault();
          e.stopPropagation();
        },
        click: function() {
          // The second parameter is to close the box after click
          _this.select($(this).data('index'), true);

          // Chrome doesn't close options box if select is wrapped with a label
          // We need to 'return false' to avoid that
          return false;
        }
      });
    },

    /**
     * Behavior when keyboard keys is pressed
     *
     * @param {object} e - Event object
     */
    handleKeys: function(e) {
      var _this = this;
      var key = e.keyCode || e.which;
      var keys = _this.options.keys;

      var isPrev = $.inArray(key, keys.previous) > -1;
      var isNext = $.inArray(key, keys.next) > -1;
      var isSelect = $.inArray(key, keys.select) > -1;
      var isOpen = $.inArray(key, keys.open) > -1;
      var idx = _this.state.selectedIdx;
      var isFirstOrLastItem = (isPrev && idx === 0) || (isNext && (idx + 1) === _this.items.length);
      var goToItem = 0;

      // Enter / Space
      if ( key === 13 || key === 32 ) {
        e.preventDefault();
      }

      // If it's a directional key
      if ( isPrev || isNext ) {
        if ( !_this.options.allowWrap && isFirstOrLastItem ) {
          return;
        }

        if ( isPrev ) {
          goToItem = _this.utils.previousEnabledItem(_this.items, idx);
        }

        if ( isNext ) {
          goToItem = _this.utils.nextEnabledItem(_this.items, idx);
        }

        _this.select(goToItem);
      }

      // Tab / Enter / ESC
      if ( isSelect && _this.state.opened ) {
        _this.select(idx, true);
        return;
      }

      // Space / Enter / Left / Up / Right / Down
      if ( isOpen && !_this.state.opened ) {
        _this.open();
      }
    },

    /** Update the items object */
    refresh: function() {
      var _this = this;

      _this.populate();
      _this.activate();
      _this.utils.triggerCallback('Refresh', _this);
    },

    /** Set options box width/height */
    setOptionsDimensions: function() {
      var _this = this;

      // Calculate options box height
      // Set a temporary class on the hidden parent of the element
      var hiddenChildren = _this.elements.items.closest(':visible').children(':hidden').addClass(_this.classes.tempshow);
      var maxHeight = _this.options.maxHeight;
      var itemsWidth = _this.elements.items.outerWidth();
      var wrapperWidth = _this.elements.wrapper.outerWidth() - (itemsWidth - _this.elements.items.width());

      // Set the dimensions, minimum is wrapper width, expand for long items if option is true
      if ( !_this.options.expandToItemText || wrapperWidth > itemsWidth ) {
        _this.finalWidth = wrapperWidth;
      } else {
        // Make sure the scrollbar width is included
        _this.elements.items.css('overflow', 'scroll');

        // Set a really long width for _this.elements.outerWrapper
        _this.elements.outerWrapper.width(9e4);
        _this.finalWidth = _this.elements.items.width();
        // Set scroll bar to auto
        _this.elements.items.css('overflow', '');
        _this.elements.outerWrapper.width('');
      }

      _this.elements.items.width(_this.finalWidth).height() > maxHeight && _this.elements.items.height(maxHeight);

      // Remove the temporary class
      hiddenChildren.removeClass(_this.classes.tempshow);
    },

    /** Detect if the options box is inside the window */
    isInViewport: function() {
      var _this = this;
      var scrollTop = $win.scrollTop();
      var winHeight = $win.height();
      var uiPosX = _this.elements.outerWrapper.offset().top;
      var uiHeight = _this.elements.outerWrapper.outerHeight();

      var fitsDown = (uiPosX + uiHeight + _this.itemsHeight) <= (scrollTop + winHeight);
      var fitsAbove = (uiPosX - _this.itemsHeight) > scrollTop;

      // If it does not fit below, only render it
      // above it fit's there.
      // It's acceptable that the user needs to
      // scroll the viewport to see the cut off UI
      var renderAbove = !fitsDown && fitsAbove;

      _this.elements.outerWrapper.toggleClass(_this.classes.above, renderAbove);
    },

    /**
     * Detect if currently selected option is visible and scroll the options box to show it
     *
     * @param {number} index - Index of the selected items
     */
    detectItemVisibility: function(index) {
      var _this = this;
      var liHeight = _this.$li.eq(index).outerHeight();
      var liTop = _this.$li[index].offsetTop;
      var itemsScrollTop = _this.elements.itemsScroll.scrollTop();
      var scrollT = liTop + liHeight * 2;

      _this.elements.itemsScroll.scrollTop(
        scrollT > itemsScrollTop + _this.itemsHeight ? scrollT - _this.itemsHeight :
          liTop - liHeight < itemsScrollTop ? liTop - liHeight :
            itemsScrollTop
      );
    },

    /**
     * Open the select options box
     *
     * @param {event} e - Event
     */
    open: function(e) {
      var _this = this;

      _this.utils.triggerCallback('BeforeOpen', _this);

      if ( e ) {
        e.preventDefault();
        e.stopPropagation();
      }

      if ( _this.state.enabled ) {
        _this.setOptionsDimensions();

        // Find any other opened instances of select and close it
        $('.' + _this.classes.hideselect, '.' + _this.classes.open).children()[pluginName]('close');

        _this.state.opened = true;
        _this.itemsHeight = _this.elements.items.outerHeight();
        _this.itemsInnerHeight = _this.elements.items.height();

        // Toggle options box visibility
        _this.elements.outerWrapper.addClass(_this.classes.open);

        // Give dummy input focus
        _this.elements.input.val('');
        if ( e && e.type !== 'focusin' ) {
          _this.elements.input.focus();
        }

        $doc
          .on('click' + bindSufix, $.proxy(_this.close, _this))
          .on('scroll' + bindSufix, $.proxy(_this.isInViewport, _this));
        _this.isInViewport();

        // Prevent window scroll when using mouse wheel inside items box
        if ( _this.options.preventWindowScroll ) {
          /* istanbul ignore next */
          $doc.on('mousewheel' + bindSufix + ' DOMMouseScroll' + bindSufix, '.' + _this.classes.scroll, function(e) {
            var orgEvent = e.originalEvent;
            var scrollTop = $(this).scrollTop();
            var deltaY = 0;

            if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1; }
            if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;  }
            if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY; }
            if ( 'deltaY'      in orgEvent ) { deltaY = orgEvent.deltaY * -1; }

            if ( scrollTop === (this.scrollHeight - _this.itemsInnerHeight) && deltaY < 0 || scrollTop === 0 && deltaY > 0 ) {
              e.preventDefault();
            }
          });
        }

        _this.detectItemVisibility(_this.state.selectedIdx);

        _this.utils.triggerCallback('Open', _this);
      }
    },

    /** Close the select options box */
    close: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeClose', _this);

      _this.change();

      // Remove custom events on document
      $doc.off(bindSufix);

      // Remove visible class to hide options box
      _this.elements.outerWrapper.removeClass(_this.classes.open);

      _this.state.opened = false;

      _this.utils.triggerCallback('Close', _this);
    },

    /** Select current option and change the label */
    change: function() {
      var _this = this;

      _this.utils.triggerCallback('BeforeChange', _this);

      if ( _this.state.currValue !== _this.state.selectedIdx ) {
        // Apply changed value to original select
        _this.$element
          .prop('selectedIndex', _this.state.currValue = _this.state.selectedIdx)
          .data('value', _this.lookupItems[_this.state.selectedIdx].text);

        // Change label text
        _this.setLabel();
      }

      _this.utils.triggerCallback('Change', _this);
    },

    /**
     * Select option
     *
     * @param {number}  index - Index of the option that will be selected
     * @param {boolean} close - Close the options box after selecting
     */
    select: function(index, close) {
      var _this = this;

      // Parameter index is required
      if ( index === undefined ) {
        return;
      }

      // If element is disabled, can't select it
      if ( !_this.lookupItems[index].disabled ) {
        _this.$li.filter('[data-index]')
          .removeClass('selected')
          .eq(_this.state.selectedIdx = index)
          .addClass('selected');

        _this.detectItemVisibility(index);

        // If 'close' is false (default), the options box won't close after
        // each selected item, this is necessary for keyboard navigation
        if ( close ) {
          _this.close();
        }
      }
    },

    /**
     * Unbind and remove
     *
     * @param {boolean} preserveData - Check if the data on the element should be removed too
     */
    destroy: function(preserveData) {
      var _this = this;

      if ( _this.state && _this.state.enabled ) {
        _this.elements.items.add(_this.elements.wrapper).add(_this.elements.input).remove();

        if ( !preserveData ) {
          _this.$element.removeData(pluginName).removeData('value');
        }

        _this.$element.prop('tabindex', _this.originalTabindex).off(bindSufix).off(_this.eventTriggers).unwrap().unwrap();

        _this.state.enabled = false;
      }
    }
  };

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[pluginName] = function(args) {
    return this.each(function() {
      var data = $.data(this, pluginName);

      if ( data && !data.disableOnMobile ) {
        (typeof args === 'string' && data[args]) ? data[args]() : data.init(args);
      } else {
        $.data(this, pluginName, new Selectric(this, args));
      }
    });
  };

  /**
   * Hooks for the callbacks
   *
   * @type {object}
   */
  $.fn[pluginName].hooks = {
    /**
     * @param {string} callbackName - The callback name.
     * @param {string}     hookName - The name of the hook to be attached.
     * @param {function}         fn - Callback function.
     */
    add: function(callbackName, hookName, fn) {
      if ( !this[callbackName] ) {
        this[callbackName] = {};
      }

      this[callbackName][hookName] = fn;
    },

    /**
     * @param {string} callbackName - The callback name.
     * @param {string}     hookName - The name of the hook that will be removed.
     */
    remove: function(callbackName, hookName) {
      delete this[callbackName][hookName];
    }
  };

  /**
   * Default plugin options
   *
   * @type {object}
   */
  $.fn[pluginName].defaults = {
    onChange             : function(elm) { $(elm).change(); },
    maxHeight            : 300,
    keySearchTimeout     : 500,
    arrowButtonMarkup    : '<b class="button">&#x25be;</b>',
    disableOnMobile      : true,
    openOnFocus          : true,
    openOnHover          : false,
    hoverIntentTimeout   : 500,
    expandToItemText     : false,
    responsive           : false,
    preventWindowScroll  : true,
    inheritOriginalWidth : false,
    allowWrap            : true,
    optionsItemBuilder   : '{text}', // function(itemData, element, index)
    labelBuilder         : '{text}', // function(currItem)
    keys                 : {
      previous : [37, 38],                 // Left / Up
      next     : [39, 40],                 // Right / Down
      select   : [9, 13, 27],              // Tab / Enter / Escape
      open     : [13, 32, 37, 38, 39, 40], // Enter / Space / Left / Up / Right / Down
      close    : [9, 27]                   // Tab / Escape
    },
    customClass          : {
      prefix: pluginName,
      camelCase: false
    }
  };
}));
var _pageShare;
var moduleApp = {
    'init': function () {
        moduleApp.pollifil();
        moduleApp.sliderSwiper();
        moduleApp.productSlider();
        moduleApp.formValidation();
        moduleApp.contacts();
        moduleApp.tabs();
        moduleApp.alternativeAccordion();
        moduleApp.accordion();
        moduleApp.customSelect();
        moduleApp.elementForms();
        moduleApp.mobileMenu();
        moduleApp.callMe();
        moduleApp.sendMessage();
        // moduleApp.sendEmail();
        moduleApp.parallaxTeam();
        moduleApp.resizeWindow();
        moduleApp.validSearch();
        moduleApp.brands();
        moduleApp.pageNews();
        moduleApp.subscribe();
        moduleApp.searchHeader();
        moduleApp.socialShare();
        moduleApp.loupeCursore();
        moduleApp.catalogPage();
        moduleApp.counterProduct();
        moduleApp.detailPage();
        moduleApp.shoppingCart();
        moduleApp.lkHistory();
        moduleApp.lkProfile();
        moduleApp.addFile();
        moduleApp.lkPersonal();
        moduleApp.newOrder();
        moduleApp.regionalOrder();
        moduleApp.startupMessage();
        moduleApp.basket();
        moduleApp.adaptiveTable();
    },
    'pollifil': function(){
        if (!("classList" in window.document.body)) {
            Object.defineProperty(Element.prototype, 'classList', {
                get: function() {
                    var self = this, bValue = self.className.split(" ")
                    bValue.add = function (){
                        var b;
                        for(var i in arguments){
                            b = true;
                            for (var j = 0; j<bValue.length;j++)
                                if (bValue[j] == arguments[i]){
                                    b = false;
                                    break
                                }
                            if(b)
                                self.className += (self.className?" ":"")+arguments[i]
                        }
                    };
                    bValue.remove = function(){
                        self.className = "";
                        for(var i in arguments)
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != arguments[i])
                                    self.className += (self.className?" " :"")+bValue[j]
                    };
                    bValue.toggle = function(x){
                        var b;
                        if(x){
                            self.className = "";
                            b = false;
                            for (var j = 0; j<bValue.length;j++)
                                if(bValue[j] != x){
                                    self.className += (self.className?" " :"")+bValue[j];
                                    b = false;
                                } else b = true;
                            if(!b)
                                self.className += (self.className?" ":"")+x
                        } else throw new TypeError("Failed to execute 'toggle': 1 argument required");
                        return !b;
                    };

                    return bValue;
                },
                enumerable: false
            });
        };

        (function() {
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                    || window[vendors[x]+'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                        timeToCall);
                    lastTime = currTime + timeToCall;
                    return id;
                };

            if (!window.cancelAnimationFrame)
                window.cancelAnimationFrame = function(id) {
                    clearTimeout(id);
                };
        }());
    },
    'popupOpen': function (content, style, beforeFunction, afterFunction, beforeClose, $subject) {
        $subject = $subject || $;
        content = content || '';
        style = style || 'fb-default-style';
        beforeFunction = beforeFunction || '';
        afterFunction = afterFunction || false;
        beforeClose = beforeClose || false;
        var configFancy = {
            content: content,
            wrapCSS: style,
            padding: 0,
            margin: 10,
            fitToView: false,
            openEffect: 'drop',
            closeEffect: 'drop',
            scrolling: 'auto',
            maxWidth: 1100,
            // maxHeight: 800,
            autoHeight: true,
            'beforeShow': function () {
                if (beforeFunction) {
                    beforeFunction();
                }
                hasPlaceholderSupport = function () {
                    var input = document.createElement('input');
                    return ('placeholder' in input);
                }
            },
            'afterShow': function () {
                $('.fancybox-wrap').addClass('fancybox-wrap-open');
                if (afterFunction) {
                    afterFunction();
                }
            },
            'beforeClose': function () {
                var $thisWrapper = $('.fancybox-wrap');
                if ($thisWrapper.hasClass('fancybox-wrap-close')) {
                    return true;
                } else {
                    if (beforeClose) {
                        beforeClose();
                    }
                    $thisWrapper.addClass('fancybox-wrap-close');
                    setTimeout(function () {
                        $.fancybox.close();
                    }, 300);
                    return false;
                }
            }
        };

        $subject.fancybox(configFancy);
    },
    'formValidation': function ($submitBtn, submitFunction) {
        submitFunction = submitFunction || false;
        $submitBtn = $submitBtn || $('.js-form-submit');
        var $submitForm = $submitBtn.closest('form');
        $submitForm.addClass('is-form-validation');
        var errorValidate = 'Поле обязательно для заполнения';
        var errorValidate2 = 'Поле заполнено не корректно';

        if(!device.mobile()){
            if(!device.tablet() || !device.android()){
                $submitForm.find('[data-mask="phone"]').mask("+7 (999) 999 99 99", {placeholder: "-"});
            }
            else{
                $submitForm.find('[data-mask="phone"]').attr('type','number');
            }
        }
        else{
            $submitForm.find('[data-mask="phone"]').attr('type','number');
        }

        $submitBtn.click(function (e) {
            e.preventDefault();

            var $this = $(this);
            var $thisForm = $this.closest('form');
            if ($this.hasClass('disabled')) {
                return false;
            }
            var $forms = $thisForm.find('[data-validate]');
            var result = formChecking($forms, true);
            if (result) {
                if (submitFunction) {
                    $this.addClass('disabled');
                    submitFunction($thisForm);
                }
                else{
                    setTimeout(function() {
                        $thisForm.submit();
                    }, 50);
                }
            }
            else {
                $forms.on('keyup keypress change', function () {
                    var $current = $(this);
                    setTimeout(function () {
                        formChecking($current, true);
                    }, 100);
                });
            }

            return false;
        });

        $(document).on('keydown', function (e) {
            return true;
            if (e.keyCode == 13) {
                var $thisFocus = $(document.activeElement);
                if ($thisFocus.is('textarea')) {
                    return true;
                }
                if ($thisFocus.closest('.form-select').length) {
                    return true;
                }
                if ($thisFocus.closest('.is-form-validation').length) {
                    $submitBtn.trigger('click');
                }
                return false;
            }
        });


        function formChecking($inp, onFocus) {

            onFocus = onFocus || false;
            var noError = true;

            $inp.each(function (ind, elm) {

                var $this = $(elm);
                var mask = $this.data('validate');
                var value = $this.val();
                var placeHolder = $this.attr('placeholder');

                if($this.is(':visible')){
                    if (mask == 'text') {
                        var regex = /^\s/;
                        if ((value.length < 1 || regex.test(value)) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'phone') {
                        if ((value.length < 1) || (value.indexOf('-') > -1)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'file') {
                        if (value.length < 2) {
                            noError = false;
                            $this.closest('.form-file').addClass('show-error');
                            if (onFocus) {
                                $this.focus();
                                onFocus = false;
                            }
                        } else {
                            $this.closest('.form-file').removeClass('show-error');
                        }
                    }

                    if (mask == 'textarea') {
                        var regex = /^\s/;
                        if ((value.length < 3 || regex.test(value)) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-textarea').addClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-textarea').removeClass('show-error');
                            $this.closest('.form-textarea').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'email') {
                        if(value != ''){
                            var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                            if (!regex.test(value) || (value == placeHolder)) {
                                noError = false;
                                $this.closest('.form-input').addClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideDown(200);
                                if (onFocus) {
                                    if(!device.ios()){
                                        $this.focus();
                                        onFocus = false;
                                    }
                                }
                            } else {
                                $this.closest('.form-input').removeClass('show-error');
                                $this.closest('.form-input').find('.form-item-error').slideUp(200);
                            }
                        }
                    }

                    if(mask == "email-required"){
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,5})$/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'filter-email') {
                        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (regex.test(value) || (value == '')) {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        } else {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                    }

                    if (mask == 'select') {
                        if (!value) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-select').addClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideDown(200);
                        } else {
                            $this.closest('.form-select').removeClass('show-error');
                            $this.closest('.form-select').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'checkbox') {
                        if (!$this.is(':checked')) {
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.closest('.form-checkbox').addClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        } else {
                            $this.closest('.form-checkbox').removeClass('show-error');
                            $this.closest('.form-checkbox').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'captcha') {
                        var response = grecaptcha.getResponse();
                        if (response.length == 0){
                            noError = false;
                            if (onFocus) {
                                onFocus = false;
                            }
                            $this.addClass('show-error');
                        } else {
                            $this.removeClass('show-error');
                        }
                    }

                    if (mask == 'serial-number'){
                        var regex = /[0-9]{9,}/;
                        if (!regex.test(value) || (value == placeHolder)) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if (mask == 'pass') {
                        if (value.length < 6) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'two-pass'){
                        var pass = $('.fancybox-inner .password').val();
                        if(value == '' || value != pass){
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        }
                        else{
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }

                    if(mask == 'date'){
                        if (value.length < 1) {
                            noError = false;
                            $this.closest('.form-input').addClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideDown(200);
                            if (onFocus) {
                                if(!device.ios()){
                                    $this.focus();
                                    onFocus = false;
                                }
                            }
                        } else {
                            $this.closest('.form-input').removeClass('show-error');
                            $this.closest('.form-input').find('.form-item-error').slideUp(200);
                        }
                    }
                }
            });

            setTimeout(function(){
                $.fancybox.update();
            },300);

            return noError;
        }
    },
    'sliderSwiper': function () {
        if($('.js-main-slider').length> 0){

            var configMain = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    pagination: '.swiper-pagination',
                    onSlideChangeStart: function(){
                    }
                },
                $mainSlider = $('.js-main-slider');

            if($('.js-main-slider .swiper-slide').length > 1){
                var $mainSwiper = $mainSlider.swiper(configMain);
            }
        }

        if($('.js-about-slider').length > 0){
            var slideActive = 0;
            var slidePosition = 0;
            var scroll = 0;

            var configAbout = {
                    slidesPerView: 4,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    simulateTouch: false,
                    onSlideNextStart: function(swiper){
                        var activeIndex = swiper.activeIndex;
                        if(activeIndex > slideActive){
                            $(swiper.slides[activeIndex]).find('.js-about-date').trigger('click')
                        }
                        slidePosition++;
                    },
                    onSlidePrevStart: function(swiper){
                        var activeIndex = swiper.activeIndex,
                            currentLimit = activeIndex + 3;

                        if(slideActive > currentLimit ){
                            $(swiper.slides[currentLimit]).find('.js-about-date').trigger('click')
                        }
                        slidePosition--;
                    },
                },
                $aboutSlider = $('.js-about-slider');

            var $aboutSwiper = $aboutSlider.swiper(configAbout);

            $('.js-about-date').on('click', function(){
                if(!$(this).hasClass('active')){
                    var $dates = $('.js-about-date'),
                        $this = $(this),
                        $slide = $this.closest('.swiper-slide');
                    var $content = $this.find('.content').clone();

                    $dates.removeClass('active');
                    $this.addClass('active');
                    slideActive = $slide.index();
                    $('.js-about-content .content').removeClass('active').addClass('deleted');
                    $content.appendTo('.js-about-content');
                    setTimeout(function(){
                        $('.js-about-content .deleted').remove();
                        $('.js-about-content .content').addClass('active');
                    },400);
                    //$this.find('.content').clone().appendTo('.js-about-content');
                }
            });
        }

        if($('.js-is-slider').length > 0){
            var configIsSlider = {
                    slidesPerView: 1,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 0,
                    autoHeight: true,
                    pagination: '.swiper-pagination',
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                },
                $isSlider = $('.js-is-slider');

            var $isSwiper = $isSlider.swiper(configIsSlider);            
        }

        if($('.js-detail-slider').length > 0){
            var configDetail = {
                    slidesPerView: 2,
                    centeredSlides: false,
                    paginationClickable: true,
                    spaceBetween: 20,
                    nextButton: '.detail-slider-button-next',
                    prevButton: '.detail-slider-button-prev',
                },
                $detailSlider = $('.js-detail-slider');

            var $detailSwiper = $detailSlider.swiper(configDetail);
        }

        if($('.js-prodaction-slider').length > 0){
            var configProdaction = {
                slidesPerView: 'auto',
                spaceBetween: 20,
                nextButton: '.product-swiper-next',
                prevButton: '.product-swiper-prev',
                },
                $prodactionSlider = $('.js-prodaction-slider');

            var productSwiper = $prodactionSlider.swiper(configProdaction);
        }

        if($('.js-office-slider').length > 0){
            var configOffice = {
                slidesPerView: 'auto',
                spaceBetween: 20,
                nextButton: '.office-swiper-next',
                prevButton: '.office-swiper-prev',
                },
                $officeSlider = $('.js-office-slider');

            var officeSwiper = $officeSlider.swiper(configOffice);
        }

        if($('.js-slider-news-profile').length > 0){
            var configNewsProfile = {
                    slidesPerView: 3,
                    centeredSlides: false,
                    paginationClickable: false,
                    spaceBetween: 0,
                    autoHeight: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    breakpoints: {
                        945:{
                            slidesPerView: 4,
                            spaceBetween: 0,
                        },
                        935:{
                            slidesPerView: 2,
                        },
                        640:{
                            slidesPerView: 1,
                            spaceBetween: 0,
                        }
                    }
                },
                $configNewsProfileSlider = $('.js-slider-news-profile');

            var $configNewsProfileSwiper = $configNewsProfileSlider.swiper(configNewsProfile);
        }
    },
    'productSlider': function(){
        if($('.js-product-slider').length > 0){
            var configProduct = {
                    slidesPerView: 4,
                    centeredSlides: false,
                    paginationClickable: false,
                    spaceBetween: 30,
                    updateTranslate: false,
                    breakpoints: {
                        945:{
                            slidesPerView: 4,
                            spaceBetween: 30,
                        },
                        935:{
                            slidesPerView: 2,
                        },
                        640:{
                            slidesPerView: 1,
                            spaceBetween: 20,
                        }
                    }
                },
                $productSlider = $('.js-product-slider:visible');

            $productSlider.each(function(ind,elt){
                var $this = $(this);
                configProduct.nextButton = $this.closest('.product-slider-wrapper').find('.swiper-button-next')[0];
                configProduct.prevButton = $this.closest('.product-slider-wrapper').find('.swiper-button-prev')[0];

                if($productSlider.find('.swiper-slide').length < 4) {
                    $this.find('.swiper-slide').addClass('swiper-slide-active');
                    $this.find('.swiper-button-prev').css('display', 'none');
                    $this.find('.swiper-button-next').css('display', 'none');
                }
                else{
                    var $productSwiper = new Swiper($this, configProduct);
                }
            });
        }
    },
    'productSlider2': function($wrapper){
        var configProduct = {
                slidesPerView: 4,
                centeredSlides: false,
                paginationClickable: false,
                spaceBetween: 30,
                updateTranslate: false,
                breakpoints: {
                    945:{
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                    935:{
                        slidesPerView: 2,
                    },
                    640:{
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            },
            $productSlider = $wrapper.find('.js-product-slider'),
            $productSwiper;

        configProduct.nextButton = $wrapper.find('.swiper-button-next');
        configProduct.prevButton = $wrapper.find('.swiper-button-prev');

        $productSwiper = new Swiper($productSlider, configProduct);
    },
    'contacts': function() {
        if($('.page-contacts').length){

            var $points = $('.js-map-item.active'),
                lat = $points.attr('data-lat'),
                long = $points.attr('data-long'),
                myMap,
                myGeoObject;

            ymaps.ready(function() {
                initMap(lat, long);
            });
        }

        function initMap(lat, long){
            myMap = new ymaps.Map('map', {
                    center: [lat, long],
                    zoom: 14,
                    controls: ['zoomControl']
            });

            constructMapPoints(lat, long);
        };

        function constructMapPoints(lat, long){
            if (myGeoObject) {
                myMap.geoObjects.remove(myGeoObject);
                myGeoObject = null;
            }

            myMap.setCenter([lat, long], 14);

            myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [lat, long]
                },
            }, {
                iconImageHref: '/dist/img/pin.png',
                iconLayout:  'default#image',
                iconImageSize: [58, 66],
            });

            myMap.geoObjects.add(myGeoObject);
        };

        $('.js-map-item').on('click', function(e){
            e.preventDefault();

            var $this = $(this);

            if(!$this.hasClass('active')){
                $('.js-map-item').removeClass('active');
                $this.addClass('active');

                lat = $this.attr('data-lat');
                long = $this.attr('data-long');

                constructMapPoints(lat, long);
            }
        });

        $('.js-connect-btn').on('click', function(){
            var template = $('.js-connect-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){
                setTimeout(function(){
                    var selectricElement = $('.fancybox-inner  .js-select').selectric();
                    selectricElement


                },300);

                if(device.ipad()) {
                    $('.fancybox-inner .js-select').on('mouseup', function () {
                        $(document).scrollTop(0);
                    });
                }

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){

                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);

                });
            });
        });

        $('.js-service-btn').on('click', function(){
            var template = $('.js-service-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        $('.js-training-btn').on('click', function(){
            var template = $('.js-training-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'alternativeAccordion': function() {
        $('.js-alternative-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.alternative-accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300,
                imageUrl = '',
                logoUrl = '';

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

                imageUrl = $this.attr('data-img');
                logoUrl = $this.attr('data-logo');

                $('.js-image-about').addClass('disabled');

                setTimeout(function(){
                    $('.js-border-img').attr('xlink:href', imageUrl);
                    $('.js-img-slide').attr('src', imageUrl);
                    $('.js-logo-slide').attr('src', logoUrl);

                    setTimeout(function(){
                        $('.js-image-about').removeClass('disabled');
                    },100);

                },300);
            }
        });
    },
    'tabs': function() {
        $(document).on('click', '.js-tabs-controls', function(e){
            e.preventDefault();

            var $item = $(this).closest('.tabs-controls-item'),
                $parent = $(this).closest('.js-tabs-wrapper'),
                $contentItem = $parent.find('.js-tabs-item'),
                itemNumber = $item.index();

            $contentItem.eq(itemNumber)
                .add($item)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });

        //табы для деталки со слайдерами
        $(document).on('click', '.js-tabs-controls-custom-slider', function(e){
            e.preventDefault();

            var $item = $(this).closest('.tabs-controls-item'),
                $parent = $(this).closest('.js-tabs-wrapper'),
                $contentItem = $parent.find('.js-tabs-item'),
                itemNumber = $item.index();

            if(!$(this).hasClass('active-slide')){

                setTimeout(function(){
                    moduleApp.productSlider2($contentItem.eq(itemNumber));
                }, 200);
                $(this).addClass('active-slide');
            }

            $contentItem.eq(itemNumber)
                .add($item)
                .addClass('active')
                .siblings()
                .removeClass('active');
        });

        if($(document).width() < 930){
            if(!$('.tabs-controls').hasClass('mobile')){
                $('.tabs-controls').each(function(ind,elem){
                    var $this = $(elem),
                        length = 0;

                    $this.css('width', '1000px');
                    $this.find('.tabs-controls-item').each(function(index,element){
                        length = length + $(element).outerWidth(true);
                    });

                    $this.addClass('mobile');
                    $this.css('width', length+'px');
                });
            }
        }
    },
    'accordion': function() {
        $('.js-accordion-trigger').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                item = $this.closest('.accordion-item'),
                list = $this.closest('.accordion-list'),
                items = list.find('.accordion-item'),
                content = item.find('.accordion-inner'),
                otherContent = list.find('.accordion-inner'),
                duration = 300;

            if (!item.hasClass('active')) {
                items.removeClass('active');
                item.addClass('active');

                otherContent.stop(true, true).slideUp(duration);
                content.stop(true, true).slideDown(duration);

            } else {
                content.stop(true, true).slideUp(duration);
                item.removeClass('active');
            }

        });
    },
    'customSelect': function(){
        if($('.js-select').is(':visible')){
            $('.js-select').selectric();
        }
    },
    'elementForms': function(){
        $(document).on('change', 'input', function(){
            if($(this).val().length > 0){
                $(this).closest('.form-input').addClass('filled');
                $(this).closest('.delivery-wrapper').addClass('filled');
            }
            else{
                $(this).closest('.form-input').removeClass('filled');
                $(this).closest('.delivery-wrapper').removeClass('filled');
            }
        });

        $(document).on('change', 'textarea', function(){
            if($(this).val().length > 0){
                $(this).parents('.form-textarea').addClass('filled');
            }
            else{
                $(this).parents('.form-textarea').removeClass('filled');
            }
        });
    },
    'mobileMenu': function(){
        $('.js-m-btn-menu').on('click', function(){
            $(this).toggleClass('active');
            $('.mobile-menu-wrapper').toggleClass('active');

            $('.site-body').toggleClass('open-menu');
        });

        $('.js-mm-trigger').on('click', function(){
            var $triggerElement = $('.js-mm-trigger'),
                $this = $(this),
                $parentTrigger = $this.parents('.mm-item'),
                $parentsTrigger = $('.js-mm-trigger').parents('.mm-item'),
                listItem = $parentTrigger.find('.mm-inner-item'),
                listsItem = $parentsTrigger.find('.mm-inner-item'),
                duration = 300;

            if($this.hasClass('active')){
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                listsItem.stop(true, true).slideUp(duration);
            }
            else{
                $triggerElement.removeClass('active');
                $parentsTrigger.removeClass('active');
                $(this).addClass('active');
                $parentTrigger.addClass('active');
                listsItem.stop(true, true).slideUp(duration);
                listItem.stop(true, true).slideDown(duration);
            }

        });

        $('.js-inner-menu-link').on('click', function(e){

            var $this = $(this);
            if(!$this.closest('.mobile-inner-menu-item').hasClass('open')){
                e.preventDefault();
                console.log('open');
                $('.mobile-inner-menu-item').removeClass('open');
                $this.closest('.mobile-inner-menu-item').addClass('open');
            }
            else {
                console.log('close');
                $this.closest('.mobile-inner-menu-item').removeClass('open');
            }
        });

        $('.js-close-third-menu').on('click', function(){
            $('.mobile-inner-menu-item').removeClass('open');
        });

        if($(document).width() < 940){
            moduleApp.innerMobileMenu();
            if($('.mobile-inner-menu-item.active').length > 0){
                $('.mobile-wrapper-inner-menu').animate({'scrollLeft': $('.mobile-wrapper-inner-menu').scrollLeft() + $('.mobile-inner-menu-item.active').offset().left - 30}, 1500);
            }
        }
    },
    'innerMobileMenu': function(){
        var countWidthMenu = 0;

        $('.mobile-inner-menu-item').each(function(ind,elem){
            var $elem = $(elem);
            countWidthMenu = countWidthMenu + $elem.outerWidth(true);
        });

        $('.mobile-inner-menu').css('width', countWidthMenu+'px');
    },
    'callMe': function(){
        $('.js-call-me').on('click', function(){
            var template = $('.js-call-me-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //$.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendMessage': function(){
        $('.js-send-message').on('click', function(){
            var template = $('.js-send-message-form').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){
                setTimeout(function(){
                    $('.fancybox-inner .js-select').selectric();
                },300);
                if(device.ipad()) {
                    $('.fancybox-inner .js-select').on('change', function () {
                        $(document).scrollTop(0);
                    });
                }

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    //setTimeout(function(){
                    //    $.fancybox.update();
                    //}, 600);

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });
    },
    'sendEmail': function(){
        $('.js-form-email').on('click', function(){
            var $this = $(this),
                $formContainer = $this.closest('form');

            moduleApp.popupOpen($formContainer, false, function(){
                var value = $formContainer.serializeArray(),
                    urlAjax = $formContainer.attr('action');

                moduleApp.ajaxSendForm(value,urlAjax);
            });
        });
    },
    'ajaxSendForm': function(value,urlAjax){
        $.ajax({
            url: urlAjax,
            data: value,
            type: 'POST',
            dataType: 'json',
            success: function (result) {
                console.log(result.popup_id);
                //console.log($('#'+result.popup_id));
                var resultMessage = $('#'+result.popup_id).html();
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            },
            error: function (error) {
                var resultMessage = '<div class="result-message"><h3>Ошибка</h3><p>'+error+'</p></div>';
                $.fancybox.close();
                setTimeout(function(){
                    moduleApp.popupOpen(resultMessage, false, function(){});
                },1000);
            }
        });
    },
    'parallaxTeam': function(){
        if($('.animate-layer-element').length){
            var scrolled = $(window).scrollTop();
            var countSrolled = (0-(scrolled*.15));
            $('.animate-layer-element img').each(function(ind,elem){
                //var transformZ = 'translateZ(0.000' + ind + 'px)';
                $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
            });

            $(window).bind('scroll',function(){
                moduleApp.parallaxScroll();
            });
        }

        // $('.team-production-sleder').addClass('active');

        // $('.animation-slide-element').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        //     if(visiblePartY == 'both' || visiblePartY == 'bottom') {
        //         $('.animation-slide-element').addClass('active');
        //     }
        // });

        $('.swiper-container').on('inview', function (event, isInView, visiblePartX, visiblePartY) {

            if(isInView){
                $(this).find('.animation-slide-element').addClass('active');
            }
        });
    },
    'parallaxScroll': function(){
        var scrolled = $(window).scrollTop();
        var countSrolled = (0-(scrolled*.15));
        $('.animate-layer-element img').each(function(ind,elem){
            $(elem).css('transform','translate3d(0,' + countSrolled + 'px,0)');
        });
    },
    'resizeWindow': function(){
        $(window).resize(function(){
            var widthWindow = $(document).width();

            if(widthWindow < 930){
                if(!$('.tabs-controls').hasClass('mobile')){
                    $('.tabs-controls').each(function(ind,elem){
                        var $this = $(elem);
                        var length = 0;
                        $this.find('.tabs-controls-item').each(function(index,element){
                            length = length + $(element).outerWidth(true);
                        });

                        $this.addClass('mobile');
                        $this.css('width', length+'px');
                    });
                }

                moduleApp.innerMobileMenu();

                if(!$('.adaptive-table').parent('.container-scroll-table')){
                    $('.adaptive-table').wrap('<div class="wrapper-scroll-table"><div class="container-scroll-table"></div> </div>')
                }
            }
            if(widthWindow > 930){
                if($('.tabs-controls').hasClass('mobile')) {
                    $('.tabs-controls').each(function (ind, elem) {
                        var $this = $(elem);
                        $this.css('width', '100%');
                        $this.removeClass('mobile');
                    });
                }
            }

        });
    },
    'validSearch': function(){
        $('.js-search-btn').on('click', function(e){
            var $this = $(this);
            var $input = $this.siblings('.js-search-input');
            if($input.val().length < 3){
                e.preventDefault(e);
            }
        })
    },
    'brands': function(){
        $('.js-brand-item').on('click', function(){
            if($(this).hasClass('active')){
                $('.js-brand-item').removeClass('active');
            }
            else{
                $('.js-brand-item').removeClass('active');
                $(this).addClass('active');
            }
        });
    },
    'pageNews': function(){

        var stopFlagNews = true,
            filterValue = '',
            lang = $('.site_lang').html(),
            ie9 = false

        if($('html').hasClass('ie9')){
            ie9 = true;
        }

        if($('.ajax-wrapper-news').length > 0){

            if($('.actual-news-wrapper').length == 0){
                var $activeCard = $('.item-news-card.active'),
                    id = $activeCard.find('.js-name-news').attr('data-id'),
                    link = $activeCard.find('.js-name-news').attr('data-link'),
                    nextNews = $activeCard.find('.js-name-news').attr('data-next'),
                    prevNews = $activeCard.find('.js-name-news').attr('data-prev');

                sendNews(id,link,nextNews,prevNews,filterValue);
            }
        }

        $('.mCustomScrollbar-list-news').mCustomScrollbar({
            axis:"y",
            theme:"dark",
            callbacks:{
                onTotalScroll: function(){
                    if($('.js-info-news-list').length > 0){

                        stopFlagNews = false;

                        var page = $('.js-info-news-list').attr('data-page');
                        filterValue = $('.js-search-news-text').val();

                        $('.js-info-news-list').remove();

                        $.ajax({
                            url: '/ajax/news-list.php',
                            data: {
                                PAGEN_1: page,
                                filter: filterValue
                            },
                            type: 'POST',
                            dataType: 'html',
                            success: function (result) {
                                addListNews(result);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }
                }
            }
        });

        $('.js-search-news-btn').on('click', function(e){
            e.preventDefault();

            //if($('.js-search-news-text').val().length > 3){
                var value = $('.js-search-news-text').val(),
                    lang = $('.site_lang').html();
                $.ajax({
                    url: '/ajax/filter-news.php',
                    data: {
                        value: value,
                        site_lang: lang
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function (result) {
                        addNewNews(result.actualNews);
                        changeListNews(result.listNews);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            //}
        });

        $(document).on('click', '.js-name-news', function(e){
            if(!ie9){
                e.preventDefault();

                $('.item-news-card').removeClass('active');
                $(this).closest('.item-news-card').addClass('active');
                var id = $(this).attr('data-id'),
                    link = $(this).attr('data-link'),
                    nextNews = $(this).attr('data-next'),
                    prevNews = $(this).attr('data-prev');

                filterValue = $('.js-search-news-text').val();

                sendNews(id, link, nextNews, prevNews, filterValue);
            }
        });

        $(document).on('click', '.js-next-news', function(e){
            e.preventDefault();
            var $listNews = $('.list-news'),
                indexActiveElement = $listNews.find('.active').index() + 1;
                
            $listNews.find('.active').removeClass('active');
            if(indexActiveElement > $listNews.find('.item-news-card').length - 1){
                var page = $('.js-info-news-list').attr('data-page');
                filterValue = $('.js-search-news-text').val();

                $('.js-info-news-list').remove();

                $.ajax({
                    url: '/ajax/news-list.php',
                    data: {
                        PAGEN_1: page,
                        filter: filterValue
                    },
                    type: 'POST',
                    dataType: 'html',
                    success: function (result) {
                        addListNews(result);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            setTimeout(function(){
                $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');
            }, 400);


            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');

            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);

            setTimeout(function(){
                $('html,body').animate({'scrollTop': $('.ajax-wrapper-news').offset().top}, 500);
            },700);

        });

        $(document).on('click', '.js-prev-news', function(e){
            e.preventDefault();

            var $listNews = $('.list-news');
            var indexActiveElement = $listNews.find('.active').index() - 1;
            $listNews.find('.active').removeClass('active');
            $listNews.find('.item-news-card').eq(indexActiveElement).addClass('active');

            var id = $(this).attr('data-id'),
                link = $(this).attr('data-link'),
                nextNews = $(this).attr('data-next'),
                prevNews = $(this).attr('data-prev');
            filterValue = $('.js-search-news-text').val();
            sendNews(id, link, nextNews, prevNews, filterValue);
        });

        $(document).on('click', '.tag-list span', function(e){
            e.preventDefault();

            var value = $(this).html();
                lang = $('.site_lang').text();

            $.ajax({
                url: '/ajax/filter-news.php',
                data: {
                    value: value,
                    site_lang: lang
                },
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    addNewNews(result.actualNews);
                    changeListNews(result.listNews);
                    $('.js-search-news-text').val(value);
                    $('.wrapper-search-news').find('.form-input-search').addClass('filled');
                },
                error: function (error) {
                    console.log(error);
                }
            });

        });

        function sendNews(id, link, nextNews, prevNews, filterValue){
            var lang = $('.site_lang').html();
            $.ajax({
                url: '/ajax/news.php',
                 data: {
                     id: id,
                     link: link,
                     nextNews: nextNews,
                     prevNews: prevNews,
                     filter: filterValue,
                     site_lang: lang,
                 },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    _pageShare = {
                        'link': result._pageShare.link,
                        'title': result._pageShare.title,
                        'description': result._pageShare.description,
                        'twitter_description': result._pageShare.twitter_description,
                        'image': result._pageShare.image
                    };
                    addNewNews(result.actualNews, link);
                    changeMetaTeg();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        };

        function addNewNews(resultNews, linkNews){
            $('.actual-news-wrapper').addClass('deleted');
            $('.ajax-wrapper-news').append(resultNews);
            if(linkNews != ''){
                changeUrlNews(linkNews);
            }
            setTimeout(function() {
                $('.actual-news-wrapper.deleted').remove();
                $('.actual-news-wrapper.desabled').removeClass('desabled');
                updateScrollbar();
            }, 500);
        };

        function addListNews(newsListAdd){
            $('.list-news').append(newsListAdd);
            stopFlagNews = true;
        }

        function changeListNews(resultListNews){
            $('.list-news').addClass('deleted');
            $('.wrapper-list-news .mCSB_container').append(resultListNews);
            setTimeout(function() {
                $('.list-news.deleted').remove();
                $('.list-news.desabled').removeClass('desabled');
                updateScrollbar();
            }, 700);
        };

        function updateScrollbar(){
            setTimeout(function() {
                $('.mCustomScrollbar ').mCustomScrollbar("update");
                $('.mCustomScrollbar-list-news').mCustomScrollbar("update");
            }, 300);
        };

        function changeMetaTeg(){
            $('#og_url').attr('content', _pageShare.link);
            $('#og_title').attr('content', _pageShare.title);
            $('#og_description').attr('content', _pageShare.description);
            $('#og_image').attr('content', _pageShare.image);
        };

        function changeUrlNews(linkNews){
            if($('.page-news').length > 0 && !$('html').hasClass('ie9')){
                history.pushState('', '', linkNews);
            }
        }
    },
    'startupMessage':function(){
        var title = '';
        if (appConfig.startupMessage.title && appConfig.startupMessage.message) {
            var template = '<div class="fb-popup-default">';
            template += '<div class="fbp-title">'+appConfig.startupMessage.title+'</div>';
            template += '<div class="fbp-message">'+appConfig.startupMessage.message+'</div>';
            template += '</div>';

            $.fancybox({
                content: template,
                wrapCSS: 'message-wrapper-popUp',
                padding: [0],
                fitToView: false,
                openEffect: 'elastic',
                closeEffect: 'elastic',
                maxWidth: 500,
                maxHeight: 380,
            });
        }
    },
    'subscribe': function(){
        moduleApp.formValidation($('.js-form-subscribe'), function(e){
            console.log(e);
            var $this = $(e),
                url = $this.attr('action'),
                value = $this.serializeArray();
            console.log(value);
            $this.find('.delivery-input').val('');
            $('.filled').removeClass('filled');
            moduleApp.ajaxSendForm(value,url);
            $this.find('.js-form-subscribe').removeClass('disabled');
        });

        $('.delivery-input').focusout(function(){
            $(this).closest('.form-input').removeClass('show-error');
        });
    },
    'searchHeader': function(){
        $('.js-search-header-btn').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $thisForm = $this.closest('form');

            if(!$this.hasClass('active')){
                $this.addClass('active');
                $this.closest('form').addClass('activated');
            }
            else{

                if($thisForm.find('.search-header-text').val().length > 3){
                    $thisForm.submit();
                }
                else{
                    $thisForm.find('.search-header-text').val('');
                    $this.removeClass('active');
                    $this.closest('form').removeClass('activated');
                }
            }
        });
    },
    'socialShare': function(){
        $(document).on('click', '[data-service]', function(e){
            e.preventDefault();

            var $this = $(this),
                shareService = $this.attr('data-service'),
                windowLink = '';

            windowLink += 'http://share.yandex.ru/go.xml?service=' + shareService;
            windowLink += '&title=' + _pageShare.title;

            if (shareService=='twitter') {
                windowLink += ' ' + _pageShare.twitter_description;
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
            }
            else if (shareService=='livejournal'){
                windowLink = 'http://www.livejournal.com/update.bml?';
                windowLink += 'subject=' + _pageShare.title;
                windowLink += '&event=' + _pageShare.description + ' <a href="' + _pageShare.link + '">' + _pageShare.link + '</a>';
            } else {
                windowLink += '&url=' + _pageShare.link;
                windowLink += '&link=' + _pageShare.link;
                windowLink += '&description=' + _pageShare.description;
                windowLink += '&image=' + _pageShare.image;
            }

            window.open(windowLink,'','toolbar=0,status=0,width=625,height=435');

        });
    },
    'loupeCursore': function(){
        (function ($) {
            $.fn.loupe = function (arg) {
                var options = $.extend({
                    loupe: 'loupe',
                    width: 200,
                    height: 150
                }, arg || {});

                return this.length ? this.each(function () {
                    var $this = $(this), $big, $loupe,
                        $small = $this.is('img') ? $this : $this.find('img:first'),
                        move, hide = function () { $loupe.hide(); },
                        time;

                    if ($this.data('loupe') != null) {
                        return $this.data('loupe', arg);
                    }

                    move = function (e) {
                        var os = $small.offset(),
                            sW = $small.outerWidth(),
                            sH = $small.outerHeight(),
                            oW = options.width / 2,
                            oH = options.height / 2;

                        if (!$this.data('loupe') ||
                            e.pageX > sW + os.left + 10 || e.pageX < os.left - 10 ||
                            e.pageY > sH + os.top + 10 || e.pageY < os.top - 10) {
                            return hide();
                        }

                        time = time ? clearTimeout(time) : 0;

                        $loupe.show().css({
                            left: e.pageX - oW,
                            top: e.pageY - oH
                        });
                        $big.css({
                            left: -(((e.pageX - os.left) / sW) * $big.width() - oW)|0,
                            top: -(((e.pageY - os.top) / sH) * $big.height() - oH)|0
                        });
                    };

                    $loupe = $('<div />')
                        .addClass(options.loupe)
                        .css({
                            width: options.width,
                            height: options.height,
                            position: 'absolute',
                            overflow: 'hidden'
                        })
                        .append($big = $('<img />').attr('src', $this.attr($this.is('img') ? 'src' : 'href')).css('position', 'absolute'))
                        .mousemove(move)
                        .hide()
                        .appendTo('body');

                    $this.data('loupe', true)
                        .mouseenter(move)
                        .mouseout(function () {
                            time = setTimeout(hide, 10);
                        });
                }) : this;
            };
        }(jQuery));
    },
    'catalogPage': function(){
        var loupeConfig = {
            width: 260,
            height: 260,
            loupe:'quick-view-loupe'
        },
            ajax_request = "";

        if($('.js-checkbox-filter:checked').length > 0){
            createElementsFilter();
            serializeArray();
        }

        $(document).on('click', '.js-quick-view', function(e){
            e.preventDefault();
            var template = $(this).find('.js-quick-view-block').html();

            var configFancy = {
                content: template,
                wrapCSS: 'fb-card-window',
                padding: 0,
                margin: 10,
                fitToView: false,
                openEffect: 'drop',
                closeEffect: 'drop',
                scrolling: 'auto',
                maxWidth: 1100,
                autoWidth: true,
                autoHeight: true,
                'beforeShow': function () {
                    $('.fancybox-inner .js-loupe-container img').loupe(loupeConfig);
                },
            }

            $.fancybox(configFancy);
        });

        $('.js-filter-element').on('change', function(){
            serializeArray();
        });

        $('.js-sort-element').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $thisInput = $this.find('input');

            if($this.hasClass('up') || $this.hasClass('down')){

                if($this.hasClass('down')){
                    $this.removeClass('down').addClass('up');
                    $thisInput.attr('value','asc');
                }
                else{
                    $this.removeClass('up').addClass('down');
                    $thisInput.attr('value','desc');
                }
            }
            else{
                $('.js-sort-element').each(function(ind,elt){
                    $(elt).find('input').attr('value','off');
                    $(elt).removeClass('up down')
                });
                $this.addClass('up');
                $thisInput.attr('value','asc');
            }
            setTimeout(function(){
                serializeArray();
            },100);
        });

        $(document).on('click', '.js-sf-delete', function(){
            var $this = $(this);
            deleteElementFilter($this);
        });

        $('.js-checkbox-filter').on('change', function(){
            if($(this).is(':checked')){
                createElementFilter($(this).attr('data-name'),$(this).attr('id'));
            }
            else{
                var idElement = $(this).attr('id'),
                    $element = $('.catalog-sf-item').find('.js-sf-delete').filter(function(){
                        return $(this).attr('data-id') == idElement;
                    });
                deleteElementFilter($element);
            }
        });

        $('.js-sort-clear-filter').on('click', function(){
            $('input[type=checkbox]').prop("checked", false);
            $('.catalog-sub-filter-wrapper').addClass('disabled');
            setTimeout(function(){
                $('.catalog-sub-filter-wrapper').html('');
                $('.catalog-sub-filter-wrapper').removeClass('disabled');
                serializeArray();
            },300);
        });

        $('.js-catalog-filter-trigger').on('click', function(){
            var $triggerElement = $(this),
                $parentTrigger = $triggerElement.parents('.catalog-fl'),
                listItem = $parentTrigger.find('.catalog-fl-inner'),
                duration = 300;

            if($triggerElement.hasClass('active')){
                $triggerElement.removeClass('active');
                listItem.stop(true, true).slideUp(duration);
            }
            else{
                $triggerElement.addClass('active');
                listItem.stop(true, true).slideDown(duration);
            }
        });

        //открытие и закрытие мобильного фильтра
        $(document).on('touchstart', function(e){
            $(".filter-global-list").removeClass("active");
        });
        //открытие и закрытие мобильного фильтра
        $(".filter-global-list").on('touchstart', function(e){
            e.stopPropagation();
        });
        //открытие и закрытие мобильного фильтра
        $('.js-mm-trigger-filter').on('touchstart', function(e){
            e.preventDefault();
            e.stopPropagation();
            $('.filter-global-list').toggleClass('active');
            // alert('filter4');
        });

        //пагинация
        $(document).on('click', '.js-ajax-product-pagination', function(e){
            e.preventDefault();
            var  value = $('.js-catalog-global-wrapper').serializeArray(),
                pagin = $(this).attr('data-pagen_1');

            sendAjax(value,pagin);
        });

        function serializeArray(){
            value = $('.js-catalog-global-wrapper').serializeArray();
            //console.log(value);
            sendAjax(value);
        };

        function sendAjax(value,pagin){
            if (ajax_request) {
                ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/catalog-global-filter.php',
                data: {
                    q: value,
                    PAGEN_1: pagin
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    bodyAddClass();
                    scrollTop();
                    deletedPagination();
                    deletedListElement();
                    setTimeout(function(){
                        addNewListElement(result.poducts);
                    },300);
                    setTimeout(function(){
                        addNewPagination(result.paginator);
                    },800);
                },
                error: function (error) {
                }
            });
        };

        function createElementFilter(name,id){
            $('.catalog-sub-filter-wrapper').append('<div class="catalog-sf-item"><span class="catalog-sf-title">'+name+'</span><div class="catalog-sf-delete js-sf-delete" data-id="'+id+'"></div></div>')
        };

        function createElementsFilter(){
            $('.js-checkbox-filter:checked').each(function(ind,elt){
                var name = $(elt).attr('data-name'),
                    id = $(elt).attr('id');
                $('.catalog-sub-filter-wrapper').append('<div class="catalog-sf-item"><span class="catalog-sf-title">'+name+'</span><div class="catalog-sf-delete js-sf-delete" data-id="'+id+'"></div></div>')
            });
        };

        function deleteElementFilter($element){
            var $wrapper = $element.closest('.catalog-sf-item'),
                name = $element.attr('data-id');

            $wrapper.addClass('deleted');
            $('.catalog-filter-list-wrapper').find('#'+name).removeAttr("checked").prop("checked", false);
            setTimeout(function(){
                $wrapper.remove()
            },300);
            serializeArray();
        };

        function addNewListElement(ListElement){
            $('.catalog-content-wrapper').append(ListElement);
            setTimeout(function(){
                $('.catalog-cards-list.append').removeClass('append');
            },300);
        };

        function deletedListElement(){
            $('.catalog-content-wrapper .catalog-cards-list').addClass('deleted');
            setTimeout(function(){
                $('.catalog-cards-list.deleted').remove();
            },300);
        };

        function scrollTop(){
            $('html,body').animate({'scrollTop': $('.catalog-main-block-wrapper').offset().top - 200}, 400);
        };

        function addNewPagination(newPagination){
            $('.catalog-main-block-wrapper').append(newPagination);
            setTimeout(function() {
                $('.pagination-wrapper.append').removeClass('append');
            },300);
        };

        function deletedPagination(){
            $('.catalog-main-block-wrapper .pagination-wrapper').addClass('deleted');
            setTimeout(function(){
                $('.pagination-wrapper.deleted').remove();
            },300);
        };

        function bodyAddClass(){
            $('body').addClass('overflow-catalog');
            setTimeout(function(){
                $('body').removeClass('overflow-catalog');
            },400);
        };

        if(device.ipad()){
            $('html').addClass('ipad-tablet');
        }
        else{
            $('html').removeClass('ipad-tablet');
        }
    },
    'counterProduct': function(){
        $(document).on('click', '.js-card-counter-plus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-card-counter-value');


            if(parseInt($input.val()) < $this.attr('data-max')){
                $input.val(parseInt($input.val()) + 1);
            }

            if(parseInt($input.val()) == $this.attr('data-max')){
                $this.addClass('disabled');
            }

            if(parseInt($input.val()) > 1){
                $this.siblings('.js-card-counter-minus').removeClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('click', '.js-card-counter-minus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-card-counter-value');

            if(!$this.hasClass('disabled')){
                $input.val(parseInt($input.val()) - 1);

                if(parseInt($input.val()) < $this.siblings('.js-card-counter-plus').attr('data-max')){
                    $this.siblings('.js-card-counter-plus').removeClass('disabled');
                }
            }

            if(parseInt($input.val()) == 1){
                $this.addClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('change', '.js-card-counter-value', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(parseInt($this.val()) > parseInt($this.siblings('.js-card-counter-plus').attr('data-max'))){
                $this.val($this.siblings('.js-card-counter-plus').attr('data-max'));
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($this.val()));
            }
        });
    },
    'detailPage': function(){
        var loupeConfig = {
            width: 260,
            height: 260,
            loupe:'quick-view-loupe-detail'
        };

        //лупа на деталке
        if(!device.mobile()){
            $('.js-loop-detail img').loupe(loupeConfig);
        }

        //переключалка изображений на деталке
        $('.js-detail-main-slide').on('click', function(){
            var $this = $(this),
                urlMediumImage = $this.attr('data-mediumImg');
                //urlBigImage = $this.attr('data-bigImg');
            if(!$this.hasClass('active')){
                //$('.big-detail-image').attr('data-bigImg', urlBigImage);
                $('.big-detail-image img').addClass('deleted');
                $('.js-detail-main-slide').removeClass('active');
                $this.addClass('active')
                setTimeout(function(){
                    $('.big-detail-image .deleted').remove();
                    $('.big-detail-image').append('<img src="'+urlMediumImage+'" class="append">');
                    setTimeout(function(){
                        $('.big-detail-image .append').removeClass('append');
                        if(!device.mobile()) {
                            $('.js-loop-detail img').loupe(loupeConfig);
                        }
                    },300);
                },300);

                if($('.quick-view-loupe').length > 0){
                    $('.quick-view-loupe').remove();
                }
            }
        });

        //$('.js-fancy-image').on('click', function(){
        //   console.log('click');
        //
        //    var template = '<div class="loop-image-detail"><img src="'+$(this).attr("data-bigImg")+'"></div>';
        //    moduleApp.popupOpen(template, 'fb-noscroll-window');
        //});

        //выставление рейтинга в форме
        $(document).on('click', '.fancybox-inner .form-rating-item', function(){
            var $this = $(this),
                $stars = $this.siblings(),
                count = $stars.length + 1,
                index = $this.index();

            $stars.removeClass('active');
            $this.addClass('active');
            for(var i = 0; i < count; i++){
                if(index <= i ){
                    $($stars[i]).addClass('active');
                }
            }

            var activeCount = $('.fancybox-inner .js-rating .active').length;
            $('.fancybox-inner .js-rating-count').val(activeCount)
        });

        //оставить отзыв
        $('.js-reviews').on('click', function(){
            var template = $('.js-review-form').html();
            moduleApp.popupOpen(template,'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        //полезные ли комментарии
        $('.js-review-help').on('click', function(){
            if($(this).attr('data-checked') != 'checked'){
                var $this = $(this),
                    idProduct = $this.attr('data-id'),
                    typeReview = $this.attr('data-type'),
                    countReview = parseInt($this.attr('data-count'));

                countReview++;
                $this.attr('data-count',countReview);
                $this.find('span').html('('+countReview+')');
                $this.attr('data-checked','checked');
                $this.siblings('.js-review-help').attr('data-checked','checked');

                $.ajax({
                    url:'/ajax/comments/comment-like.php',
                    data:{
                        id: idProduct,
                        type: typeReview
                    },
                    type: 'POST',
                    dataType:'JSON',
                    success: function(result){
                        console.log(result);
                    }
                });
            }
        });

        //ajax пагинация
        $(document).on('click', '.js-ajax-comment-pagination', function(e){
            e.preventDefault();

            var $this = $(this),
                idProduct = $this.attr('data-product_id'),
                pagenPage = $this.attr('data-pagen_1');

            $.ajax({
                url: '/ajax/comments/comments-pagination.php',
                data:{
                    product_id:idProduct,
                    PAGEN_1: pagenPage
                },
                type: 'POST',
                dataType:'JSON',
                success:function(result){

                    console.log(result);
                    $('.reviews-list').addClass('deleted');
                    $('.reviews-list-wrapper').append(result.comments);
                    setTimeout(function(){
                        $('.reviews-list.deleted').remove();
                        $('.reviews-list.active').removeClass('active');
                        $('.reviews-right-column .pagination-wrapper').addClass('deleted');
                    },300);
                    setTimeout(function(){
                        $('html,body').animate({'scrollTop': $('.reviews-list-wrapper').offset().top - ($(window).outerHeight()/3)}, 500);
                        $('.reviews-right-column .pagination-wrapper').remove();
                        $('.reviews-right-column ').append(result.paginator);
                        setTimeout(function(){
                            $('.reviews-right-column .pagination-wrapper').removeClass('append');
                        },200);
                    },700);
                }
            });
        });

        //добавление в избранное
        $('.js-liked').on('click', function(){

            $(this).toggleClass('active');
            var idProduct = $(this).attr('data-product_id');

            $.ajax({
                url:'/ajax/store/add_to_favorites.php',
                data:{
                    id: idProduct
                },
                type: 'POST',
                dataType:'JSON',
                success:function(result){
                    console.log(result);
                }
            });
        });

        //социальное меню для адаптивки
        $('.js-social-btn').on('click', function(e){
            e.preventDefault();
            $(this).closest('.social-menu').toggleClass('open');
        });
    },
    'shoppingCart': function(){
        $(document).on('click', '.js-add-cart-product', function(){
            var $this = $(this);
                //idProduct = $this.attr('data-id'),
                //countProduct = $this.siblings('.js-card-counter-value');
            addProductCart($this);
            clearCounterProduct($this);
        });

        //добавление в корзину
        function addProductCart($product){
            var idProduct = $product.attr('data-id'),
                $cardBuy = $product.closest('.js-card-buy'),
                countProduct = $cardBuy.find('.js-card-counter-value').val(),
                stateProduct = 'add';
            sendAjaxCart(idProduct, countProduct, stateProduct, $product);
        };

        //удаление из корзины
        function removeProductCart($product){
            var idProduct = $product.attr('data-id'),
                countProduct = 0,
                stateProduct = 'remove';
            sendAjaxCart(idProduct, countProduct, stateProduct);
        };

        //изменение малой корзины на странице
        function changeSmollChoppingCart(template){
            console.log(template);
        };

        //отправка аякса в корзину
        function sendAjaxCart(idProduct, countProduct, stateProduct, $product){
            console.log(idProduct);
            console.log(countProduct);
            console.log(stateProduct);
            $.ajax({
                url:'/ajax/store/add_to_cart.php',
                data:{
                    PRODUCT_ID: idProduct,
                    QUANTITY: countProduct,
                    state: stateProduct
                },
                type: 'POST',
                dataType:'json',
                success: function(result){
                    console.log(result);
                    animationBasket($product);
                }
            });
        };

        //сбрасываем значение счетчика и цену товара
        function clearCounterProduct($product){
            var $cardBuy = $product.closest('.js-card-buy'),
                price = $cardBuy.find('.js-price-count').attr('data-price');

            $cardBuy.find('.js-price-count span').html(price);
            $cardBuy.find('.js-card-counter-value').val('1');
        };

        function animationBasket($product){
            console.log('animation');
            var imageUrl = $product.attr('data-img');
            var leftPos = 0,
                topPos = 0,
                leftBasketPos = $('.shop-basket').offset().left,
                topBasketPos = $('.shop-basket').offset().top;
            if($('.page-detail-tea').length > 0){
                topPos = $('.detail-slider-wrapper').offset().top;
                leftPos = $('.detail-slider-wrapper').offset().left;
            }

            if($('.page-catalog-tea').length > 0){
                topPos = $product.offset().top - 10;
                leftPos = $product.offset().left + ($product.width()/2);
            }

            $('body').append('<div class="animation-product" style="left: '+leftPos+'px; top: '+topPos+'px;" ><img src="'+imageUrl+'" alt=""></div> ');

            setTimeout(function(){
                $('.animation-product').addClass('animation');
                $('.animation-product').css({left:leftBasketPos, top:topBasketPos});
            },100);
            setTimeout(function(){
                // $('.animation-product').remove();
            },450);
        };
    },
    'lkHistory': function(){
        $(document).on('click', '.js-info-order', function(){
            var template = $(this).siblings('.info-ordewr-container').html();
            moduleApp.popupOpen(template, 'fb-noscroll-window');
        });

        $('.js-calculation').on('click', function(){

            var user = $(this).attr('data-user'),
                urlAjax = $(this).attr('data-url');

            $.ajax({
                url: urlAjax,
                data: {user:user},
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    moduleApp.popupOpen($('#'+result.popup_id));
                },
                error: function (error) {
                    console.log('error' + error);
                }
            });
        });

        $(document).on('change', '.js-add-print-row', function(){            

            var tempalte = '',
                $printWrapper = $('.print-table-wrapper');

            $('.js-add-print-row:checked').closest('tr').each(function(ind,elt){
                console.log($(elt));
                tempalte = tempalte + '<tr>' +$(elt).html() + '</tr>';
            });

            $printWrapper.find('tbody').empty();
            $printWrapper.find('tbody').append(tempalte);

            if($('.js-add-print-row:checked').length < 1){
                $printWrapper.find('tbody').append($('.history-table.ht tbody').html());
            }

            $printWrapper.find('tbody tr').each(function(ind,elt){
                $(elt).find('td:last').empty();
            });


        });

        $(document).on('click', '.js-period-print', function(){
            window.print();
        });
    },
    'lkProfile': function(){
        $('.js-write-manager').on('click', function(){
            var template = $('.js-manager-container').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function(){
                    $.fancybox.update();

                    var $thisForm = $('.fancybox-inner form'),
                        value = $thisForm.serializeArray(),
                        urlAjax = $thisForm.attr('action');

                    moduleApp.ajaxSendForm(value,urlAjax);
                });
            });
        });

        $('.js-period-filter').on('change', function(){
            console.log('change');
            var $thisForm = $(this),
                value = $thisForm.serializeArray(),
                urlAjax = $thisForm.attr('action');
            // console.log(value);
            // console.log(urlAjax);
            ajaxSendFilter(value,urlAjax);
        });

        $.datetimepicker.setLocale('ru');

        $('.datetimepicker').each(function(ind,elt){
            
            $(elt).datetimepicker({ 
                lang:'ru',
                timepicker:false,
                format:'d.m.Y',
                onClose: function(ct,$i){
                    console.log($i);
                }
            });
        });

        $('.js-reorder-btn').on('click', function(){
           var $this = $(this),
               dataOrder = $this.attr('data-order');
           console.log(dataOrder);

            $.ajax({
                url: '/ajax/store/reorder.php',
                data: {order:dataOrder},
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        function ajaxSendFilter(value,urlAjax){
            $.ajax({
                url: urlAjax,
                data: value,
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    console.log(result.table);
                    $('.js-wrapper-ajax-filter tbody').empty();
                    $('.js-wrapper-ajax-filter tbody').append(result.table);
                },
                error: function (error) {
                    console.log('error' + error);
                }
            });
        };

        // function errorOrder(){}

        moduleApp.formValidation($('.js-book-form'), function($form){
            // console.log($form);
            $form.submit();
        });
    },
    'lkPersonal': function(){
        $('.js-save-change').on('click', function(){
            $('.personal-global-form').submit();
        });

        $('.js-add-person').on('click', function(){
            console.log('add');
            var template = $('.js-new-manager-form').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function($form){
                    $.fancybox.update();
                    
                    $form.submit();
                });
            });
        });

        $('.js-new-pass').on('click', function(){
            var template = $('.js-form-new-pass').html();

            moduleApp.popupOpen(template, 'fb-noscroll-window', function(){

                moduleApp.formValidation($('.fancybox-inner .js-form-reviews'), function($form){
                    $.fancybox.update();
                    
                    $form.submit();
                });
            });
        });

        $('.js-add-logo').on('change', function(){
            $('.personal-logotip').addClass('disabled');
        })
    },
    'handleFileSelect': function(evt){

        var $this = $(this);
        var $wrapperFileInput = $this.closest('.js-file-wrapper');
        $wrapperFileInput.find('.file-name').remove();
        var name = '<span class="file-name">'+$(this).val().split('\\').pop().split('/').pop()+'</span>';
        // var deletes = '<div class="delete-file js-delete-file"></div>';
        var size = '';
        // console.log($wrapperFileInput);

        // if (window.File) {
        //     var files = evt.target.files;
        //     var f = files[0];
        //     var reader = new FileReader();

        //     reader.readAsDataURL(f);
        //     reader.onload = (function (theFile) {
        //         size = '<div class="file-size">'+'('+Math.floor(theFile.size/1024) +'кб)'+'</div>';
        //     })(f);
        // }

        // $listFile.find('label').removeClass('active');
        // if($this.hasClass('one-file')){
        //     $listFile.append('<label class="active"><input type="file" class="js-add-file one-file" disabled name="File[]" data-validate="file"></label>');
        // }
        // else{
        //     if($listFile.find('label').length < 3){
        //         $listFile.append('<label class="active"><input type="file" class="js-add-file" name="File[]"></label>');
        //     }
        //     else{
        //         $listFile.append('<label class="active"><input type="file" class="js-add-file" disabled name="File[]"></label>');
        //     }
        // }

        $wrapperFileInput.append(name);
    },
    'addFile':function(){
        $(document).on('change', '.js-add-file', moduleApp.handleFileSelect);

        // $(document).on('click', '.js-delete-file', function(){
        //     var $this = $(this);
        //     var $formFile = $this.closest('.form-file');
        //     var $fileItem = $this.parent('.file-item');
        //     var index = $fileItem.index();
        //     var $listLabel = $formFile.find('.js-list-file');

        //     console.log($listLabel.find('.active'));
        //     $listLabel.find('.active').find('input').removeAttr('disabled');
        //     $listLabel.find('label').eq(index).remove();
        //     $fileItem.remove();
        // });
    },
    'newOrder': function(){
        $('.js-filter-order').on('change', function(){
            // sendAjax($(this).val());
            $(this).closest('form').submit();
        });

        // аякс для филтра
        // function sendAjax(value,pagin){
        //     if (ajax_request) {
        //         ajax_request.abort();
        //     }
        //     ajax_request = $.ajax({
        //         url: '/ajax/lk/catalog-lk-filter.php',
        //         data: {
        //             q: value
        //         },
        //         type: 'POST',
        //         dataType: 'JSON',
        //         success: function (result) {
        //             addElementTable(result);
        //         },
        //         error: function (error) {
        //         }
        //     });
        // };

        // смена элементов таблицы
        // function addElementTable(result){
        //     $('.table-new-order tbody').empty();
        //     $('.table-new-order tbody').append(result);
        // };

        // валидация изменение значение кол-ва товара
        $(document).on('change', '.js-count-new-order', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            var id = $this.attr('data-id');
            var price = $this.closest('tr').find('.js-price').text().replace(/,/, '.');
            var $newPrice = String((price * $this.val()).toFixed(2)).replace(/\./, ',');

            $this.closest('tr').find('.js-all-price').html($newPrice);
        });

        // добавлене к заказу
        $(document).on('click', '.js-add-to-order', function(){
            var value = $(this).attr('data-id'),
                count = $(this).closest('tr').find('.js-count-new-order').val();
            $.ajax({
                url: '/ajax/store/add_to_cart.php',
                data: {
                    PRODUCT_ID: value,
                    QUANTITY: count
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                },
                error: function (error) {
                }
            });
        });
    },
    'regionalOrder': function(){
        ajax_request = "";

        $(document).on('change', '.js-count-order', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            var id = $this.attr('data-id'),
                name = $this.attr('data-name'),
                value = $this.val(),
                basketAction = 'recalculate',
                idProduct = $this.attr('data-product_id');

            sendAjaxOrder(id,value,name, basketAction, idProduct, $this);
        });

        $('.js-deleted-in-order').on('click', function(){

            var $this = $(this),
                id = $this.attr('data-id'),
                basketAction = 'delete',
                value = 0,
                name = id;

            $this.closest('tr').remove();

            sendAjaxOrder(id,value,name, basketAction);
        });

        function sendAjaxOrder(id,value,name,basketAction, idProduct, $element){
            if (ajax_request) {
                // ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/recalculation_cart.php',
                data: {
                    id: id,
                    value: value,
                    name:name,
                    basketAction: basketAction,
                    product_id:idProduct
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    console.log(result);
                    changeCartAfterAjax(result);

                    if(basketAction == 'recalculate'){
                        changePriceElement($element);
                    }

                },
                error: function (error) {
                }
            });
        };

        function changePriceElement($element){
            var $this = $element,
                $thisRow = $this.closest('tr'),
                count = $thisRow.find('.js-count-order').val(),
                price = Number($thisRow.find('.js-price-sale').text());

            $thisRow.find('.js-all-price').html((price * count).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
            console.log($thisRow);
        };

        function changeCartAfterAjax(result){
            $('.js-total-price').html((result.ALL_PRICE_WITHOUT_DISCOUNT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-total-sale').html((result.DISCOUNT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-total-price-sale').html((result.TOTAL).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
            $('.js-marketing').html((result.MARKETING_SUPPORT).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽');
        };
    },
    'startupMessage':function(){
        var title = '';
        if (appConfig.startupMessage.title && appConfig.startupMessage.message) {
            var template = '<div class="fb-popup-message">';
            template += '<h3 class="fb-title">'+appConfig.startupMessage.title+'</h3>';
            template += '<p class="fb-message">'+appConfig.startupMessage.message+'</p>';
            template += '</div>';

            $.fancybox({
                content: template,
                wrapCSS: 'fb-noscroll-window',
                padding: 0,
                margin: 10,
                fitToView: false,
                openEffect: 'drop',
                closeEffect: 'drop',
                scrolling: 'auto',
                maxWidth: 1100,
                autoHeight: true,
            });
        }
    },
    'basket': function(){
        $(document).on('click', '.js-basket-counter-plus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-count-basket'),
                id = $input.attr('data-id'),
                name = $input.attr('data-id'),
                basketAction = 'recalculate',
                idProduct = $input.attr('data-product_id'),
                value = $input.val() + 1;

            sendAjaxOrder(id,value,name,basketAction, idProduct, $this);

            if(parseInt($input.val()) < $this.attr('data-max')){
                $input.val(parseInt($input.val()) + 1);
            }

            if(parseInt($input.val()) == $this.attr('data-max')){
                $this.addClass('disabled');
            }

            if(parseInt($input.val()) > 1){
                $this.siblings('.js-basket-counter-minus').removeClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('click', '.js-basket-counter-minus', function(){
            var $this = $(this),
                $input = $this.siblings('.js-count-basket'),
                id = $input.attr('data-id'),
                name = $input.attr('data-id'),
                basketAction = 'recalculate',
                idProduct = $input.attr('data-product_id'),
                value = $input.val() - 1;

            sendAjaxOrder(id,value,name,basketAction, idProduct, $this);

            if(!$this.hasClass('disabled')){
                $input.val(parseInt($input.val()) - 1);

                if(parseInt($input.val()) < $this.siblings('.js-basket-counter-plus').attr('data-max')){
                    $this.siblings('.js-basket-counter-plus').removeClass('disabled');
                }
            }

            if(parseInt($input.val()) == 1){
                $this.addClass('disabled');
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($input.val()));
            }
        });

        $(document).on('change', '.js-count-basket', function(e){
            e.preventDefault();
            var $this = $(this);

            if($this.val() == ''){
                $this.val('1');
            }

            if(parseInt($this.val()) <= 0){
                $this.val('1');
            }

            if(isNaN(parseInt($this.val())) || isNaN($this.val()*1)){
                $this.val('1');
            }

            if(parseInt($this.val()) > parseInt($this.siblings('.js-basket-counter-plus').attr('data-max'))){
                $this.val($this.siblings('.js-basket-counter-plus').attr('data-max'));
            }

            if($this.siblings('.js-price-count').length > 0){
                var $priceContainer = $this.siblings('.js-price-count'),
                    price = parseInt($priceContainer.attr('data-price'));
                $priceContainer.find('span').html(price * parseInt($this.val()));
            }
        });

        $(document).on('click', '.js-deleted-in-basket', function(){

            var $this = $(this),
                id = $this.attr('data-id'),
                basketAction = 'delete',
                value = 0,
                name = id;

            $this.closest('tr').remove();

            sendAjaxOrder(id,value,name, basketAction);
        });

        $('.js-add-basket-product').on('click', function(){
            console.log('add to cart');
        });

        ajax_request = "";

        function sendAjaxOrder(id,value,name,basketAction, idProduct, $element){
            if (ajax_request) {
                ajax_request.abort();
            }
            ajax_request = $.ajax({
                url: '/ajax/store/add_to_cart.php',
                data: {
                    id: id,
                    value: value,
                    name:name,
                    basketAction: basketAction,
                    product_id:idProduct
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (result) {
                    console.log(result);
                    console.log($element);
                },
                error: function (error) {
                }
            });
        };
    },
    'adaptiveTable': function(){
        var widthWindow = $(document).width();

        if(widthWindow < 930){
            $('.adaptive-table').wrap('<div class="wrapper-scroll-table"><div class="container-scroll-table"></div> </div>');
        }
    }
};

$(document).ready(function () {
    moduleApp.init();
});
//# sourceMappingURL=main.js.map
