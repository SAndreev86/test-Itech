<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;


if (CModule::IncludeModule('iblock')) {

    $arSelect = Array("NAME", "DETAIL_TEXT");

    if ($USER->IsAuthorized()) {
        $arFilter = Array("IBLOCK_ID" => "1", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
    } else {
        $arFilter = Array("IBLOCK_ID" => "1", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "property_AUTHORIZED" => 1);
    }
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arResult['news'][] = [
            'caption' => $ob->GetFields()['NAME'],
            'text' => $ob->GetFields()['DETAIL_TEXT'],
        ];

    }

}


$this->IncludeComponentTemplate();

?>