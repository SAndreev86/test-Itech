<div class="row">

    <? foreach ($arResult["news"] as $arItem): ?>

        <div class="col-md-12">
            <h2 class="text_color text-center"><?= $arItem['caption'] ?></h2>
            <p class="text_color text-center"><?= $arItem['text'] ?></p>
        </div>
    <? endforeach; ?>
</div>
