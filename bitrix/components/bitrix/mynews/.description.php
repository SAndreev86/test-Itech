<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
    "NAME" => GetMessage('NEWS_NAME'),
    "DESCRIPTION" => GetMessage('DESCR'),
    "PATH" => [
        "ID" => "mynews",
        "NAME" => GetMessage('NEWS_NAME'),
    ],
    "ICON" => "/images/news_icon.png",
];

?>
